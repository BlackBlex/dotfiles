local wezterm = require 'wezterm';
function basename(s) return string.gsub(s, "(.*[/\\])(.*)", "%2") end

return {
    font = wezterm.font("Cascadia Code"),
    font_size = 11,

    check_for_updates = false,
    show_update_window = false,
    prefer_egl = false,
    hide_tab_bar_if_only_one_tab = true,
    tab_bar_at_bottom = false,
    -- use_fancy_tab_bar = true,
    freetype_load_target = "HorizontalLcd",

    pane_focus_follows_mouse = true,

    window_padding = {left = 4, right = 4, top = 4, bottom = 4},
    window_background_opacity = 0.85,

    inactive_pane_hsb = {saturation = 1.0, brightness = 0.1},

    colors = {
        tab_bar = {
            -- The color of the strip that goes along the top of the window
            -- (does not apply when fancy tab bar is in use)
            background = "#181b20",

            -- The active tab is the one that has focus in the window
            active_tab = {
                -- The color of the background area for the tab
                bg_color = "#30363f",
                -- The color of the text for the tab
                fg_color = "#ececec",

                -- Specify whether you want "Half", "Normal" or "Bold" intensity for the
                -- label shown for this tab.
                -- The default is "Normal"
                intensity = "Bold",

                -- Specify whether you want "None", "Single" or "Double" underline for
                -- label shown for this tab.
                -- The default is "None"
                underline = "None",

                -- Specify whether you want the text to be italic (true) or not (false)
                -- for this tab.  The default is false.
                italic = false,

                -- Specify whether you want the text to be rendered with strikethrough (true)
                -- or not for this tab.  The default is false.
                strikethrough = false
            },

            -- Inactive tabs are the tabs that do not have focus
            inactive_tab = {
                bg_color = "#1f2329",
                fg_color = "#535965",
                italic = true

                -- The same options that were listed under the `active_tab` section above
                -- can also be used for `inactive_tab`.
            },

            -- You can configure some alternate styling when the mouse pointer
            -- moves over inactive tabs
            inactive_tab_hover = {
                bg_color = "#30363f",
                fg_color = "#909090",
                italic = true

                -- The same options that were listed under the `active_tab` section above
                -- can also be used for `inactive_tab_hover`.
            },

            -- The new tab button that let you create new tabs
            new_tab = {
                bg_color = "#323641",
                fg_color = "#a0a8b7"

                -- The same options that were listed under the `active_tab` section above
                -- can also be used for `new_tab`.
            },

            -- You can configure some alternate styling when the mouse pointer
            -- moves over the new tab button
            new_tab_hover = {
                bg_color = "#1f2329",
                fg_color = "#a0a8b7",
                italic = true

                -- The same options that were listed under the `active_tab` section above
                -- can also be used for `new_tab_hover`.
            }
        }
    },

    -- Color schema
    color_scheme = 'OneHalfDark',
    color_schemes = {
        ['OneHalfDark'] = {
            -- The default text color
            foreground = "#a0a8b7",
            -- The default background color
            background = "#181b20",

            -- Overrides the cell background color when the current cell is occupied by the
            -- cursor and the cursor style is set to Block
            cursor_bg = "#a0a8b7",
            -- Overrides the text color when the current cell is occupied by the cursor
            cursor_fg = "#a0a8b7",
            -- Specifies the border color of the cursor when the cursor style is set to Block,
            -- of the color of the vertical or horizontal bar when the cursor style is set to
            -- Bar or Underline.
            cursor_border = "#a0a8b7",

            -- the foreground color of selected text
            -- selection_fg = "black",
            -- the background color of selected text
            -- selection_bg = "#fffacd",

            -- The color of the scrollbar "thumb"; the portion that represents the current viewport
            scrollbar_thumb = "#2b2f35",

            -- The color of the split lines between panes
            split = "#535965",

            ansi = {
                "#0e1013", "#e55561", "#8ebd6b", "#e2b86b", "#4fa6ed",
                "#bf68d9", "#48b0bd", "#ececec"
            },
            brights = {
                "#303741", "#e86973", "#9bc47c", "#e5c07c", "#61afef",
                "#c67add", "#5db9c4", "#ececec"
            }
        }
    },

    -- Cursor
    default_cursor_style = "SteadyBar",

    custom_block_glyphs = true,
    disable_default_key_bindings = true,
    mouse_bindings = {
        -- Change the default click behavior so that it only selects
        -- text and doesn't open hyperlinks
        {
            event = {Up = {streak = 1, button = "Left"}},
            mods = "NONE",
            action = wezterm.action {CompleteSelection = "PrimarySelection"}
        }, -- and make CTRL-Click open hyperlinks
        {
            event = {Up = {streak = 1, button = "Left"}},
            mods = "CTRL",
            action = "OpenLinkAtMouseCursor"
        }, {
            event = {Down = {streak = 1, button = "Right"}},
            action = wezterm.action {PasteFrom = "PrimarySelection"}
        }
    },

    wezterm.on("format-tab-title",
               function(tab, tabs, panes, config, hover, max_width)
        local pane = tab.active_pane
        local title = basename(pane.foreground_process_name) -- .. " " .. pane.pane_id

        return {{Text = " " .. title .. " "}}
    end),

    keys = {
        -- Create a new tab in the same domain as the current tab
        {
            key = "t",
            mods = "CTRL",
            action = wezterm.action {SpawnTab = "CurrentPaneDomain"}
        }, -- Create a new tab in the default domain
        {
            key = "t",
            mods = "ALT",
            action = wezterm.action {SpawnTab = "DefaultDomain"}
        }, -- move tabs
        {
            key = "LeftArrow",
            mods = "CTRL",
            action = wezterm.action {MoveTabRelative = -1}
        }, {
            key = "RightArrow",
            mods = "CTRL",
            action = wezterm.action {MoveTabRelative = 1}
        }, -- move pane
        {
            key = "LeftArrow",
            mods = "CTRL|SHIFT",
            action = wezterm.action {ActivatePaneDirection = "Left"}
        }, {
            key = "RightArrow",
            mods = "CTRL|SHIFT",
            action = wezterm.action {ActivatePaneDirection = "Right"}
        }, {
            key = "UpArrow",
            mods = "CTRL|SHIFT",
            action = wezterm.action {ActivatePaneDirection = "Up"}
        }, {
            key = "DownArrow",
            mods = "CTRL|SHIFT",
            action = wezterm.action {ActivatePaneDirection = "Down"}
        }, -- move focus between tabs
        {
            key = "LeftArrow",
            mods = "ALT",
            action = wezterm.action {ActivateTabRelative = -1}
        }, {
            key = "RightArrow",
            mods = "ALT",
            action = wezterm.action {ActivateTabRelative = 1}
        }, {key = "1", mods = "ALT", action = wezterm.action {ActivateTab = 0}},
        {key = "2", mods = "ALT", action = wezterm.action {ActivateTab = 1}},
        {key = "3", mods = "ALT", action = wezterm.action {ActivateTab = 2}},
        {key = "4", mods = "ALT", action = wezterm.action {ActivateTab = 3}},
        {key = "5", mods = "ALT", action = wezterm.action {ActivateTab = 4}},
        {key = "6", mods = "ALT", action = wezterm.action {ActivateTab = 5}},
        {key = "7", mods = "ALT", action = wezterm.action {ActivateTab = 6}},
        {key = "8", mods = "ALT", action = wezterm.action {ActivateTab = 7}},
        {key = "9", mods = "ALT", action = wezterm.action {ActivateTab = 8}},
        {key = "0", mods = "ALT", action = wezterm.action {ActivateTab = -1}},
        -- Close current tab
        {
            key = "w",
            mods = "CTRL",
            action = wezterm.action {CloseCurrentTab = {confirm = true}}
        }, -- Close current pane
        {
            key = "w",
            mods = "CTRL|SHIFT",
            action = wezterm.action {CloseCurrentPane = {confirm = true}}
        }, -- Search casesensitive
        {
            key = "f",
            mods = "CTRL",
            action = wezterm.action {Search = {CaseSensitiveString = ""}}
        }, -- Search caseinsensitive
        {
            key = "f",
            mods = "CTRL|SHIFT",
            action = wezterm.action {Search = {CaseInSensitiveString = ""}}
        }, -- Create new window
        {key = "n", mods = "CTRL", action = "SpawnWindow"}, -- Split vertical
        {
            key = "v",
            mods = "CTRL|SHIFT|ALT",
            action = wezterm.action {
                SplitVertical = {domain = "CurrentPaneDomain"}
            }
        }, -- Split horizontal
        {
            key = "h",
            mods = "CTRL|SHIFT|ALT",
            action = wezterm.action {
                SplitHorizontal = {domain = "CurrentPaneDomain"}
            }
        }, -- Show launcher
        {key = "l", mods = "ALT", action = "ShowLauncher"}, -- Copy selection
        {
            key = "c",
            mods = "CTRL|SHIFT",
            action = wezterm.action {CopyTo = "Clipboard"}
        }, -- Paste selection
        {
            key = "v",
            mods = "CTRL|SHIFT",
            action = wezterm.action {PasteFrom = "Clipboard"}
        }
    },

    window_frame = {
        -- The font used in the tab bar.
        -- Roboto Bold is the default; this font is bundled
        -- with wezterm.
        -- Whatever font is selected here, it will have the
        -- main font setting appended to it to pick up any
        -- fallback fonts you may have used there.
        font = wezterm.font({family = "Fira Code", weight = "Regular"}),

        -- The size of the font in the tab bar.
        -- Default to 10. on Windows but 12.0 on other systems
        font_size = 11.0,

        -- The overall background color of the tab bar when
        -- the window is focused
        active_titlebar_bg = "#181b20",

        -- The overall background color of the tab bar when
        -- the window is not focused
        inactive_titlebar_bg = "#181b20"
    }
}
