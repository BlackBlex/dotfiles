-- requirements
-- ~~~~~~~~~~~~
local awful = require("awful")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi
local gears = require("gears")
local wibox = require("wibox")
local helpers = require("helpers")
local clickable_container = require('mods.clickable-container')

local calendar = {}

local styles_month = {}
local styles_year = {}

styles_month.month = {
    fg_color = beautiful.fg_color,
    bg_color = beautiful.alpha,
    padding = { top = 20, bottom = 5, left = 5, right = 5 },
}

styles_month.normal = {
    bg_color = beautiful.alpha,
    fg_color = beautiful.fg_color,
    padding = { top = 0, bottom = 0, left = 3, right = 3 },
    markup = function(t) return t end
}

styles_month.focus = {
    fg_color = beautiful.accent,
    bg_color = beautiful.bg_2,
    padding = { top = 0, bottom = 0, left = 1, right = 1 },
    markup = function(t) return '<b>' .. t .. '</b>' end,
    shape = helpers.rrect(beautiful.rounded)
}

styles_month.header = {
    fg_color = beautiful.accent,
    bg_color = beautiful.alpha,
    padding = { top = 4, bottom = 4, left = 0, right = 0 },
    markup = function(t)
        return '<span font_desc="' .. beautiful.font_var .. ' Bold 18">' .. t ..
            '</span>'
    end
}

styles_month.weekday = {
    fg_color = beautiful.accent,
    bg_color = beautiful.alpha,
    padding = { top = 0, bottom = 0, left = 3, right = 3 },
    markup = function(t) return '<b>' .. t .. '</b>' end
}

styles_month.weeknumber = {
    fg_color = beautiful.accent,
    bg_color = beautiful.alpha,
    padding = { top = 0, bottom = 0, left = 1, right = 0 },
    markup = function(t)
        return
            '<span font_desc="' .. beautiful.font_var .. ' Italic 12">' .. t ..
            '</span>'
    end
}

-- Years
styles_year.year = styles_month.month
styles_year.normal = styles_month.normal
styles_year.focus = styles_month.focus
styles_year.weekday = styles_month.weekday
styles_year.weeknumber = styles_month.weeknumber
styles_year.yearheader = styles_month.header
styles_year.monthheader = {
    fg_color = beautiful.accent,
    bg_color = beautiful.alpha,
    padding = { top = 0, bottom = 0, left = 0, right = 0 },
    markup = function(t)
        return '<span font_desc="' .. beautiful.font_var .. ' Bold 14">' .. t ..
            '</span>'
    end
}

local function decorate_month(widget, flag, date)
    if flag == 'monthheader' and not styles_month.monthheader then
        flag = 'header'
    end

    local props = styles_month[flag] or {}
    if props.markup and widget.get_text and widget.set_markup then
        widget:set_markup(props.markup(widget:get_text()))
    end
    -- Change bg color for weekends
    local d = {
        year = date.year,
        month = (date.month or 1),
        day = (date.day or 1)
    }
    local weekday = tonumber(os.date('%w', os.time(d)))
    local default_fg = ((weekday == 0 or weekday == 6) and flag == 'weekday') and beautiful.red_color or
        props.fg_color
    local default_bg = props.bg_color or beautiful.bg_color

    local ret = wibox.widget {
        {
            { widget, halign = 'center', widget = wibox.container.place },
            margins = props.padding or 2,
            widget = wibox.container.margin
        },
        shape = props.shape,
        shape_border_color = props.border_color or beautiful.bg_color,
        shape_border_width = props.border_width or 0,
        fg = default_fg,
        bg = default_bg,
        widget = wibox.container.background
    }

    if flag == "header" then
        local clickHeader = wibox.widget {
            ret,
            widget = clickable_container
        }

        clickHeader:connect_signal("button::press", function()
            awesome.emit_signal("calendar::year_toggle")
        end)

        return clickHeader
    end

    return ret
end

local function decorate_year(widget, flag, date)
    if flag == 'monthheader' and not styles_year.monthheader then flag = 'header' end
    local props = styles_year[flag] or {}
    if props.markup and widget.get_text and widget.set_markup then
        widget:set_markup(props.markup(widget:get_text()))
    end

    local default_fg = props.fg_color or beautiful.ext_white_bg
    local default_bg = props.bg_color or beautiful.bg_color
    local ret = wibox.widget {
        {
            { widget, halign = 'center', widget = wibox.container.place },
            margins = props.padding or 2,
            widget = wibox.container.margin
        },
        shape = props.shape,
        shape_border_color = props.border_color or beautiful.bg_color,
        shape_border_width = props.border_width or 0,
        fg = default_fg,
        bg = default_bg,
        widget = wibox.container.background
    }

    return ret
end

calendar.years = wibox.widget {
    forced_num_cols = 3,
    forced_num_rows = 3,
    homogeneous = true,
    expand = true,
    visible = false,
    spacing = dpi(20),
    layout = wibox.layout.grid
}

local current_year = os.date('*t').year
local current_month = os.date('*t').month

calendar.years.generate = function(base_year)
    calendar.years:reset()
    local current = base_year
    for y = -1, 1, 1 do
        for x = -1, 1, 1 do
            current = base_year + ((3 * y) + x)
            local year = wibox.widget {
                {
                    {
                        fg = beautiful.fg_color,
                        text = current,
                        widget = wibox.widget.textbox
                    },
                    margins = dpi(15),
                    widget = wibox.container.margin
                },
                value = current,
                widget = clickable_container
            }

            year:connect_signal("button::press", function()
                calendar.month.go_to_year(year.value)
                calendar.years.visible = false
                calendar.month.visible = true
            end)

            calendar.years:add(year)
        end
    end
end

calendar.years.generate(current_year)

calendar.month = wibox.widget {
    date = os.date('*t'),
    font = beautiful.font_var .. "12",
    long_weekdays = true,
    spacing = dpi(1),
    week_numbers = true,
    flex_height = true,
    start_sunday = false,
    fn_embed = decorate_month,
    widget = wibox.widget.calendar.month
}

calendar.year = wibox.widget {
    date = os.date('*t'),
    font = beautiful.font_var .. "12",
    long_weekdays = true,
    spacing = dpi(1),
    week_numbers = true,
    start_sunday = false,
    fn_embed = decorate_year,
    widget = wibox.widget.calendar.year
}

calendar.month.reset_date = function() calendar.month.date = os.date('*t') end

calendar.month.go_to_year = function(year)
    if calendar.month.date.month == current_month and calendar.month.date.year ==
        year then
        calendar.month.date = os.date('*t')
    else
        calendar.month.date = { month = calendar.month.date.month, year = year }
    end
end

calendar.month.go_to_month = function(month)
    if calendar.month.date.year == current_year and calendar.month.date.month ==
        month then
        calendar.month.date = os.date('*t')
    else
        calendar.month.date = { year = calendar.month.date.year, month = month }
    end
end

calendar.month.down_month = function()
    local new_month = calendar.month.date.month - 1

    if new_month == current_month and calendar.month.date.year == current_year then
        calendar.month.date = os.date('*t')
    else
        calendar.month.date = {
            month = new_month,
            year = calendar.month.date.year
        }
    end
end

calendar.month.up_month = function()
    local new_month = calendar.month.date.month + 1

    if new_month == current_month and calendar.month.date.year == current_year then
        calendar.month.date = os.date('*t')
    else
        calendar.month.date = {
            month = new_month,
            year = calendar.month.date.year
        }
    end
end

calendar.month.down_year = function()
    local new_year = calendar.month.date.year - 1

    if new_year == current_year and calendar.month.date.month == current_month then
        calendar.month.date = os.date('*t')
    else
        calendar.month.date = {
            month = calendar.month.date.month,
            year = new_year
        }
    end
end

calendar.month.up_year = function()
    local new_year = calendar.month.date.year + 1

    if new_year == current_year and calendar.month.date.month == current_month then
        calendar.month.date = os.date('*t')
    else
        calendar.month.date = {
            month = calendar.month.date.month,
            year = new_year
        }
    end
end

awesome.connect_signal('calendar::year_toggle', function()
    if not calendar.years.visible then
        calendar.month.visible = false
        calendar.years.visible = true
    end
end)

calendar.month:buttons(gears.table
    .join( -- Left Click - Reset date to current date
        awful.button({}, 1, function()
            calendar.month.reset_date()
        end)))

return calendar
