local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi
local helpers = require("helpers")

-- widgets
-- ~~~~~~~
local calendar = require("layout.clock.calendar")

local calendar_month_box = wibox({
    type = "dock",
    width = dpi(320),
    height = dpi(320),
    bg = beautiful.black_color,
    border_width = dpi(2),
    border_color = beautiful.bg_3,
    ontop = true,
    visible = false,
    shape = helpers.infobubble(beautiful.rounded, 15, nil)
})

local calendar_year_box = wibox({
    type = "dock",
    width = dpi(1100),
    height = dpi(660),
    bg = beautiful.black_color,
    border_width = dpi(2),
    border_color = beautiful.bg_3,
    ontop = true,
    visible = false,
    shape = helpers.infobubble(beautiful.rounded, 15, nil)
})

-- toggler
local calendar_show = function(s, kind)
    kind.screen = s
    kind.x = (s.geometry.x - (kind.width / 2)) +
        (s.geometry.width / 2)
    kind.y = s.geometry.y + beautiful.bar_size
    kind.visible = true
end

local calendar_hide = function(kind)
    kind.visible = false

    if kind.month then
        calendar.month.reset_date()
    end
end

local calendar_month_grabber = awful.keygrabber {
    auto_start = true,
    stop_event = 'release',
    keypressed_callback = function(self, mod, key, command)
        if key == 'Escape' then
            if calendar.years.visible then
                calendar.years.visible = false
                calendar.month.visible = true
            else
                calendar_month_toggle()
            end
        elseif key == 'Up' then
            calendar.month.up_year()
        elseif key == 'Down' then
            calendar.month.down_year()
        elseif key == 'Left' then
            calendar.month.down_month()
        elseif key == 'Right' then
            calendar.month.up_month()
        end
    end
}

local calendar_year_grabber = awful.keygrabber {
    auto_start = true,
    stop_event = 'release',
    keypressed_callback = function(self, mod, key, command)
        if key == 'Escape' then
            calendar_year_toggle()
            -- elseif key == 'Up' then
            --     calendar.year.up_year()
            -- elseif key == 'Down' then
            --     calendar.year.down_year()
        end
    end
}

calendar_month_toggle = function(s)
    if calendar_month_box.visible then
        calendar_hide(calendar_month_box)
        calendar_month_grabber:stop()
    else
        if calendar_year_box.visible then
            calendar_hide(calendar_year_box)
            calendar_year_grabber:stop()
        end

        calendar_show(s, calendar_month_box)
        calendar_month_grabber:start()
    end
end

calendar_year_toggle = function(s)
    if calendar_year_box.visible then
        calendar_hide(calendar_year_box)
        calendar_year_grabber:stop()
    else
        if calendar_month_box.visible then
            calendar_hide(calendar_month_box)
            calendar_month_grabber:stop()
        end

        calendar_show(s, calendar_year_box)
        calendar_year_grabber:start()
    end
end

calendar_month_box:setup {
    id = "month",
    calendar.month,
    {
        {
            calendar.years,
            margins = { top = dpi(20) },
            widget = wibox.container.margin
        },
        widget = wibox.container.place
    },
    layout = wibox.layout.stack
}

calendar_year_box:setup {
    id = "year",
    calendar.year,
    widget = wibox.container.place
}
