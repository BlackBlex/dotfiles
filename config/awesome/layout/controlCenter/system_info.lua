-- profile widget
-- ~~~~~~~~~~~~~~


-- requirements
-- ~~~~~~~~~~~~
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi
local helpers = require("helpers")
local wibox = require("wibox")
local awful = require("awful")
-- local update_widget = require("mods.update_widget")

-- cpu, ram, screenshot, launcher


-- widgets
-- ~~~~~~~
local eth_icon = wibox.widget {
	font = beautiful.icon_var .. "14",
	markup = helpers.colorize_text("󰈀", beautiful.fg_color),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local eth_ip = wibox.widget {
	font = beautiful.font_var .. "Bold 11",
	markup = helpers.colorize_text("not connected", beautiful.fg_color .. "99"),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local wifi_icon = wibox.widget {
	font = beautiful.icon_var .. "14",
	markup = helpers.colorize_text("󰖩", beautiful.fg_color),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local wifi_ip = wibox.widget {
	font = beautiful.font_var .. "Bold 11",
	markup = helpers.colorize_text("not connected", beautiful.fg_color .. "99"),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local ethernet = wibox.widget {
	eth_icon,
	eth_ip,
	layout = wibox.layout.fixed.horizontal,
	spacing = dpi(8)
}

local wifi = wibox.widget {
	wifi_icon,
	wifi_ip,
	layout = wibox.layout.fixed.horizontal,
	spacing = dpi(8)
}

local kernel_icon = wibox.widget {
	font = beautiful.icon_var .. "14",
	markup = helpers.colorize_text("󰌽", beautiful.fg_color),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local kernel_name = wibox.widget {
	font = beautiful.font_var .. "Bold 11",
	markup = helpers.colorize_text("linux", beautiful.fg_color .. "99"),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local kernel = wibox.widget {
	kernel_icon,
	kernel_name,
	layout = wibox.layout.fixed.horizontal,
	spacing = dpi(8)
}

local packages_icon = wibox.widget {
	font = beautiful.icon_var .. "14",
	markup = helpers.colorize_text("󰏖", beautiful.fg_color),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local packages_name = wibox.widget {
	font = beautiful.font_var .. "Bold 11",
	markup = helpers.colorize_text("0", beautiful.fg_color .. "99"),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local packages = wibox.widget {
	packages_icon,
	packages_name,
	layout = wibox.layout.fixed.horizontal,
	spacing = dpi(8)
}

local shell_icon = wibox.widget {
	font = beautiful.icon_var .. "14",
	markup = helpers.colorize_text("", beautiful.fg_color),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local shell_name = wibox.widget {
	font = beautiful.font_var .. "Bold 11",
	markup = helpers.colorize_text("bash", beautiful.fg_color .. "99"),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local shell = wibox.widget {
	shell_icon,
	shell_name,
	layout = wibox.layout.fixed.horizontal,
	spacing = dpi(8)
}

local uptime_icon = wibox.widget {
	font = beautiful.icon_var .. "14",
	markup = helpers.colorize_text("󰥔", beautiful.fg_color),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local uptime_name = wibox.widget {
	font = beautiful.font_var .. "Bold 11",
	markup = helpers.colorize_text("1hr", beautiful.fg_color .. "99"),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local uptime = wibox.widget {
	uptime_icon,
	uptime_name,
	layout = wibox.layout.fixed.horizontal,
	spacing = dpi(8)
}

-- local distro_icon = wibox.widget {
-- 	font = beautiful.icon_var .. "14",
-- 	markup = helpers.colorize_text("󰣇", beautiful.fg_color),
-- 	widget = wibox.widget.textbox,
-- 	valign = "center",
-- 	align = "center"
-- }

-- local distro_name = wibox.widget {
-- 	font = beautiful.font_var .. "Bold 11",
-- 	markup = helpers.colorize_text("arch", beautiful.fg_color .. "99"),
-- 	widget = wibox.widget.textbox,
-- 	valign = "center",
-- 	align = "center"
-- }

-- local distro = wibox.widget {
-- 	distro_icon,
-- 	distro_name,
-- 	layout = wibox.layout.fixed.horizontal,
-- 	spacing = dpi(8)
-- }

local updates_icon = wibox.widget {
	font = beautiful.icon_var .. "14",
	markup = helpers.colorize_text("󰅢", beautiful.fg_color),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local updates_name = wibox.widget {
	font = beautiful.font_var .. "Bold 11",
	markup = helpers.colorize_text("0", beautiful.fg_color .. "99"),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local updates = wibox.widget {
	updates_icon,
	updates_name,
	layout = wibox.layout.fixed.horizontal,
	spacing = dpi(8)
}

local getIP = function()
	awful.spawn.easy_async("get_local_ip", function(stdout)
		local value = string.gsub(stdout, '^%s*(.-)%s*$', '%1')
		local ips = helpers.split(value, ',')

		if ips ~= nil then
			if ips[1] ~= "" then
				eth_ip.markup = helpers.colorize_text(ips[1], beautiful.fg_color .. "99")
			end

			if ips[2] ~= "" then
				wifi_ip.markup = helpers.colorize_text(ips[2], beautiful.fg_color .. "99")
			end
		end
	end)
end

local getKernel = function()
	awful.spawn.easy_async("uname -r", function(stdout)
		local value = string.gsub(stdout, '^%s*(.-)%s*$', '%1')

		if value ~= '' then
			kernel_name.markup = helpers.colorize_text(value, beautiful.fg_color .. "99")
		end
	end)
end

local getPackages = function()
	awful.spawn.easy_async_with_shell("pacman -Q | wc -l", function(stdout)
		local value = string.gsub(stdout, '^%s*(.-)%s*$', '%1')

		if value ~= '' then
			packages_name.markup = helpers.colorize_text(value, beautiful.fg_color .. "99")
		end
	end)
end

local getShell = function()
	awful.spawn.easy_async_with_shell("echo $SHELL | awk -F'/' '{print $NF}'", function(stdout)
		local value = string.gsub(stdout, '^%s*(.-)%s*$', '%1')

		if value ~= '' then
			shell_name.markup = helpers.colorize_text(value, beautiful.fg_color .. "99")
		end
	end)
end

local getUptime = function()
	awful.spawn.easy_async_with_shell("uptime -p | sed -e 's/up //'", function(stdout)
		local value = string.gsub(stdout, '^%s*(.-)%s*$', '%1')

		if value ~= '' then
			uptime_name.markup = helpers.colorize_text(value, beautiful.fg_color .. "99")
		end
	end)
end

-- local getDistro = function()
-- 	awful.spawn.easy_async_with_shell("awk '/^NAME=/' /etc/*-release | awk -F'=' '{ print $2 }' | sed -e 's/\"//g'",
-- 		function(stdout)
-- 			local value = string.gsub(stdout, '^%s*(.-)%s*$', '%1')

-- 			if value ~= '' then
-- 				distro_name.markup = helpers.colorize_text(value, beautiful.fg_color .. "99")
-- 			end
-- 		end)
-- end

local getUpdates = function()
	awful.spawn.easy_async_with_shell("pacupdate", function(stdout)
		local value = string.gsub(stdout, '^%s*(.-)%s*$', '%1')

		if value ~= '' then
			updates_name.markup = helpers.colorize_text(value, beautiful.fg_color .. "99")
		end
	end)
end

getIP()
getKernel()
getPackages()
getShell()
getUptime()
-- getDistro()
getUpdates()

awesome.connect_signal("controlCenter::visible", function(value)
	if value then
		getIP()
		getPackages()
		getUptime()
		getUpdates()
	end
end)

local system_info = wibox.widget {
	{
		{
			ethernet,
			layout = wibox.layout.fixed.horizontal
		},
		nil,
		{
			wifi,
			layout = wibox.layout.fixed.horizontal
		},
		layout = wibox.layout.align.horizontal
	},
	{
		{
			kernel,
			layout = wibox.layout.fixed.horizontal
		},
		nil,
		{
			packages,
			shell,
			layout = wibox.layout.fixed.horizontal,
			spacing = dpi(20)
		},
		layout = wibox.layout.align.horizontal
	},
	{
		{
			uptime,
			layout = wibox.layout.fixed.horizontal
		},
		nil,
		{
			-- distro,
			updates,
			layout = wibox.layout.fixed.horizontal,
			spacing = dpi(16)
		},
		layout = wibox.layout.align.horizontal
	},
	-- {
	-- 	updates,
	-- 	layout = wibox.layout.align.horizontal
	-- },
	layout = wibox.layout.fixed.vertical,
	spacing = dpi(8)
}


-- return
return system_info
