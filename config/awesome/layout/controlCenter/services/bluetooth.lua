-- Bluetooth button/widget
-- ~~~~~~~~~~~~~~~~~~


-- requirements
-- ~~~~~~~~~~~~
local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local dpi = beautiful.xresources.apply_dpi
local helpers = require("helpers")
local wibox = require("wibox")
local readwrite = require("misc.scripts.read_writer")

local accent_value = readwrite.readall("accent_state")
local button_creator = require("helpers.widgets.create_button")


-- misc/vars
-- ~~~~~~~~~

local service_name = "Bluetooth"
local service_icon = ""


-- widgets
-- ~~~~~~~

-- icon
local icon = wibox.widget {
    font = beautiful.icon_var .. "18",
    markup = helpers.colorize_text(service_icon, beautiful.fg_color),
    widget = wibox.widget.textbox,
    valign = "center",
    align = "center"
}

-- name
local name = wibox.widget {
    font = beautiful.font_var .. "10",
    widget = wibox.widget.textbox,
    markup = helpers.colorize_text(service_name, beautiful.fg_color),
    valign = "center",
    align = "center"
}

-- mix those
local alright = wibox.widget {
    {
        {
            nil,
            {
                icon,
                name,
                layout = wibox.layout.fixed.vertical,
                spacing = dpi(10)
            },
            layout = wibox.layout.align.vertical,
            expand = "none"
        },
        layout = wibox.layout.stack
    },
    widget = wibox.container.place,
    forced_width = dpi(80),
    forced_height = dpi(80)
}


local button = button_creator(alright, nil, nil, dpi(0), 0, beautiful.fg_color .. "33",
    helpers.rrect(beautiful.rounded), true, beautiful[accent_value .. "_color"])

-- function that updates everything
local function update_everything(value)
    button.set_state(value)
    if value then
        icon.markup = helpers.colorize_text(service_icon, beautiful.bg_color)
        name.markup = helpers.colorize_text(service_name, beautiful.bg_color)
    else
        icon.markup = helpers.colorize_text(service_icon, beautiful.fg_color)
        name.markup = helpers.colorize_text(service_name, beautiful.fg_color)
    end
end

awesome.connect_signal("signal::bluetooth", function(value, isrun)
    if value then
        update_everything(true)

        alright:buttons(gears.table.join(
            awful.button({}, 1, function()
                awful.spawn("toggle bluetooth off", false)
                update_everything(false)
                require("layout.ding.extra.short")("󰂲", "Bluetooth disabled")
            end)
        )
        )
    else
        update_everything(false)

        alright:buttons(gears.table.join(
            awful.button({}, 1, function()
                awful.spawn("toggle bluetooth on", false)
                update_everything(true)
                require("layout.ding.extra.short")("󰂯", "Bluetooth enabled")
            end)
        )
        )
    end
end)

return button
