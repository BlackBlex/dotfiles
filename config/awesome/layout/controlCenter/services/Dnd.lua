-- wifi button/widget
-- ~~~~~~~~~~~~~~~~~~


-- requirements
-- ~~~~~~~~~~~~
local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local dpi = beautiful.xresources.apply_dpi
local helpers = require("helpers")
local wibox = require("wibox")
local readwrite = require("misc.scripts.read_writer")

local accent_value = readwrite.readall("accent_state")
local button_creator = require("helpers.widgets.create_button")


-- misc/vars
-- ~~~~~~~~~

local service_name = "Silent"
local service_icon = "󰂛"

-- widgets
-- ~~~~~~~

-- icon
local icon = wibox.widget {
    font = beautiful.icon_var .. "18",
    markup = helpers.colorize_text(service_icon, beautiful.fg_color),
    widget = wibox.widget.textbox,
    valign = "center",
    align = "center"
}

-- name
local name = wibox.widget {
    font = beautiful.font_var .. "12",
    widget = wibox.widget.textbox,
    markup = helpers.colorize_text(service_name, beautiful.fg_color),
    valign = "center",
    align = "center"
}

-- mix those
local alright = wibox.widget {
    {
        {
            nil,
            {
                icon,
                name,
                layout = wibox.layout.fixed.vertical,
                spacing = dpi(10)
            },
            layout = wibox.layout.align.vertical,
            expand = "none"
        },
        layout = wibox.layout.stack
    },
    widget = wibox.container.place,
    forced_width = dpi(80),
    forced_height = dpi(80)
}

_G.awesome_dnd_state = false

local button = button_creator(alright, nil, nil, dpi(0), 0, beautiful.fg_color .. "33",
    helpers.rrect(beautiful.rounded), true, beautiful[accent_value .. "_color"])

local update_things = function()
    button.set_state(_G.awesome_dnd_state)
    if _G.awesome_dnd_state then
        icon.markup = helpers.colorize_text(service_icon, beautiful.bg_color)
        name.markup = helpers.colorize_text(service_name, beautiful.bg_color)
        require("layout.ding.extra.short")("󰂛", "Notifications disabled")
    else
        icon.markup = helpers.colorize_text(service_icon, beautiful.fg_color)
        name.markup = helpers.colorize_text(service_name, beautiful.fg_color)
        require("layout.ding.extra.short")("󰂚", "Notifications enabled")
    end
end

-- reload old state
--~~~~~~~~~~~~~~~~~

local output = readwrite.readall("dnd_state")

local boolconverter = {
    ["true"]  = true,
    ["false"] = false
}

_G.awesome_dnd_state = boolconverter[output]
update_things()
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




alright:buttons(gears.table.join(
    awful.button({}, 1, function()
        _G.awesome_dnd_state = not _G.awesome_dnd_state
        readwrite.write("dnd_state", tostring(_G.awesome_dnd_state))
        update_things()
    end)
)
)

return button
