-- Dark button/widget
-- ~~~~~~~~~~~~~~~~~~


-- requirements
-- ~~~~~~~~~~~~
local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local dpi = beautiful.xresources.apply_dpi
local helpers = require("helpers")
local wibox = require("wibox")
local readwrite = require("misc.scripts.read_writer")

local button_creator = require("helpers.widgets.create_button")
local list_creator = require("helpers.widgets.list_widget")


-- misc/vars
-- ~~~~~~~~~

local service_name = "Accent"
local service_icon = "󰏫"

-- widgets
-- ~~~~~~~

-- icon
local icon = wibox.widget {
    font = beautiful.icon_var .. "18",
    markup = helpers.colorize_text(service_icon, beautiful.bg_color),
    widget = wibox.widget.textbox,
    valign = "center",
    align = "center"
}

-- name
local name = wibox.widget {
    font = beautiful.font_var .. "12",
    widget = wibox.widget.textbox,
    markup = helpers.colorize_text(service_name, beautiful.bg_color),
    valign = "center",
    align = "center"
}

local menu_items = {
    { name = 'Red' },
    { name = 'Green' },
    { name = 'Yellow' },
    { name = 'Orange' },
    { name = 'Blue' },
    { name = 'Purple' },
    { name = 'Cyan' },
    { name = 'Grey' },
    { name = 'Black' },
}

local callback = function(item)
    readwrite.write("accent_state", string.lower(item.name))
    awesome.restart()
end

local list_selector = list_creator('󰕱', beautiful.icon_var .. "12", menu_items, callback, 100, nil, nil, { x = 10, y = 0 },
    nil, nil, helpers.rrect(beautiful.rounded))

list_selector.visible = false

-- mix those
local alright = wibox.widget {
    {
        {
            nil,
            {
                icon,
                list_selector,
                name,
                layout = wibox.layout.fixed.vertical,
                spacing = dpi(8)
            },
            layout = wibox.layout.align.vertical,
            expand = "none"
        },
        layout = wibox.layout.stack
    },
    widget = wibox.container.place,
    forced_width = dpi(80),
    forced_height = dpi(80)
}

alright:buttons(gears.table.join(
    awful.button({}, 1, function()
        list_selector.toggle()
    end)
))

awesome.connect_signal("controlCenter::visible", function(value)
    if not value then
        list_selector.close()
    end
end)


return button_creator(alright, beautiful.accent, nil, dpi(0), 0, beautiful.fg_color .. "33",
    helpers.rrect(beautiful.rounded))
