-- services buttons
-- ~~~~~~~~~~~~~~~~
-- each button has a fade animation when activated



-- requirements
-- ~~~~~~~~~~~~
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi
local wibox = require("wibox")


-- widgets
-- ~~~~~~~
local wifi = require("layout.controlCenter.services.wifi")
local bluetooth   = require("layout.controlCenter.services.bluetooth")
local accent_color = require("layout.controlCenter.services.accent_color")
local dnd = require("layout.controlCenter.services.Dnd")

-- local mic = require("layout.controlCenter.services.mic")
-- local record = require("layout.controlCenter.services.Record")

-- return
-- ~~~~~~
local returner =  wibox.widget{
	wifi,
	bluetooth,
	dnd,
	accent_color,
	layout = wibox.layout.fixed.horizontal,
	spacing = dpi(22)
}


-- awesome.connect_signal("controlCenter::extras", function (value)
--     control_center_extra_conrols.visible = value or false
--     if not value then
--         returner.spacing = dpi(0)
--     else
--         returner.spacing = dpi(22)
--     end
-- end)


return returner
