-- services buttons
-- ~~~~~~~~~~~~~~~~
-- each button has a fade animation when activated



-- requirements
-- ~~~~~~~~~~~~
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi
local wibox = require("wibox")
local helpers = require("helpers")
local gears = require("gears")
local awful = require("awful")
local button_creator = require("helpers.widgets.create_button")

local size_button = dpi(50)
local size_font = "24"

local libreoffice_base_icon = wibox.widget {
	font = beautiful.icon_var .. size_font,
	markup = helpers.colorize_text("󰆼", beautiful.grey_color),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local libreoffice_base = wibox.widget {
	libreoffice_base_icon,
	widget = wibox.container.place,
	forced_width = size_button,
	forced_height = size_button
}

libreoffice_base:buttons(gears.table.join(
	awful.button({}, 1, function()
		awful.spawn.with_shell("libreoffice --base", false)
		cc_toggle()
	end)
)
)

local libreoffice_base_tooltip = awful.tooltip {
	objects          = { libreoffice_base },
	mode             = "mouse",
	align            = "top_right",
	delay_show       = 1.5,
	margin_leftright = 10,
	timer_function   = function()
		return os.date('LibreOffice Base')
	end,
}

local libreoffice_calc_icon = wibox.widget {
	font = beautiful.icon_var .. size_font,
	markup = helpers.colorize_text("󰃬", beautiful.grey_color),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local libreoffice_calc = wibox.widget {
	libreoffice_calc_icon,
	widget = wibox.container.place,
	forced_width = size_button,
	forced_height = size_button
}

libreoffice_calc:buttons(gears.table.join(
	awful.button({}, 1, function()
		awful.spawn.with_shell("libreoffice --calc", false)
		cc_toggle()
	end)
)
)

local libreoffice_calc_tooltip = awful.tooltip {
	objects          = { libreoffice_calc },
	mode             = "mouse",
	align            = "top_right",
	delay_show       = 1.5,
	margin_leftright = 10,
	timer_function   = function()
		return os.date('LibreOffice Calc')
	end,
}

local libreoffice_draw_icon = wibox.widget {
	font = beautiful.icon_var .. size_font,
	markup = helpers.colorize_text("󰍣", beautiful.grey_color),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local libreoffice_draw = wibox.widget {
	libreoffice_draw_icon,
	widget = wibox.container.place,
	forced_width = size_button,
	forced_height = size_button
}

libreoffice_draw:buttons(gears.table.join(
	awful.button({}, 1, function()
		awful.spawn.with_shell("libreoffice --draw", false)
		cc_toggle()
	end)
)
)

local libreoffice_draw_tooltip = awful.tooltip {
	objects          = { libreoffice_draw },
	mode             = "mouse",
	align            = "top_right",
	delay_show       = 1.5,
	margin_leftright = 10,
	timer_function   = function()
		return os.date('LibreOffice Draw')
	end,
}

local libreoffice_impress_icon = wibox.widget {
	font = beautiful.icon_var .. size_font,
	markup = helpers.colorize_text("󱀂", beautiful.grey_color),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local libreoffice_impress = wibox.widget {
	libreoffice_impress_icon,
	widget = wibox.container.place,
	forced_width = size_button,
	forced_height = size_button
}

libreoffice_impress:buttons(gears.table.join(
	awful.button({}, 1, function()
		awful.spawn.with_shell("libreoffice --impress", false)
		cc_toggle()
	end)
)
)

local libreoffice_impress_tooltip = awful.tooltip {
	objects          = { libreoffice_impress },
	mode             = "mouse",
	align            = "top_right",
	delay_show       = 1.5,
	margin_leftright = 10,
	timer_function   = function()
		return os.date('LibreOffice Impress')
	end,
}

local libreoffice_math_icon = wibox.widget {
	font = beautiful.icon_var .. size_font,
	markup = helpers.colorize_text("󰍘", beautiful.ext_white_bg),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local libreoffice_math = wibox.widget {
	libreoffice_math_icon,
	widget = wibox.container.place,
	forced_width = size_button,
	forced_height = size_button
}

libreoffice_math:buttons(gears.table.join(
	awful.button({}, 1, function()
		awful.spawn.with_shell("libreoffice --math", false)
		cc_toggle()
	end)
)
)

local libreoffice_math_tooltip = awful.tooltip {
	objects          = { libreoffice_math },
	mode             = "mouse",
	align            = "top_right",
	delay_show       = 1.5,
	margin_leftright = 10,
	timer_function   = function()
		return os.date('LibreOffice Math')
	end,
}

local libreoffice_writer_icon = wibox.widget {
	font = beautiful.icon_var .. size_font,
	markup = helpers.colorize_text("󰏪", beautiful.grey_color),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local libreoffice_writer = wibox.widget {
	libreoffice_writer_icon,
	widget = wibox.container.place,
	forced_width = size_button,
	forced_height = size_button
}

libreoffice_writer:buttons(gears.table.join(
	awful.button({}, 1, function()
		awful.spawn.with_shell("libreoffice --writer", false)
		cc_toggle()
	end)
)
)

local libreoffice_writer_tooltip = awful.tooltip {
	objects          = { libreoffice_writer },
	mode             = "mouse",
	align            = "top_right",
	delay_show       = 1.5,
	margin_leftright = 10,
	timer_function   = function()
		return os.date('LibreOffice Writer')
	end,
}

local virtualbox_icon = wibox.widget {
	font = beautiful.icon_var .. size_font,
	markup = helpers.colorize_text("󰇣", beautiful.grey_color),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local virtualbox = wibox.widget {
	virtualbox_icon,
	widget = wibox.container.place,
	forced_width = size_button,
	forced_height = size_button
}

virtualbox:buttons(gears.table.join(
	awful.button({}, 1, function()
		awful.spawn.with_shell("virtualbox", false)
		cc_toggle()
	end)
)
)

local virtualbox_tooltip = awful.tooltip {
	objects          = { virtualbox },
	mode             = "mouse",
	align            = "top_right",
	delay_show       = 1.5,
	margin_leftright = 10,
	timer_function   = function()
		return os.date('VirtualBox')
	end,
}

local ranger_icon = wibox.widget {
	font = beautiful.icon_var .. size_font,
	markup = helpers.colorize_text("󰝰", beautiful.grey_color),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local ranger = wibox.widget {
	ranger_icon,
	widget = wibox.container.place,
	forced_width = size_button,
	forced_height = size_button
}

ranger:buttons(gears.table.join(
	awful.button({}, 1, function()
		awful.spawn.with_shell("kitty --class 'ranger' --name 'ranger' ranger", false)
		cc_toggle()
	end)
)
)

local ranger_tooltip = awful.tooltip {
	objects          = { ranger },
	mode             = "mouse",
	align            = "top_right",
	delay_show       = 1.5,
	margin_leftright = 10,
	timer_function   = function()
		return os.date('Ranger')
	end,
}

local brave_browser_icon = wibox.widget {
	font = beautiful.icon_var .. "28",
	markup = helpers.colorize_text("󰊯", beautiful.grey_color),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local brave_browser = wibox.widget {
	brave_browser_icon,
	widget = wibox.container.place,
	forced_width = size_button,
	forced_height = size_button
}

brave_browser:buttons(gears.table.join(
	awful.button({}, 1, function()
		awful.spawn.with_shell("brave", false)
		cc_toggle()
	end)
)
)

local brave_browser_tooltip = awful.tooltip {
	objects          = { brave_browser },
	mode             = "mouse",
	align            = "top_right",
	delay_show       = 1.5,
	margin_leftright = 10,
	timer_function   = function()
		return os.date('Brave browser')
	end,
}

local vscodium_icon = wibox.widget {
	font = beautiful.icon_var .. size_font,
	markup = helpers.colorize_text("", beautiful.grey_color),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local vscodium = wibox.widget {
	vscodium_icon,
	widget = wibox.container.place,
	forced_width = size_button,
	forced_height = size_button
}

vscodium:buttons(gears.table.join(
	awful.button({}, 1, function()
		awful.spawn.with_shell("vscodium", false)
		cc_toggle()
	end)
)
)

local vscodium_tooltip = awful.tooltip {
	objects          = { vscodium },
	mode             = "mouse",
	align            = "top_right",
	delay_show       = 1.5,
	margin_leftright = 10,
	timer_function   = function()
		return os.date('VSCodium')
	end,
}

local krita_icon = wibox.widget {
	font = beautiful.icon_var .. size_font,
	markup = helpers.colorize_text("󰏘", beautiful.grey_color),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local krita = wibox.widget {
	krita_icon,
	widget = wibox.container.place,
	forced_width = size_button,
	forced_height = size_button
}

krita:buttons(gears.table.join(
	awful.button({}, 1, function()
		awful.spawn.with_shell("krita", false)
		cc_toggle()
	end)
)
)

local krita_tooltip = awful.tooltip {
	objects          = { krita },
	mode             = "mouse",
	align            = "top_right",
	delay_show       = 1.5,
	margin_leftright = 10,
	timer_function   = function()
		return os.date('Krita')
	end,
}

local virt_manager_icon = wibox.widget {
	font = beautiful.icon_var .. size_font,
	markup = helpers.colorize_text("󰌢", beautiful.ext_white_bg),
	widget = wibox.widget.textbox,
	valign = "center",
	align = "center"
}

local virt_manager = wibox.widget {
	virt_manager_icon,
	widget = wibox.container.place,
	forced_width = size_button,
	forced_height = size_button
}

virt_manager:buttons(gears.table.join(
	awful.button({}, 1, function()
		awful.spawn.with_shell("virt-manager", false)
		cc_toggle()
	end)
)
)

local virt_manager_tooltip = awful.tooltip {
	objects          = { virt_manager },
	mode             = "mouse",
	align            = "top_right",
	delay_show       = 1.5,
	margin_leftright = 10,
	timer_function   = function()
		return os.date('QEMU Virt Manager')
	end,
}



return wibox.widget {
	{
		{
			button_creator(virtualbox, beautiful.cyan_color, beautiful.cyan, dpi(0), 0, beautiful.fg_color .. "33", helpers.rrect(50)),
			button_creator(ranger, beautiful.orange_color, beautiful.orange, dpi(0), 0, beautiful.fg_color .. "33", helpers.rrect(50)),
			button_creator(brave_browser, beautiful.red_color, beautiful.red, dpi(0), 0, beautiful.fg_color .. "33", helpers.rrect(50)),
			button_creator(vscodium, beautiful.blue_color, beautiful.blue, dpi(0), 0, beautiful.fg_color .. "33", helpers.rrect(50)),
			button_creator(krita, beautiful.purple_color, beautiful.purple, dpi(0), 0, beautiful.fg_color .. "33", helpers.rrect(50)),
			button_creator(virt_manager, beautiful.black_color, beautiful.black, dpi(0), 0, beautiful.fg_color .. "33", helpers.rrect(50)),
			layout = wibox.layout.fixed.horizontal,
			spacing = dpi(10)
		},
		{
			button_creator(libreoffice_base, beautiful.red_color, beautiful.red, dpi(0), 0, beautiful.fg_color .. "33", helpers.rrect(50)),
			button_creator(libreoffice_calc, beautiful.green_color, beautiful.green, dpi(0), 0, beautiful.fg_color .. "33", helpers.rrect(50)),
			button_creator(libreoffice_draw, beautiful.yellow_color, beautiful.yellow, dpi(0), 0, beautiful.fg_color .. "33", helpers.rrect(50)),
			button_creator(libreoffice_impress, beautiful.orange_color, beautiful.orange, dpi(0), 0, beautiful.fg_color .. "33", helpers.rrect(50)),
			button_creator(libreoffice_math, beautiful.grey_color, beautiful.grey, dpi(0), 0, beautiful.fg_color .. "33", helpers.rrect(50)),
			button_creator(libreoffice_writer, beautiful.blue_color, beautiful.blue, dpi(0), 0, beautiful.fg_color .. "33", helpers.rrect(50)),
			layout = wibox.layout.fixed.horizontal,
			spacing = dpi(10)
		},
		layout = wibox.layout.fixed.vertical,
		spacing = dpi(20)
	},
	widget = wibox.container.place
}
