-- ding - notification
-- ~~~~~~~~~~~~~~~~~~~
-- requirements
-- ~~~~~~~~~~~~
local naughty = require("naughty")
local beautiful = require("beautiful")
local wibox = require("wibox")
local awful = require("awful")
local dpi = beautiful.xresources.apply_dpi
local helpers = require("helpers")
local ruled = require("ruled")
local menubar = require("menubar")
local gears = require("gears")
local clickable_container = require('mods.clickable-container')

-- extras
-- ~~~~~~

require("layout.ding.extra.music")
require("layout.ding.extra.popup")

-- misc/vars
-- ~~~~~~~~~

-- table of icons
local app_icons = {
    ["brave"] = { icon = "󰊯" },
    ["discord"] = { icon = "󰙯" },
    ["music"] = { icon = "󰝚" },
    ["color picker"] = { icon = "󰏘" },
    ["screenshot"] = { icon = "󰊓" },
    ["notify-send"] = { icon = "󰂚" },
    ["notify-send.sh"] = { icon = "󰂚" },
    ["set theme"] = { icon = "󰉦" }
}

-- configuration
-- ~~~~~~~~~~~~~

-- icon
naughty.connect_signal("request::icon", function(n, context, hints)
    if context ~= 'app_icon' then return end

    local path = menubar.utils.lookup_icon(hints.app_icon) or
        menubar.utils.lookup_icon(hints.app_icon:lower())

    if path then n.icon = path end
end)

-- ruled notification
ruled.notification.connect_signal("request::rules", function()
    ruled.notification.append_rule {
        rule = {},
        properties = {
            screen = awful.screen.preferred,
            implicit_timeout = 6,
            position = "bottom_right",
            bg = beautiful.bg_color
        }
    }

    ruled.notification.append_rule {
        rule = { urgency = 'critical' },
        properties = {
            implicit_timeout = 0,
            font = beautiful.font_var .. "12",
            bg = beautiful.red_color
        }
    }

    ruled.notification.append_rule {
        rule = { urgency = 'normal' },
        properties = { font = beautiful.font_var .. "10", implicit_timeout = 5 }
    }

    ruled.notification.append_rule {
        rule = { urgency = 'low' },
        properties = { font = beautiful.font_var .. "10", implicit_timeout = 3 }
    }
end)

-- connect to each display
-- ~~~~~~~~~~~~~~~~~~~~~~~
naughty.connect_signal("request::display", function(n)
    -- app icon
    local app_icon
    local tolow = string.lower

    if app_icons[tolow(n.app_name)] then
        app_icon = app_icons[tolow(n.app_name)].icon
    else
        app_icon = "󰘔"
    end

    local action_button = {
        {
            {
                id = "text_role",
                font = beautiful.font_var .. "10",
                widget = wibox.widget.textbox
            },
            forced_height = dpi(40),
            widget = wibox.container.place
        },
        widget = clickable_container
    }

    -- action widget
    local action_widget = {
        {
            action_button,
            left = dpi(6),
            right = dpi(6),
            widget = wibox.container.margin
        },
        bg = beautiful.bg_2,
        forced_height = dpi(30),
        shape = helpers.rrect(dpi(5)),
        widget = wibox.container.background
    }

    -- actions
    local actions = wibox.widget {
        notification = n,
        base_layout = wibox.widget {
            spacing = dpi(8),
            layout = wibox.layout.flex.horizontal
        },
        widget_template = action_widget,
        style = { underline_normal = false, underline_selected = true },
        widget = naughty.list.actions
    }

    -- image
    local image_n = wibox.widget {
        {
            image = n.icon,
            resize = true,
            clip_shape = helpers.rrect(beautiful.rounded),
            halign = "center",
            valign = "center",
            forced_height = dpi(72),
            forced_width = dpi(72),
            widget = wibox.widget.imagebox
        },
        margin = dpi(5),
        widget = wibox.container.margin
    }

    -- title
    local title_n = wibox.widget {
        {
            {
                markup = helpers.colorize_text(n.title,
                    beautiful.ext_white_bg .. "BF"),
                font = beautiful.font_var .. "Bold 11",
                align = "left",
                valign = "center",
                widget = wibox.widget.textbox
            },
            forced_width = dpi(200),
            widget = wibox.container.scroll.horizontal,
            step_function = wibox.container.scroll.step_functions
                .waiting_nonlinear_back_and_forth,
            speed = 50
        },
        margins = { right = 15 },
        widget = wibox.container.margin
    }

    local message_n = wibox.widget {
        {
            {
                markup = helpers.colorize_text(
                    "<span weight='normal'>" .. n.message .. "</span>",
                    beautiful.ext_white_bg .. "BF"),
                font = beautiful.font_var .. " 11",
                align = "left",
                valign = "top",
                widget = wibox.widget.textbox
            },
            width = dpi(250),
            height = nil,
            widget = wibox.container.constraint
        },
        margins = { right = 15 },
        widget = wibox.container.margin
    }

    -- app icon
    local app_icon_n = wibox.widget {
        {
            font = beautiful.icon_var .. "12",
            markup = helpers.colorize_text(app_icon, beautiful.black_color),
            align = "center",
            valign = "center",
            widget = wibox.widget.textbox
        },
        bg = beautiful.accent,
        widget = wibox.container.background,
        shape = gears.shape.circle,
        forced_height = dpi(22),
        forced_width = dpi(22)
    }

    -- app name
    local app_name_n = wibox.widget {
        markup = helpers.colorize_text(n.app_name,
            beautiful.ext_white_bg .. "BF"),
        font = beautiful.font_var .. " 10",
        align = "left",
        valign = "center",
        widget = wibox.widget.textbox
    }

    local time_n = wibox.widget {
        {
            markup = helpers.colorize_text("now", beautiful.ext_white_bg .. "BF"),
            font = beautiful.font_var .. " 10",
            align = "right",
            valign = "center",
            widget = wibox.widget.textbox
        },
        margins = { right = 20 },
        widget = wibox.container.margin
    }

    -- extra info
    local notif_info = wibox.widget {
        app_name_n,
        {
            widget = wibox.widget.separator,
            shape = gears.shape.circle,
            forced_height = dpi(4),
            forced_width = dpi(4),
            color = beautiful.fg_color .. "BF"
        },
        time_n,
        layout = wibox.layout.fixed.horizontal,
        spacing = dpi(7)
    }

    -- init
    naughty.layout.box {
        notification = n,
        type = "notification",
        shape = helpers.rrect(beautiful.rounded),
        widget_template = {
            {
                {
                    {
                        {
                            {
                                app_icon_n,
                                notif_info,
                                spacing = dpi(10),
                                layout = wibox.layout.fixed.horizontal
                            },
                            {
                                {
                                    title_n,
                                    message_n,
                                    layout = wibox.layout.fixed.vertical,
                                    spacing = dpi(3)
                                },
                                margins = { left = dpi(6) },
                                widget = wibox.container.margin
                            },
                            layout = wibox.layout.fixed.vertical,
                            spacing = dpi(16)
                        },
                        nil,
                        image_n,
                        layout = wibox.layout.align.horizontal,
                        expand = "none"
                    },
                    {
                        { actions, layout = wibox.layout.fixed.vertical },
                        margins = { top = dpi(20) },
                        visible = n.actions and #n.actions > 0,
                        widget = wibox.container.margin
                    },
                    layout = wibox.layout.fixed.vertical
                },
                margins = dpi(18),
                widget = wibox.container.margin
            },
            widget = wibox.container.background,
            forced_width = dpi(350),
            shape = helpers.rrect(beautiful.rounded),
            id = 'background_role',
            bg = beautiful.alpha
        }
    }

    -- don't show notification when dnd is on or dash is visible
    if _G.awesome_dnd_state or notifications_llc.visible or _G.lockscreen then
        naughty.destroy_all_notifications(nil, 1)
    end
end)
