-- a minimal bar
-- ~~~~~~~~~~~~~
-- requirements
-- ~~~~~~~~~~~~
local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local helpers = require("helpers")
local dpi = beautiful.xresources.apply_dpi
local button_creator = require("helpers.widgets.create_button")

-- misc/vars
-- ~~~~~~~~~

-- connect to screen
-- ~~~~~~~~~~~~~~~~~
awful.screen.connect_for_each_screen(function(s)
    -- screen width
    local screen_width = s.geometry.width
    local screen_height = s.geometry.height

    -- widgets
    -- ~~~~~~~

    -- taglist
    local taglist = require("layout.bar.taglist")(s)
    local tasklist = require("layout.bar.tasklist")(s)

    -- tag_label
    local tag_label = wibox.widget {
        widget = wibox.widget.textbox,
        markup = s.selected_tag.name,
        font = beautiful.icon_var .. "17",
        forced_height = beautiful.bar_size,
        forced_width = beautiful.bar_size,
        align = "center",
        valign = "center"
    }

    s:connect_signal("tag::history::update", function()
        if s and s.selected_tag then
            tag_label.markup = s.selected_tag.name
            s.tagslist_popup.visible = true

            s.hide_tagslist()
        end
    end)

    local dashboard_button = button_creator(tag_label, beautiful.alpha,
        beautiful.bg_2, {
            top = dpi(4),
            bottom = dpi(4),
            left = dpi(0),
            right = dpi(0)
        }, 0,
        beautiful.fg_color .. "33",
        gears.shape.rectangle)

    dashboard_button:buttons {
        gears.table.join(awful.button({}, 1, function() cc_toggle(s) end))
    }
    -- cc

    -- cc
    local notification_center = wibox.widget {
        markup = "󰚈",
        font = beautiful.icon_var .. "17",
        valign = "center",
        align = "center",
        widget = wibox.widget.textbox
    }

    local notification_center_button = button_creator(notification_center,
        beautiful.alpha,
        beautiful.bg_2, {
            top = dpi(4),
            bottom = dpi(4),
            left = dpi(10),
            right = dpi(10)
        }, 0, beautiful.fg_color .. "33", gears.shape.rectangle)

    notification_center_button:buttons {
        gears.table.join(awful.button({}, 1,
            function() notification_toggle() end))
    }
    -- cc

    local tray = wibox.widget.systray()
    tray:set_base_size(24)

    -- clock
    ---------------------------
    local clock = wibox.widget {
        format = "%a - %d %b %y, %I:%M %P",
        font = beautiful.font_var .. "Bold 10",
        valign = "center",
        align = "center",
        widget = wibox.widget.textclock
    }

    local clock_button = button_creator(clock, beautiful.alpha, beautiful.bg_2,
        { right = dpi(10), left = dpi(10) }, 0,
        beautiful.fg_color .. "33",
        gears.shape.rectangle)

    clock_button:buttons {
        gears.table.join(
            awful.button({}, 1, function()
                calendar_month_toggle(s)
            end),
            awful.button({}, 3, function()
                calendar_year_toggle(s)
            end)
        )
    }

    -- Eo clock
    ------------------------------------------

    s.tagslits_timer = nil

    s.hide_tagslist = function()
        if s.tagslits_timer == nil then
            s.tagslits_timer = gears.timer {
                timeout = 3,
                autostart = true,
                single_shot = true,
                callback = function()
                    s.tagslist_popup.visible = false
                    s.tagslits_timer = nil
                end
            }
        else
            s.tagslits_timer:again()
        end
    end

    -- wibar
    s.bar_left = awful.wibar({
        screen = s,
        visible = true,
        ontop = false,
        type = "dock",
        width = beautiful.bar_size,
        bg = beautiful.bg_color,
        height = screen_height - beautiful.bar_size
    })

    -- wibar
    s.bar_top = awful.wibar({
        screen = s,
        visible = true,
        ontop = false,
        type = "dock",
        width = screen_width,
        bg = beautiful.bg_color,
        height = beautiful.bar_size
    })

    s.tagslist_popup = awful.popup {
        widget = {
            {
                taglist,
                bg = beautiful.bg_color,
                clip = true,
                shape = helpers.rrect(beautiful.rounded),
                widget = wibox.widget.background
            },
            margins = { top = 0, bottom = dpi(40), left = 0, right = 0 },
            widget = wibox.container.margin
        },
        bg = '#00000000',
        -- border_width = 5,
        ontop = true,
        screen = s,
        placement = awful.placement.bottom + awful.placement.center +
            awful.placement.no_offscreen,
        visible = true
    }

    -- wibar placement
    -- awful.placement.top_left(s.bar_tag_label)

    awful.placement.left(s.bar_left, { margins = { top = s.bar_top.height } })

    s.bar_left:struts {
        left = s.bar_left.width + beautiful.useless_gap,
        top = s.bar_top.height
    }

    awful.placement.top(s.bar_top)
    s.bar_top:struts {
        top = s.bar_top.height + beautiful.useless_gap,
        left = s.bar_left.width
    }

    -- bar setup
    -- s.bar_tag_label:setup {
    --     tag_label,
    --     expand = "fill",
    --     layout = wibox.container.place
    -- }

    s.bar_top:setup {
        {
            dashboard_button,
            clock_button,
            {
                { tray, widget = wibox.container.place },
                notification_center_button,
                layout = wibox.layout.fixed.horizontal,
                spacing = dpi(12)
            },
            layout = wibox.layout.align.horizontal,
            expand = "none"
        },
        layout = wibox.container.margin,
        margins = { right = dpi(6) }
    }

    s.bar_left:setup {
        {
            tasklist,
            layout = wibox.layout.align.vertical,
            expand = "inside"
        },
        layout = wibox.container.margin,
        margins = { top = dpi(6), bottom = dpi(6) }
    }
end)
