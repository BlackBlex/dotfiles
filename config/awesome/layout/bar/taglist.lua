-- Taglist widget
-- ~~~~~~~~~~~~~~
-- requirements
-- ~~~~~~~~~~~~
local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

-- default modkey
local modkey = "Mod4"

local get_taglist = function(s)
    -- Taglist buttons
    local taglist_buttons = gears.table.join(
        awful.button({}, 1,
            function(t) t:view_only() end),
        awful.button({ modkey }, 1, function(t)
            if client.focus then client.focus:move_to_tag(t) end
        end), awful.button({}, 3, awful.tag.viewtoggle),
        awful.button({ modkey }, 3, function(t)
            if client.focus then client.focus:toggle_tag(t) end
        end), awful.button({}, 4, function(t)
            awful.tag.viewnext(t.screen)
        end), awful.button({}, 5, function(t)
            awful.tag.viewprev(t.screen)
        end))

    local the_taglist = awful.widget.taglist {
        screen = s,
        filter = awful.widget.taglist.filter.all,
        -- style = { shape = gears.shape.circle },
        layout = wibox.layout.fixed.horizontal,
        widget_template = {
            {
                {
                    {
                        id = "text_role",
                        widget = wibox.widget.textbox,
                        font = beautiful.icon_var .. "20",
                        align = "center",
                        valign = "center"
                    },
                    margins = dpi(8),
                    widget = wibox.container.margin
                },
                widget = wibox.container.background
            },
            id = "background_role",
            widget = wibox.container.background,

            create_callback = function(self, c3, _)
                local old_cursor, old_wibox
                self:connect_signal('mouse::enter', function()
                    local w = mouse.current_wibox
                    if w then
                        old_cursor, old_wibox = w.cursor, w
                        w.cursor = 'hand2'
                    end
                end)
                self:connect_signal('mouse::leave', function()
                    if old_wibox then
                        old_wibox.cursor = old_cursor
                        old_wibox = nil
                    end
                end)
            end,

            -- update_callback = function(self, c3, _)
            -- 	self:connect_signal('mouse::enter', function()
            -- 		self.bg = beautiful.accent
            --     end)

            --     self:connect_signal('mouse::leave', function()
            -- 		if c3.selected then
            -- 			self.bg = beautiful.bg_3
            -- 		else
            -- 			self.bg = beautiful.bg_color
            -- 		end
            -- 	end)
            -- end
        },

        buttons = taglist_buttons
    }

    return the_taglist
end

return get_taglist
