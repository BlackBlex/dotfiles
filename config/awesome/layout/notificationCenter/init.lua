-- notifs ig
-- ~~~~~~~~~
-- requirements
-- ~~~~~~~~~~~~
local awful = require("awful")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi
local wibox = require("wibox")
local gears = require("gears")
local helpers = require("helpers")

-- Dashboard
-- ~~~~~~~~~

-- a random name
notifications_llc = wibox({
    type = "dock",
    -- shape = helpers.rrect(beautiful.rounded),
    screen = screen.primary,
    height = screen.primary.geometry.height - (beautiful.bar_size * 2),
    width = dpi(420),
    bg = beautiful.bg_2,
    ontop = true,
    visible = false,
    border_width = dpi(2),
    border_color = beautiful.bg_3,
    shape = helpers.infobubble(10, 15, dpi(385))
})

-- Animations
-- ~~~~~~~~~~
local hidden_pos = screen.primary.geometry.x
local visibl_pos = screen.primary.geometry.x - notifications_llc.width

notifications_llc.x = (visibl_pos + screen.primary.geometry.width) -
    (beautiful.useless_gap * 2.4)
notifications_llc.y = screen.primary.geometry.y + beautiful.bar_size

-- toggler
local notification_show = function()
    notifications_llc.visible = true
    -- notification_status = false
end

local notification_hide = function() notifications_llc.visible = false end

local notification_grabber = awful.keygrabber {
    auto_start = true,
    stop_event = 'release',
    keypressed_callback = function(self, mod, key, command)
        if key == 'Escape' then notification_toggle() end
    end
}

notification_toggle = function()
    if notifications_llc.visible then
        notification_hide()
        notification_grabber:stop()
    else
        notification_show()
        notification_grabber:start()
    end
end

-- widgets
-- ~~~~~~~

-- clear button
-------------------------------------------------------------------
local notifs_clear = require("layout.notificationCenter.notifs.clear")

notifs_clear:buttons(gears.table.join(awful.button({}, 1, function()
    _G.notif_center_reset_notifs_container()
end)))
--------------------------------------------------------------------

-- local cal = require("layout.notificationCenter.calendar")
local notifs = require("layout.notificationCenter.notifs.build")

local notifs_text = wibox.widget {
    widget = wibox.widget.textbox,
    text = "Notification center",
    font = beautiful.font_var .. "Bold 16",
    align = "left",
    valign = "center"
}

local content = wibox.widget {
    { notifs_text, nil, notifs_clear, layout = wibox.layout.align.horizontal },
    notifs,
    spacing = dpi(4),
    layout = wibox.layout.ratio.vertical
}

content:set_ratio(1, 0.06)
content:set_ratio(2, 0.94)

notifications_llc:setup {
    content,
    margins = { top = dpi(30), bottom = dpi(15), right = dpi(15), left = dpi(15) },
    widget = wibox.container.margin
}
