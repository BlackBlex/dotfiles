-- requirements
-- ~~~~~~~~~~~~
local awful = require "awful"
local beautiful = require "beautiful"
local gears = require "gears"
local wibox = require "wibox"
local dpi = beautiful.xresources.apply_dpi

local pointer = 0
local min_widgets = 5
local carousel = false


local box_content = wibox.widget {
	spacing = 10,
	layout = wibox.layout.fixed.vertical
}

box_content:connect_signal("button::press", function(_,_,_,button)
    if carousel then
        if button == 4 then -- up scrolling
            local cnt = #box_content.children
            local first_widget = box_content.children[1]
            box_content:insert(cnt+1, first_widget)
            box_content:remove(1)
        elseif button == 5 then -- down scrolling
            local cnt = #box_content.children
            local last_widget = box_content.children[cnt]
            box_content:insert(1, last_widget)
            box_content:remove(cnt+1)
        end
    else
        if button == 4 then -- up scrolling
            if pointer < #box_content.children and ((#box_content.children - pointer) >= min_widgets) then
                pointer = pointer + 1
                box_content.children[pointer].visible = false
            end
        elseif button == 5 then -- down scrolling
            if pointer > 0 then
                box_content.children[pointer].visible = true
                pointer = pointer - 1
            end
        end
    end
end)

return box_content
