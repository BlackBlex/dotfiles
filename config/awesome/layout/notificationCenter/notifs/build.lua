-- notification center
-- ~~~~~~~~~~~~~~~~~~


-- requirements
-- ~~~~~~~~~~~~
local beautiful = require("beautiful")
local naughty = require("naughty")
local wibox = require("wibox")
local helpers = require("helpers")
local dpi = beautiful.xresources.apply_dpi

-- empty box
-------------------------------------------------------------------
local notification_empty = require("layout.notificationCenter.notifs.notifs-empty")
--------------------------------------------------------------------

-- Create notif
--------------------------------------------------------------
local create_notif = require("layout.notificationCenter.notifs.creator")
---------------------------------------------------------------

-- Notifs container
--------------------------------------------------------------------
local notifs_container = require("layout.notificationCenter.notifs.container")
--------------------------------------------------------------------

-- Helper functions
-------------------------------
local remove_notifs_empty = true

notif_center_reset_notifs_container = function()
  notifs_container:reset(notifs_container)
  notifs_container:insert(1, notification_empty)
  remove_notifs_empty = true
end

notif_center_remove_notif = function(box)
  notifs_container:remove_widgets(box)

  if #notifs_container.children == 0 then
    notifs_container:insert(1, notification_empty)
    remove_notifs_empty = true
  end
end
--------------------------------------------------

-- update functions
----------------------------------------
notifs_container:insert(1, notification_empty)

naughty.connect_signal("request::display", function(n)
  if #notifs_container.children == 1 and remove_notifs_empty then
    notifs_container:reset(notifs_container)
    remove_notifs_empty = false
  end

  local appicon = n.icon or n.app_icon
  if not appicon then
    appicon = beautiful.notification_icon
  end

  notifs_container:insert(1, create_notif(appicon, n))
end)
-------------------------------------------------------

-----------------------------------------------
-----------------------------------------------
return wibox.widget {
	notifs_container,
	widget = wibox.container.margin
}
-----------------------------------------------
-----------------------------------------------
