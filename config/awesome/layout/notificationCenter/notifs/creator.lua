-- requirements
local helpers = require("helpers")
local beautiful = require("beautiful")
local gears = require("gears")
local dpi = beautiful.xresources.apply_dpi
local awful = require("awful")
local wibox = require("wibox")
local naughty = require("naughty")
local clickable_container = require('mods.clickable-container')

-- table of icons
local app_icons = {
    ["brave"] = { icon = "󰊯" },
    ["discord"] = { icon = "󰙯" },
    ["music"] = { icon = "󰝚" },
    ["color picker"] = { icon = "󰏘" },
    ["screenshot"] = { icon = "󰊓" },
    ["notify-send"] = { icon = "󰂚" },
    ["notify-send.sh"] = { icon = "󰂚" },
    ["set theme"] = { icon = "󰉦" }
}

-- notification themselves
return function(icon, n, width)
    local time = os.date "%I:%M"

    -- app icon
    local app_icon
    local tolow = string.lower

    if app_icons[tolow(n.app_name)] then
        app_icon = app_icons[tolow(n.app_name)].icon
    else
        app_icon = "󰘔"
    end

    local action_button = {
        {
            {
                id = "text_role",
                font = beautiful.font_var .. "10",
                widget = wibox.widget.textbox
            },
            forced_height = dpi(40),
            widget = wibox.container.place
        },
        widget = clickable_container
    }

    -- action widget
    local action_widget = {
        {
            action_button,
            left = dpi(6),
            right = dpi(6),
            widget = wibox.container.margin
        },
        bg = beautiful.black,
        forced_height = dpi(30),
        shape = helpers.rrect(dpi(5)),
        widget = wibox.container.background
    }

    -- actions
    local actions = wibox.widget {
        notification = n,
        base_layout = wibox.widget {
            spacing = dpi(8),
            layout = wibox.layout.flex.horizontal
        },
        widget_template = action_widget,
        style = { underline_normal = false, underline_selected = true },
        widget = naughty.list.actions
    }

    local app_icon_n = wibox.widget {
        {
            font = beautiful.icon_var .. "10",
            markup = "<span foreground='" .. beautiful.black_color .. "'>" ..
                app_icon .. "</span>",
            align = "center",
            valign = "center",
            widget = wibox.widget.textbox
        },
        bg = beautiful.accent,
        widget = wibox.container.background,
        shape = gears.shape.circle,
        border_width = dpi(1),
        border_color = beautiful.ext_light_fg,
        forced_height = dpi(20),
        forced_width = dpi(20)
    }

    local message = wibox.widget {
        {
            {
                markup = helpers.colorize_text(
                    "<span weight='normal'>" .. n.message .. "</span>",
                    beautiful.ext_white_bg .. "BF"),
                font = beautiful.font_var .. " 11",
                align = "left",
                valign = "top",
                widget = wibox.widget.textbox
            },
            width = dpi(250),
            height = nil,
            widget = wibox.container.constraint
        },
        margins = { right = 15 },
        widget = wibox.container.margin
    }

    local title = wibox.widget {
        {
            {
                markup = helpers.colorize_text(n.title,
                    (beautiful.ext_light_fg or
                        beautiful.fg_color)),
                font = beautiful.font_var .. " Bold 11",
                align = "left",
                valign = "center",
                widget = wibox.widget.textbox
            },
            step_function = wibox.container.scroll.step_functions.waiting_nonlinear_back_and_forth,
            speed = 50,
            forced_width = dpi(200),
            widget = wibox.container.scroll.horizontal
        },
        nil,
        {
            {
                {
                    widget = wibox.widget.separator,
                    shape = gears.shape.circle,
                    valign = "center",
                    align = "center",
                    forced_width = dpi(8),
                    forced_height = dpi(8),
                    color = (beautiful.ext_light_fg or beautiful.fg_color) ..
                        "E6"
                },
                margins = { left = dpi(15), right = dpi(15) },
                widget = wibox.container.margin
            },
            {
                markup = helpers.colorize_text("  " .. time,
                    (beautiful.ext_light_fg or
                        beautiful.fg_color) ..
                    "E6"),
                font = beautiful.font_var .. "10",
                align = "left",
                valign = "center",
                widget = wibox.widget.textbox
            },
            layout = wibox.layout.align.horizontal
        },
        layout = wibox.layout.align.horizontal
    }

    local image = wibox.widget {
        {
            {
                image = icon,
                resize = true,
                clip_shape = gears.shape.circle,
                halign = "center",
                valign = "center",
                widget = wibox.widget.imagebox
            },
            strategy = "exact",
            height = dpi(50),
            width = dpi(50),
            widget = wibox.container.constraint
        },
        {
            nil,
            nil,
            {
                nil,
                nil,
                app_icon_n,
                layout = wibox.layout.align.horizontal,
                expand = "none"
            },
            layout = wibox.layout.align.vertical,
            expand = "none"
        },
        layout = wibox.layout.stack
    }

    local box = {}

    box = wibox.widget {
        {
            {
                {
                    image,
                    {
                        nil,
                        {
                            title,
                            message,
                            layout = wibox.layout.fixed.vertical,
                            spacing = dpi(3)
                        },
                        layout = wibox.layout.align.vertical,
                        expand = "none"
                    },
                    layout = wibox.layout.fixed.horizontal,
                    spacing = dpi(16)
                },
                {
                    { actions, layout = wibox.layout.fixed.vertical },
                    visible = n.actions and #n.actions > 0,
                    widget = wibox.container.margin
                },
                spacing = dpi(10),
                widget = wibox.layout.fixed.vertical
            },
            margins = dpi(15),
            widget = wibox.container.margin
        },
        widget = wibox.container.background,
        bg = beautiful.bg_3 .. "E6",
        shape = helpers.rrect(dpi(6))

    }

    -- setup
    box:buttons(gears.table.join(awful.button({}, 1, function()
        _G.notif_center_remove_notif(box)
    end)))

    return box
end
