-- requirements
local helpers = require("helpers")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi
local wibox = require("wibox")

local content = wibox.widget {
    {
        {
            widget = wibox.widget.textbox,
            markup = helpers.colorize_text("No notifications",
                                           (beautiful.grey_color)),
            font = beautiful.font_var .. "18",
            valign = "center",
            align = "center"
        },
        {
            widget = wibox.widget.textbox,
            markup = helpers.colorize_text("󰇸", (beautiful.grey_color)),
            font = beautiful.font_var .. "64",
            valign = "center",
            align = "center"
        },
        spacing = dpi(10),
        layout = wibox.layout.fixed.vertical
    },
    margins = dpi(20),
    widget = wibox.container.margin
}

local separator_for_empty_msg = wibox.widget {
    orientation = 'vertical',
    opacity = 0,
    widget = wibox.widget.separator
}

local centered_empty_notifbox = wibox.widget {
    separator_for_empty_msg,
    content,
    separator_for_empty_msg,
    expand = 'none',
	shape = helpers.rrect(beautiful.rounded),
    layout = wibox.layout.align.vertical
}

return centered_empty_notifbox
