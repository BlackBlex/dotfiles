-- Catppuccin frappe theme colors
-- ~~~~~~~~~~~~~~~~~

local colors = {}

colors.black = "#303446"
colors.red = "#E78284"
colors.green = "#A6D189"
colors.yellow = "#E5C890"
colors.orange = "#ef9f76"                      -- Peach
colors.blue = "#8CAAEE"
colors.purple = "#ca9ee6"                      -- Mauve
colors.cyan = "#81C8BE"                        -- Teal
colors.grey = "#414559"                        -- Surface0

colors.foreground = "#B5BFE2"                  -- Subtext 1
colors.background = colors.black .. "BA"       -- Base more BA
colors.background_trans = colors.black .. "C8" -- Base more C8
colors.background_2 = "#292c3c"                -- Mantle
colors.background_3 = "#232634"                -- Crust

colors.white = "#C6D0F5"                       -- Text
colors.alpha = "#00000000"
colors.transparency_20 = "#3307080a"

return colors
