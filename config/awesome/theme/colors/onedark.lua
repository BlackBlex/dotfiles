-- OneDark theme colors
-- ~~~~~~~~~~~~~~~~~

local colors = {}

colors.black_light = "#262b33"
colors.black = "#0e1013"
colors.red_light = "#e86973"
colors.red = "#e55561"
colors.green_light = "#9bc47c"
colors.green = "#8ebd6b"
colors.yellow_light = "#e5c07c"
colors.yellow = "#e2b86b"
colors.orange_light = "#d29d6b"
colors.orange = "#cc9057"
colors.blue_light = "#61afef"
colors.blue = "#4fa6ed"
colors.purple_light = "#c67add"
colors.purple = "#bf68d9"
colors.cyan_light = "#5db9c4"
colors.cyan = "#48b0bd"
colors.grey_light = "#7a818e"
colors.grey = "#535965"

colors.foreground = "#a0a8b7"
colors.background_trans = "#1f2329BA"
colors.background = "#1f2329C8"
colors.background_2 = "#30363f"
colors.background_3 = "#323641"

colors.white = "#ececec"
colors.alpha = "#00000000"
colors.transparency_20 = "#3307080a"

return colors
