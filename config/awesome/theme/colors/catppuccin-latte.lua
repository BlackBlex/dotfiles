-- Catppuccin latte theme colors
-- ~~~~~~~~~~~~~~~~~

local colors = {}

colors.black = "#eff1f5"
colors.red = "#d20f39"
colors.green = "#40a02b"
colors.yellow = "#df8e1d"
colors.orange = "#fe640b"                      -- Peach
colors.blue = "#1e66f5"
colors.purple = "#8839ef"                      -- Mauve
colors.cyan = "#179299"                        -- Teal
colors.grey = "#ccd0da"                        -- Surface0

colors.foreground = "#5c5f77"                  -- Subtext 1
colors.background_trans = colors.black .. "BA" -- Base more BA
colors.background = colors.black .. "C8"       -- Base more C8
colors.background_2 = "#e6e9ef"                -- Mantle
colors.background_3 = "#dce0e8"                -- Crust

colors.white = "#4c4f69"                       -- Text
colors.alpha = "#00000000"
colors.transparency_20 = "#3307080a"

return colors
