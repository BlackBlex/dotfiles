-- OneDark theme colors
-- ~~~~~~~~~~~~~~~~~

local colors = {}

colors.black_light = "#4f5169"
colors.black = "#21222c"
colors.red_light = "#ff6e6e"
colors.red = "#ff5555"
colors.green_light = "#69ff94"
colors.green = "#50fa7b"
colors.yellow_light = "#ffffa5"
colors.yellow = "#f1fa8c"
colors.orange_light = "#fec990"
colors.orange = "#ffb86c"
colors.blue_light = "#61afef"
colors.blue = "#4fa6ed"
colors.purple_light = "#d6acff"
colors.purple = "#bd93f9"
colors.cyan_light = "#a4ffff"
colors.cyan = "#8be9fd"
colors.grey_light = "#9698b0"
colors.grey = "#737696"

colors.foreground = "#f8f8f2"
colors.background_trans = "#282a36BA"
colors.background = "#282a36C8"
colors.background_2 = "#44475a"
colors.background_3 = "#4D4D4D"

colors.white = "#ffffff"
colors.alpha = "#00000000"
colors.transparency_20 = "#3307080a"

return colors
