-- Catppuccin mocha theme colors
-- ~~~~~~~~~~~~~~~~~

local colors = {}

colors.black = "#1e1e2e"
colors.red = "#f38ba8"
colors.green = "#a6e3a1"
colors.yellow = "#f9e2af"
colors.orange = "#fab387"                      -- Peach
colors.blue = "#89b4fa"
colors.purple = "#cba6f7"                      -- Mauve
colors.cyan = "#94e2d5"                        -- Teal
colors.grey = "#a6adc8"                        -- Surface0

colors.foreground = "#bac2de"                  -- Subtext 1
colors.background_trans = colors.black .. "BA" -- Base more BA
colors.background = colors.black .. "C8"       -- Base more C8
colors.background_2 = "#181825"                -- Mantle
colors.background_3 = "#11111b"                -- Crust

colors.white = "#cdd6f4"                       -- Text
colors.alpha = "#00000000"
colors.transparency_20 = "#3307080a"

return colors
