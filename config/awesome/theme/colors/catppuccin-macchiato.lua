-- Catppuccin macchiato theme colors
-- ~~~~~~~~~~~~~~~~~

local colors = {}

colors.black = "#24273a"
colors.red = "#ed8796"
colors.green = "#a6da95"
colors.yellow = "#eed49f"
colors.orange = "#f5a97f"                      -- Peach
colors.blue = "#8aadf4"
colors.purple = "#c6a0f6"                      -- Mauve
colors.cyan = "#8bd5ca"                        -- Teal
colors.grey = "#363a4f"                        -- Surface0

colors.foreground = "#b8c0e0"                  -- Subtext 1
colors.background_trans = colors.black .. "BA" -- Base more BA
colors.background = colors.black .. "C8"       -- Base more C8
colors.background_2 = "#1e2030"                -- Mantle
colors.background_3 = "#181926"                -- Crust

colors.white = "#cad3f5"                       -- Text
colors.alpha = "#00000000"
colors.transparency_20 = "#3307080a"

return colors
