--[[
    A random rice. i guess.
    source: https://github.com/saimoomedits/dotfiles
]]
pcall(require, "luarocks.loader")
local awful = require("awful")

-- Autostart.sh load default apps and things
awful.spawn.with_shell("~/autostart.sh awesome", false)

-- home variable 🏠
HOME_VAR = os.getenv("HOME")

-- user preferences ⚙️
USER_LIKES = {
    -- aplications
    term = "kitty",
    editor = os.getenv("EDITOR") or "nano",
    code = "vscodium",
    web = "brave",
    music = "kitty --name 'ncspot' --class 'ncspot' ncspot",
    files = "kitty --name 'ranger' --class 'ranger' ranger",

    -- your profile
    username = os.getenv("USER"):gsub("^%l", string.upper),
    userdesc = "@AwesomeWM"
}

-- theme 🖌️
require("theme")

-- configs ⚙️
require("config")

-- miscallenous ✨
require("misc")

-- signals 📶
require("signal")

-- ui elements 💻
require("layout")
