-------------------------------------------------
-- Bookmark Widget for Awesome Window Manager

-- @author Pavel Makhov
-- @copyright 2020 Pavel Makhov
-------------------------------------------------

local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi

return function (iconOrText, font, menu_items, callback_press, width, bg_normal, bg_press, offsets, border_width, border_color, shape_spe)
	--- Widget to add to the wibar
	local list = wibox.widget{
		markup = iconOrText,
		font = font,
		valign = "center",
		align = "center",
		widget = wibox.widget.textbox
	}

	local popup = awful.popup {
		ontop = true,
		visible = false,
    	shape = shape_spe or gears.shape.rounded_bar,
		border_width = border_width or dpi(0),
		border_color = border_color or bg_press or beautiful.alpha,
		maximum_width = 400,
		minimum_width = width or 0,
		offset = offsets or { y = 5 },
		widget = {}
	}

	local rows = { layout = wibox.layout.fixed.vertical }

	for _, item in ipairs(menu_items) do
		local row = nil

		if item.icon_name then
			row = wibox.widget {
				{
					{
						{
							image = item.icon_name,
							forced_width = 16,
							forced_height = 16,
							widget = wibox.widget.imagebox
						},
						{
							text = item.name,
							font = beautiful.font_var .. "11",
							widget = wibox.widget.textbox
						},
						spacing = 12,
						layout = wibox.layout.fixed.horizontal
					},
					margins = 8,
					widget = wibox.container.margin
				},
				bg = bg_normal or beautiful.bg_3,
				widget = wibox.container.background
			}
		else
			row = wibox.widget {
				{
					{
						{
							text = item.name,
							font = beautiful.font_var .. "11",
							widget = wibox.widget.textbox
						},
						spacing = 12,
						layout = wibox.layout.fixed.horizontal
					},
					margins = 8,
					widget = wibox.container.margin
				},
				bg = bg_normal or beautiful.bg_3,
				widget = wibox.container.background
			}
		end

		-- Change item background on mouse hover
		row:connect_signal("mouse::leave", function(c)
			c:set_bg(bg_normal or beautiful.bg_3)
			c:set_fg(beautiful.fg_color)
		end)
		row:connect_signal("mouse::enter", function(c)
			c:set_bg(bg_press or beautiful.accent)
			c:set_fg(beautiful.bg_color)
		end)

		-- Change cursor on mouse hover
		local old_cursor, old_wibox
		row:connect_signal("mouse::enter", function()
			local wb = mouse.current_wibox
			old_cursor, old_wibox = wb.cursor, wb
			wb.cursor = "hand2"
		end)
		row:connect_signal("mouse::leave", function()
			if old_wibox then
				old_wibox.cursor = old_cursor
				old_wibox = nil
			end
		end)

		-- Mouse click handler
		row:buttons(
			awful.util.table.join(
				awful.button({}, 1, function()
					popup.visible = not popup.visible
					callback_press(item)
				end)
			)
		)

		-- Insert created row in the list of rows
		table.insert(rows, row)
	end

	-- Add rows to the popup
	popup:setup(rows)

	list.close = function()
		popup.visible = false
	end

	list.open = function ()
		popup.visible = true
	end

	list.toggle = function()
		if popup.visible then
			popup.visible = not popup.visible
		else
			popup:move_next_to(mouse.current_widget_geometry)
		end
	end

	-- Mouse click handler
	list:buttons(
		awful.util.table.join(
			awful.button({}, 1, function()
				list.toggle()
		end))
	)

	return list
end
