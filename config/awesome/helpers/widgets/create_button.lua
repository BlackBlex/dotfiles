-- helper function to create buttons
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- uses rubato for smoooth animations


-- requirements
-- ~~~~~~~~~~~~
local beautiful = require ("beautiful")
local gears = require ("gears")
local wibox = require ("wibox")
local rubato = require("mods.rubato")
local dpi = beautiful.xresources.apply_dpi


return function (widget, normal_bg, press_color, margins, border_width, border_color, shape_spe, isToggle, toggle_color)
	local state = false

	if isToggle == nil then
		isToggle = false
	end

    -- containers
    local circle_animate = wibox.widget{
    	widget = wibox.container.background,
    	shape = shape_spe or gears.shape.rounded_bar,
    	bg = press_color or beautiful.accent,
    }

    local mainbox = wibox.widget {
        {
            circle_animate,
            {
                widget,
                margins = margins or dpi(15),
                widget = wibox.container.margin
            },
            layout = wibox.layout.stack
        },
      bg = normal_bg or beautiful.bg_3,
      shape = shape_spe or gears.shape.rounded_bar,
      border_width = border_width or dpi(0),
      border_color = border_color or press_color or beautiful.alpha,
      widget = wibox.container.background,
    }

    local animation_button_opacity = rubato.timed{
        pos = 0,
        rate = 60,
        intro = 0.06,
        duration = 0.2,
        awestore_compat = true,
        subscribed = function(pos)
		    circle_animate.opacity = pos
        end
    }

	mainbox.set_state = function (value)
		state = value

		if isToggle then
			if state then
				mainbox.bg = toggle_color
				-- animation_button_opacity:set(1)
			else
				mainbox.bg = normal_bg or beautiful.bg_3
				-- animation_button_opacity:set(0)
			end
		end
	end

	mainbox.get_state = function ()
		return state
	end

	local old_cursor, old_wibox

    mainbox:connect_signal("mouse::enter", function()
		local w = mouse.current_wibox
		if w then
			old_cursor, old_wibox = w.cursor, w
			w.cursor = 'hand2'
		end
        animation_button_opacity:set(0.9)
	end)

	mainbox:connect_signal("mouse::leave", function()
		if old_wibox then
			old_wibox.cursor = old_cursor
			old_wibox = nil
		end

		if isToggle and state then
			animation_button_opacity:set(1)
			return
		end

        animation_button_opacity:set(0.0)
	end)



    -- add buttons and commands
    mainbox:connect_signal("button::press", function()
        animation_button_opacity:set(1)

		if isToggle then
			state = not state
		end
	end)

    mainbox:connect_signal("button::release", function()
		gears.timer {
			timeout   = 0.1,
			autostart = true,
			single_shot = true,
			callback  = function()
				animation_button_opacity:set(0.5)
			end
		}
	end)




    return mainbox

end
