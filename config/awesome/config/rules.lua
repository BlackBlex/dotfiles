-- rules
-- ~~~~~
-- requirements
-- ~~~~~~~~~~~~
local awful = require("awful")
local ruled = require("ruled")
local readwrite = require("misc.scripts.read_writer")

-- Configurations
-- ~~~~~~~~~~~~~~
local has_monitor = false
local names = { "", "", "", "", "", "", "", "" }
local names_two = { "", "", "", "" }

-- connect to signal
ruled.client.connect_signal("request::rules", function()
    -- Global
    ruled.client.append_rule {
        id = "global",
        rule = {},
        properties = {
            focus = awful.client.focus.filter,
            raise = true,
            size_hints_honor = false,
            screen = awful.screen.preferred,
            titlebars_enabled = false,
            placement = awful.placement.centered + awful.placement.no_overlap +
                awful.placement.no_offscreen
        }
    }

    -- tasklist order
    ruled.client.append_rule {
        id         = "tasklist_order",
        rule       = {},
        properties = {},
        callback   = awful.client.setslave
    }

    -- Floating
    ruled.client.append_rule {
        id = "floating",
        rule_any = {
            class = {
                "Pavucontrol", "Nm-connection-editor", "Blueman-manager",
                "Colorgrab", "Lxappearance", "Betterbird:Msgcompose", "URxvt",
                "mpv"
            },
            name = { "Event Tester", "dragon", "Save Video" },
            instance = { "mpv-youtube", "yad" },
            role = {
                "AlarmWindow", "ConfigManager", "pop-up", "GtkFileChooserDialog"
            },
            type = { "dialog" }
        },
        properties = {
            above = true,
            floating = true,
            placement = awful.placement.centered
        }
    }

    -- Specify floating
    ruled.client.append_rule {
        id = "floatingCenter",
        rule_any = { class = { "ranger", "nvim" } },
        properties = {
            floating = true,
            width = 1200,
            height = 720,
            placement = awful.placement.centered
        }
    }

    ruled.client.append_rule {
        id = "floatingDocumment",
        rule_any = { class = { "Zathura" } },
        properties = {
            floating = true,
            width = 567,
            height = 734,
            placement = awful.placement.centered
        }
    }

    ruled.client.append_rule {
        id = "floatingImage",
        rule_any = { class = { "feh" } },
        properties = {
            floating = true,
            width = 800,
            height = 600,
            placement = awful.placement.centered
        }
    }

    ruled.client.append_rule {
        id = "floatingPicInPic",
        rule_any = {
            name = { "Picture-in-Picture", "Picture in picture" },
            instance = { "mpv-youtube" }
        },
        properties = {
            floating = true,
            placement = awful.placement.bottom_right +
                awful.placement.honor_padding,
            width = 400,
            height = 270,
            ontop = true,
            sticky = true,
            above = true
        }
    }

    ruled.client.append_rule {
        id = "mediaviewer_maximizedz",
        rule_any = { name = { "Media viewer" } },
        properties = {
            maximized = true,
            placement = awful.placement.no_overlap +
                awful.placement.no_offscreen
        }
    }

    -- Borders
    -- ruled.client.append_rule {
    --     id = "borders",
    --     rule_any = {type = {"normal", "dialog"}},
    --     except_any = {
    --         role = {"Popup"},
    --         type = {"splash"},
    --         name = {"^discord.com is sharing your screen.$"}
    --     },
    --     properties = {
    --         border_width = beautiful.border_width,
    --         border_color = beautiful.border_normal
    --     }
    -- }

    -- Center Placement
    -- ruled.client.append_rule {
    --     id = "center_placement",
    --     rule_any = {
    --         type = {"dialog"},
    --         class = {"Steam", "discord", "markdown_input", "nemo", "thunar" },
    --         instance = {"markdown_input",},
    --         role = {"GtkFileChooserDialog"}
    --     },
    --     properties = {placement = awful.placement.center}
    -- }

    -- Titlebar rules
    -- ruled.client.append_rule {
    --     id = "titlebars",
    --     rule_any = {
    --         type = {
    --         "dialog",
    --         "splash"
    --     },
    --     name = {
    --         "^discord.com is sharing your screen.$",
    --         "file_progress"
    --     }
    -- },
    --     properties = {titlebars_enabled = false}
    -- }

    -- Music client
    -- ruled.client.append_rule {
    --     rule_any = {class = {"music"}, instance = {"music"}},
    --     properties = {
    --         floating = true,
    --         width = 650,
    -- 		height = 550,
    --     },
    -- }

    if has_monitor then
        ruled.client.append_rule {
            rule_any = { class = { "VSCodium" } },
            properties = { screen = 2, tag = names_two[2], switchtotag = true }
        }

        ruled.client.append_rule {
            rule_any = { class = { "GitKraken" } },
            properties = { screen = 2, tag = names_two[3], switchtotag = true }
        }

        ruled.client.append_rule {
            rule_any = { class = { "KeePassXC", "cloud-drive-ui" } },
            except_any = { name = { "KeePassXC -  Access Request" } },
            properties = { screen = 2, tag = names_two[4], switchtotag = true }
        }
    else
        ruled.client.append_rule {
            rule_any = { class = { "VSCodium" } },
            properties = { screen = 1, tag = names[2], switchtotag = true }
        }

    ruled.client.append_rule {
            rule_any = { class = { "GitKraken" } },
            properties = { screen = 1, tag = names[3], switchtotag = true }
        }

        ruled.client.append_rule {
            rule_any = { class = { "KeePassXC", "cloud-drive-ui" } },
            except_any = { name = { "KeePassXC -  Access Request" } },
            properties = { screen = 1, tag = names[8], switchtotag = true }
        }
    end

    ruled.client.append_rule {
        rule_any = { -- class = {"firefox", "brave%-browser", "Brave%-browser"}, name = {".*Firefox"}
            instance = { "brave%-browser" }
        },
        properties = { screen = 1, tag = names[1], switchtotag = true }
    }

    ruled.client.append_rule {
        rule_any = { class = { "Godot" } },
        properties = { screen = 1, tag = names[2], switchtotag = true }
    }

    ruled.client.append_rule {
        rule_any = { class = { "TeamViewer" }, instance = { "office.com" } },
        properties = { screen = 1, tag = names[4], switchtotag = true }
    }

    ruled.client.append_rule {
        rule_any = {
            class = { "TelegramDesktop" },
            instance = { "web.whatsapp.com" }
        },
        properties = {
            screen = 1,
            tag = names[5],
            switchtotag = true,
            floating = false
        }
    }

    ruled.client.append_rule {
        rule_any = { class = { "Skype", "Microsoft Teams %- Preview" } },
        properties = { screen = 1, tag = names[6] }
    }

    ruled.client.append_rule {
        rule_any = { class = { "betterbird" }, name = { ".*Betterbird" } },
        properties = { screen = 1, tag = names[7] }
    }

    ruled.client.append_rule {
        rule_any = { class = { "Krita", "Soffice" }, name = { "LibreOffice" } },
        properties = { screen = 1, tag = names[8] }
    }
end)
