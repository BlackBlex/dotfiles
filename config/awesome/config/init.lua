require("config.keys")
require("config.rules")
require("config.tags")
require("config.other")

local awful = require("awful")
local readwrite = require("misc.scripts.read_writer")
local helpers = require("helpers")

local function save_current_tag()
    local tags = ""
    for s in screen do
        tags = string.format("%s%d,%s;", tags, s.index, s.selected_tag.name)
    end

    readwrite.write("last_ws_state", tags)
end

local function load_last_active_tag()
    local last_ws = readwrite.readall("last_ws_state")
    if last_ws ~= "" then
        local tags = {}
        local tmp = helpers.split(last_ws, ";")

        for k, v in ipairs(tmp) do
            local temp = helpers.split(v, ",")
            table.insert(tags, temp[1], temp[2])
        end

        for s in screen do
            local t = awful.tag.find_by_name(s, tags[s.index])

            if t then
                t:view_only()
                t.selected = true
            end
        end
    end
end

awesome.connect_signal("exit", function(c) save_current_tag() end)

awesome.connect_signal("startup", function(c) load_last_active_tag() end)
