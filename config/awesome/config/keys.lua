-- keybinds haha
-- ~~~~~~~~~~~~~
-- requirements
-- ~~~~~~~~~~~~
local awful = require("awful")
local hotkeys_popup = require("awful.hotkeys_popup")
-- local lmachi        = require("mods.layout-machi")
local bling = require("mods.bling")
local misc = require("misc")
require("layout.lockscreen").init()

-- vars/misc
-- ~~~~~~~~~

-- modkey
local modkey = "Mod5"

-- modifer keys
local shift = "Shift"
local ctrl = "Control"
local alt = "Mod1"

-- Configurations
-- ~~~~~~~~~~~~~~

-- mouse keybindings
-- awful.mouse.append_global_mousebindings({
--     awful.button({ }, 4, awful.tag.viewprev),
--     awful.button({ }, 5, awful.tag.viewnext),
-- })

require("config.key.brave")

-- launchers
awful.keyboard.append_global_keybindings({

    awful.key({ modkey }, "Return", function() awful.spawn(USER_LIKES.term) end,
        { description = "open terminal", group = "launcher" }),

    awful.key({ modkey }, "s", function() awful.spawn(USER_LIKES.music) end,
        { description = "launch music client", group = "launcher" }),

    awful.key({ modkey }, "b",
        function() awful.spawn.with_shell(USER_LIKES.web) end,
        { description = ";b open web browser", group = "launcher" }),

    awful.key({ modkey }, "e", function() awful.spawn(misc.rofiCommand) end,
        { description = "open rofi", group = "launcher" }),

    awful.key({ modkey }, "p", function()
        awful.spawn(HOME_VAR .. "/.config/awesome/scripts/picker", false)
    end, { description = "exec color picker", group = "launcher" }),

    awful.key({ modkey }, "a", function() cc_toggle(screen.primary) end,
        { description = "toggle control center", group = "launcher" }),

    awful.key({ modkey, ctrl }, "b", function()
        if control_c.visible then cc_toggle() end
        require("layout.lockscreen").init()
        lock_screen_show()
    end, { description = "show lockscreen", group = "launcher" }),

    awful.key({ modkey, ctrl }, "p", function()
        if control_c.visible then cc_toggle() end
        require("mods.exit-screen")
        awesome.emit_signal('module::exit_screen:show')
    end, { description = "show exit screen", group = "launcher" }),

    awful.key({ modkey }, "f", function() awful.spawn(USER_LIKES.files) end,
        { description = "open ranger", group = "launcher" })

})

-- Mouse fake
awful.keyboard.append_global_keybindings({
    -- awful.key({modkey, ctrl}, "KP_7", function()
    -- end, {description = "Send a mouse left click", group = "Mouse"}),
    -- awful.key({modkey, ctrl}, "KP_9", function() root.fake_input("button_press", 3)
    -- 	root.fake_input("button_release", 3)
    -- end, {description = "Send a mouse right click", group = "Mouse"}),
    -- awful.key({modkey, ctrl}, "KP_8", function()
    -- 	mouse.coords {x = mouse.coords().x + 10, y = mouse.coords().y}
    -- end, {description = "Move cursor 10px up", group = "Mouse"}),

    awful.key {
        modifiers = { modkey },
        keygroup = "numpad",
        description = "Send mouse fake moves",
        group = "Mouse",
        -- on_release = function(index)
        -- 	if index == 7 then
        -- 		-- root.fake_input("button_press", 1)
        -- 		root.fake_input("button_release", 1)
        -- 	end
        -- 	if index == 5 then
        -- 		-- root.fake_input("button_press", 2)
        -- 		root.fake_input("button_release", 2)
        -- 	end
        -- 	if index == 9 then
        -- 		-- root.fake_input("button_press", 3)
        -- 		root.fake_input("button_release", 3)
        -- 	end
        -- end,
        on_press = function(index)
            if index == 1 then
                awful.util.spawn('xdotool sleep 0.25 click 1')
                -- root.fake_input("button_press", 1)
                -- root.fake_input("button_release", 1)
            end
            if index == 5 then
                awful.util.spawn('xdotool sleep 0.25 click 2')
                -- root.fake_input("button_press", 2)
                -- root.fake_input("button_release", 2)
            end
            if index == 3 then
                awful.util.spawn('xdotool sleep 0.25 click 3')
                -- root.fake_input("button_press", 3)
                -- root.fake_input("button_release", 3)
            end
            if index == 7 then
                awful.util.spawn('xdotool sleep 0.5 click 4')
                -- root.fake_input("button_press", 3)
                -- root.fake_input("button_release", 3)
            end
            if index == 9 then
                awful.util.spawn('xdotool sleep 0.5 click 5')
                -- root.fake_input("button_press", 3)
                -- root.fake_input("button_release", 3)
            end
            -- if index == 8 then mouse.coords {x = mouse.coords().x, y = mouse.coords().y - 10} end
            -- if index == 2 then mouse.coords {x = mouse.coords().x, y = mouse.coords().y + 10} end
            -- if index == 4 then mouse.coords {x = mouse.coords().x - 10, y = mouse.coords().y} end
            -- if index == 6 then mouse.coords {x = mouse.coords().x + 10, y = mouse.coords().y} end
            if index == 8 then root.fake_input("motion_notify", true, 0, -10) end
            if index == 2 then root.fake_input("motion_notify", true, 0, 10) end
            if index == 4 then root.fake_input("motion_notify", true, -10, 0) end
            if index == 6 then root.fake_input("motion_notify", true, 10, 0) end
        end
    },
    awful.key {
        modifiers = { modkey, ctrl },
        keygroup = "numpad",
        description = "Send mouse fake moves faster",
        group = "Mouse",
        on_press = function(index)
            if index == 1 then
                awful.util.spawn('xdotool sleep 0.25 click --repeat=2 1')
            end
            if index == 5 then
                awful.util.spawn('xdotool sleep 0.25 click --repeat=2 2')
            end
            if index == 3 then
                awful.util.spawn('xdotool sleep 0.25 click --repeat=2 3')
            end
            if index == 8 then root.fake_input("motion_notify", true, 0, -25) end
            if index == 2 then root.fake_input("motion_notify", true, 0, 25) end
            if index == 4 then root.fake_input("motion_notify", true, -25, 0) end
            if index == 6 then root.fake_input("motion_notify", true, 25, 0) end
        end
    }
})

-- control/media
awful.keyboard.append_global_keybindings({

    awful.key({}, "XF86MonBrightnessUp",
        function() awful.spawn("backlight up", false) end,
        { description = "increase brightness", group = "control" }),

    awful.key({}, "XF86MonBrightnessDown",
        function() awful.spawn("backlight down", false) end,
        { description = "decrease brightness", group = "control" }),

    awful.key({}, "Print", function()
        awful.util.spawn(HOME_VAR .. "/.config/awesome/scripts/ss area", false)
    end, { description = "screenshot", group = "control" }),

    awful.key({}, "XF86AudioRaiseVolume",
        function() awful.spawn("change_volume up", false) end,
        { description = "increase volume", group = "control" }),

    awful.key({}, "XF86AudioLowerVolume",
        function() awful.spawn("change_volume down", false) end,
        { description = "decrease volume", group = "control" }),

    awful.key({}, "XF86AudioMute",
        function() awful.spawn("change_volume toggle", false) end,
        { description = "mute volume", group = "control" }),

    awful.key({ modkey }, "F2", function() misc.musicMenu() end,
        { description = "Music menu", group = "control" })

})

-- awesome yeah!
awful.keyboard.append_global_keybindings({

    awful.key({ modkey }, "F1", hotkeys_popup.show_help,
        { description = "show this help window", group = "awesome" }),

    awful.key({ modkey, ctrl }, "r", awesome.restart,
        { description = "reload awesome", group = "awesome" }),

    awful.key({ modkey, shift }, "q", awesome.quit,
        { description = "quit awesome", group = "awesome" }),

    --[[ awful.key({modkey}, "v", function()
        require("mods.exit-screen")
        awesome.emit_signal('module::exit_screen:show')
    end, {description = "show exit screen", group = "modules"}) ]]
})

-- Tags related keybindings
awful.keyboard.append_global_keybindings({
    awful.key({ modkey }, "Escape", awful.tag.history.restore,
        { description = "go back", group = "tags" })
})

-- Focus related keybindings
awful.keyboard.append_global_keybindings({
    awful.key({ modkey }, "Left", function()
        awful.client.focus.bydirection("left")
        bling.module.flash_focus.flashfocus(client.focus)
    end, { description = "focus left", group = "client" }),

    awful.key({ modkey }, "Right", function()
        awful.client.focus.bydirection("right")
        bling.module.flash_focus.flashfocus(client.focus)
    end, { description = "focus right", group = "client" }),

    awful.key({ modkey }, "Up", function()
        awful.client.focus.bydirection("up")
        bling.module.flash_focus.flashfocus(client.focus)
    end, { description = "focus up", group = "client" }),

    awful.key({ modkey }, "Down", function()
        awful.client.focus.bydirection("down")
        bling.module.flash_focus.flashfocus(client.focus)
    end, { description = "focus down", group = "client" }),

    awful.key({ modkey }, "Tab",
        function() awful.spawn("rofi_show window", false) end,
        { description = "window switcher", group = "client" }),

    awful.key({ modkey, ctrl }, "j",
        function() awful.screen.focus_relative(1) end,
        { description = "focus the next screen", group = "screen" }),

    awful.key({ modkey, ctrl }, "k",
        function() awful.screen.focus_relative(-1) end,
        { description = "focus the previous screen", group = "screen" }),

    awful.key({ modkey, ctrl }, "n", function()
        local c = awful.client.restore()
        if c then c:activate { raise = true, context = "key.unminimize" } end
    end, { description = "restore minimized", group = "client" })
})

-- Layout related keybindings
awful.keyboard.append_global_keybindings({

    awful.key({ modkey, shift }, "d", function() awful.client.swap.byidx(1) end, {
        description = "swap with next client by index",
        group = "client"
    }),

    awful.key({ modkey, shift }, "a", function() awful.client.swap.byidx(-1) end,
        {
            description = "swap with previous client by index",
            group = "client"
        }), awful.key({ modkey }, "u", awful.client.urgent.jumpto,
    { description = "jump to urgent client", group = "client" }),

    awful.key({ modkey }, "l", function() awful.tag.incmwfact(0.05) end,
        { description = "increase master width factor", group = "layout" }),

    awful.key({ modkey }, "h", function() awful.tag.incmwfact(-0.05) end,
        { description = "decrease master width factor", group = "layout" }),

    awful.key({ modkey, shift }, "h",
        function() awful.tag.incnmaster(1, nil, true) end, {
            description = "increase the number of master clients",
            group = "layout"
        }), awful.key({ modkey, shift }, "l",
    function() awful.tag.incnmaster(-1, nil, true) end, {
        description = "decrease the number of master clients",
        group = "layout"
    }),

    awful.key({ modkey, ctrl }, "h",
        function() awful.tag.incncol(1, nil, true) end, {
            description = "increase the number of columns",
            group = "layout"
        }),

    awful.key({ modkey, ctrl }, "l",
        function() awful.tag.incncol(-1, nil, true) end, {
            description = "decrease the number of columns",
            group = "layout"
        }), awful.key({ modkey }, "space", function() awful.layout.inc(1) end,
    { description = "select next", group = "layout" }),

    awful.key({ modkey, shift }, "space", function() awful.layout.inc(-1) end,
        { description = "select previous", group = "layout" })
})

-- tag related keys
awful.keyboard.append_global_keybindings({
    awful.key {
        modifiers = { modkey },
        keygroup = "numrow",
        description = "only view tag",
        group = "tag",
        on_press = function(index)
            local screen = awful.screen.focused()
            local tag = screen.tags[index]
            if tag then tag:view_only() end
        end
    }, awful.key {
    modifiers = { modkey, ctrl },
    keygroup = "numrow",
    description = "toggle tag",
    group = "tag",
    on_press = function(index)
        local screen = awful.screen.focused()
        local tag = screen.tags[index]
        if tag then awful.tag.viewtoggle(tag) end
    end
}, awful.key {
    modifiers = { modkey, shift },
    keygroup = "numrow",
    description = "move focused client to tag",
    group = "tag",
    on_press = function(index)
        if client.focus then
            local tag = client.focus.screen.tags[index]
            if tag then client.focus:move_to_tag(tag) end
        end
    end
}, awful.key {
    modifiers = { modkey, ctrl, shift },
    keygroup = "numrow",
    description = "toggle focused client on tag",
    group = "tag",
    on_press = function(index)
        if client.focus then
            local tag = client.focus.screen.tags[index]
            if tag then client.focus:toggle_tag(tag) end
        end
    end
},
    -- awful.key {
    --     modifiers = {modkey},
    --     keygroup = "numpad",
    --     description = "select layout directly",
    --     group = "layout",
    --     on_press = function(index)
    --         local t = awful.screen.focused().selected_tag
    --         if t then t.layout = t.layouts[index] or t.layout end
    --     end
    -- }
})

-- mouse mgmt
client.connect_signal("request::default_mousebindings", function()
    awful.mouse.append_client_mousebindings({

        awful.button({}, 1, function(c)
            c:activate { context = "mouse_click" }
        end),

        awful.button({ modkey }, 1, function(c)
            c:activate { context = "mouse_click", action = "mouse_move" }
        end),

        awful.button({ modkey }, 3, function(c)
            c:activate { context = "mouse_click", action = "mouse_resize" }
        end)

    })
end)

-- client mgmt
client.connect_signal("request::default_keybindings", function()
    awful.keyboard.append_client_keybindings({
        awful.key({ modkey, ctrl }, "f", function(c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end, { description = "toggle fullscreen", group = "client" }),

        awful.key({ modkey, shift }, "c", function(c) c:kill() end,
            { description = "close", group = "client" }),

        awful.key({ modkey, ctrl }, "space", awful.client.floating.toggle,
            { description = "toggle floating", group = "client" }),

        awful.key({ modkey, ctrl }, "Return",
            function(c) c:swap(awful.client.getmaster()) end,
            { description = "move to master", group = "client" }),

        awful.key({ modkey }, "o", function(c) c:move_to_screen() end,
            { description = "move to screen", group = "client" }),

        awful.key({ modkey }, "t", function(c) c.ontop = not c.ontop end,
            { description = "toggle keep on top", group = "client" }),

        awful.key({ modkey }, "n", function(c) c.minimized = true end,
            { description = "minimize", group = "client" }),

        awful.key({ modkey }, "z", function(c)
            c.maximized = not c.maximized
            c:raise()
        end, { description = "(un)maximize", group = "client" })

    })
end)
