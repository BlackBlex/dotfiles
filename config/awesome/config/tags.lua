-- tags  / layouts
-- ~~~~~~~~~~~~~~~
-- requirements
-- ~~~~~~~~~~~~
local awful = require("awful")
-- local lmachi    = require("mods.layout-machi")
-- local bling     = require("mods.bling")

-- misc/vars
-- ~~~~~~~~~

-- bling layouts
-- local mstab     = bling.layout.mstab
-- local equal     = bling.layout.equalarea
-- local deck      = bling.layout.deck

-- layout machi
-- lmachi.editor.nested_layouts = {
--     ["0"] = deck,
--     ["1"] = awful.layout.suit.spiral,
--     ["2"] = awful.layout.suit.fair,
--     ["3"] = awful.layout.suit.fair.horizontal
-- }

-- names/numbers of layouts
local names = { "", "", "", "", "", "", "", "" }
local names_two = { "", "", "", "" }
local l = awful.layout.suit

-- Configurations
-- **************

-- default tags
tag.connect_signal("request::default_layouts", function()
    awful.layout.append_default_layouts({
        l.tile, l.floating, l.magnifier, l.fair, l.max, l.spiral.dwindle
    })
end)

-- set tags
screen.connect_signal("request::desktop_decoration", function(s)
    screen[s].padding = { left = 0, right = 0, top = 0, bottom = 0 }

    if s.index == 1 then
        awful.tag(names, s, awful.layout.layouts[1])
        s.tags[1].layout = l.max
    elseif s.index == 2 then
        awful.tag(names_two, s, awful.layout.layouts[1])
    end
end)
