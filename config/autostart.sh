#!/bin/bash

if pgrep sxhkd; then pkill sxhkd; fi

if ! pgrep nm-applet; then
    /usr/bin/nm-applet &
fi

if ! pgrep blueman-applet; then
    /usr/bin/blueman-applet &
fi

if [ $1 = "awesome" ]; then

	if ! pgrep powerkit; then
		/usr/bin/powerkit &
	fi

	/usr/bin/sxhkd -c ~/.config/sxhkd/sxhkdrc_awesome &

	# Compositor
	if ! pgrep picom; then
		/usr/bin/picom -b --config /home/blackblex/.config/picom/picom.awesomewm.conf &
	fi
fi

if [ $1 = "bspwm" ]; then
	/usr/bin/sxhkd -c ~/.config/sxhkd/sxhkdrc_bspwm &

	# Compositor
	if ! pgrep picom; then
		/usr/bin/picom -b --config /home/blackblex/.config/picom/picom.bspwm.conf &
	fi
fi

# greenclip
if ! pgrep greenclip; then
	/usr/bin/greenclip daemon &
fi

# Cellphone desktop notifications
if ! pgrep dcnnt; then
	/home/blackblex/.local/bin/dcnnt start &
fi

# Polkit agent
if ! pgrep polkit-gnome-authentication-agent-1; then
	/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
fi

# Keepassxc
if ! pgrep keepassxc; then
	sleep 6 && /usr/bin/keepassxc &
fi

# Automounting
if ! pgrep udiskie; then
	/usr/bin/udiskie &
fi

# Betterbird
if ! pgrep betterbird; then
	/usr/bin/betterbird &
fi

# Remmina applet
if ! pgrep remmina; then
	/usr/bin/remmina -i &
fi

# Synology
if ! pgrep synology-drive; then
	/usr/bin/synology-drive autostart &
fi

# Printer applet
if ! pgrep system-config-printer-applet; then
	/usr/bin/system-config-printer-applet &
fi

# Printer applet
if ! pgrep systembus-notify; then
	/usr/bin/systembus-notify &
fi
