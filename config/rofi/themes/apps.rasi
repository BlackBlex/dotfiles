/**
 * This theme is intended for a 16x2 items list menu with a headerbar.
 */
@import "theme_settings.rasi"

* {
	text-color: @foreground;
	font: @text-font;
}

window {
	background-color: @background;
}

inputbar,
prompt,
textbox-prompt-colon,
entry {
	background-color: @grey;
}

#inputbar {
  children: [ prompt, textbox-prompt-colon, entry ];
  margin: @all-inputbar-margin;
  border-radius: @apps-inputbar-radius;
  expand: @apps-inputbar-expand;
}

#prompt {
  padding: @all-prompt-padding;
  background-color: @red;
  vertical-align: 0.5;
  horizontal-align: 0.5;
  text-color: @grey;
  font: @icon-font-menu;
  border-radius: @apps-prompt-radius;
}

#textbox-prompt-colon {
  expand: false;
  str: @apps-prompt;
  padding: @textbox-prompt-colon-padding;
}

#entry {
  text-color: @red;
  padding: @all-entry-padding;
  placeholder-color: @foreground;
}

listview {
  columns: @apps-columns;
  lines: @apps-lines;
  spacing: @apps-list-spacing;
}

#element {
  padding: @list-element-padding;
  margin: @list-element-margin;
  border: @list-element-border;
  orientation: @apps-element-orientation;
}

element-text,
element-icon{
	horizontal-align: @apps-element-horizontal-align;
	vertical-align: @apps-element-vertical-align;
}

element-text {
    margin: @apps-element-text-margin;
}

element.normal.normal,
element.alternate.normal {
  background-color: @apps-element-background;
  border-color: @apps-element-background;
}

element.selected.normal {
  background-color: @grey;
  text-color: @red;
  border-color: @red;
  border-radius: @apps-element-selected-radius;
}
