/**
 * Allows to change the settings of every menu simply by editing this file
 */
@import "resolution.rasi"

* {
	all-window-width: 100%;
	all-window-height: 100%;

	apps-lines: 5;
	apps-columns: 6;
	apps-prompt: "";
	apps-element-orientation: vertical;
	apps-element-horizontal-align: 0.5;
    apps-element-vertical-align: 0.5;
	apps-element-text-margin: 0.5% 0.5% -0.5% 0.5%;
	apps-element-selected-radius: 25px;
	apps-element-background: transparent;
	apps-list-spacing: 1%;
	apps-inputbar-radius: 100%;
	apps-inputbar-expand: false;
	apps-prompt-radius: 100% 0% 0% 100%;

	calc-window-width: 50%;
	calc-window-height: 50%;
	calc-window-padding: 2%;
	calc-lines: 5;
	calc-columns: 2;
	calc-prompt: @apps-prompt;
	calc-element-orientation: @apps-element-orientation;
	calc-element-horizontal-align: @apps-element-horizontal-align;
    calc-element-vertical-align: @apps-element-vertical-align;
	calc-element-text-margin: 1% 0.5% 0.5% 0.5%;
	calc-element-selected-radius: @apps-element-selected-radius;
	calc-element-background: @apps-element-background;
	calc-list-spacing: @apps-list-spacing;
	calc-inputbar-radius: @apps-inputbar-radius;
	calc-inputbar-margin: 0% 3% 0% 3%;
	calc-inputbar-expand: @apps-inputbar-expand;
	calc-prompt-radius: @apps-prompt-radius;

	clipboard-window-width: @all-window-width;
	clipboard-window-height: @all-window-height;
	clipboard-window-padding: @list-window-padding;
	clipboard-lines: @calc-lines;
	clipboard-columns: 3;
	clipboard-scrollbar: true;
	clipboard-prompt: @calc-prompt;
	clipboard-element-orientation: @calc-element-orientation;
	clipboard-element-horizontal-align: @calc-element-horizontal-align;
	clipboard-element-vertical-align: @calc-element-vertical-align;
	clipboard-element-text-margin: @calc-element-text-margin;
	clipboard-element-selected-radius: @calc-element-selected-radius;
	clipboard-element-background: @grey;
	clipboard-list-spacing: @calc-list-spacing;
	clipboard-inputbar-radius: @calc-inputbar-radius;
	clipboard-inputbar-margin: @all-inputbar-margin;
	clipboard-inputbar-expand: @calc-inputbar-expand;
	clipboard-prompt-radius: @calc-prompt-radius;

	emoji-window-width: @clipboard-window-width;
	emoji-window-height: @clipboard-window-height;
	emoji-window-padding: @clipboard-window-padding;
	emoji-lines: @clipboard-lines;
	emoji-columns: @clipboard-columns;
	emoji-scrollbar: false;
	emoji-prompt: @clipboard-prompt;
	emoji-element-orientation: @clipboard-element-orientation;
	emoji-element-horizontal-align: @clipboard-element-horizontal-align;
	emoji-element-vertical-align: @clipboard-element-vertical-align;
	emoji-element-text-margin: @clipboard-element-text-margin;
	emoji-element-selected-radius: @clipboard-element-selected-radius;
	emoji-element-background: @clipboard-element-background;
	emoji-list-spacing: @clipboard-list-spacing;
	emoji-inputbar-radius: @clipboard-inputbar-radius;
	emoji-inputbar-margin: @clipboard-inputbar-margin;
	emoji-inputbar-expand: @clipboard-inputbar-expand;
	emoji-prompt-radius: @clipboard-prompt-radius;

	network-window-width: 50%;
	network-window-height: 50%;
	network-window-padding: 2%;
	network-lines: @emoji-lines;
	network-columns: 1;
	network-scrollbar: true;
	network-prompt: @emoji-prompt;
	network-element-orientation: @emoji-element-orientation;
	network-element-horizontal-align: @emoji-element-horizontal-align;
	network-element-vertical-align: @emoji-element-vertical-align;
	network-element-text-margin: @emoji-element-text-margin;
	network-element-selected-radius: @emoji-element-selected-radius;
	network-element-background: @emoji-element-background;
	network-list-spacing: @emoji-list-spacing;
	network-inputbar-radius: @emoji-inputbar-radius;
	network-inputbar-margin: 0% 3% 3% 3%;
	network-inputbar-expand: @emoji-inputbar-expand;
	network-prompt-radius: @emoji-prompt-radius;

	power-window-width: @clipboard-window-width;
	power-window-height: @clipboard-window-height;
	power-window-padding: @clipboard-window-padding;
	power-lines: @clipboard-lines;
	power-columns: @clipboard-columns;
	power-scrollbar: false;
	power-element-horizontal-align: @clipboard-element-horizontal-align;
    power-element-vertical-align: @clipboard-element-vertical-align;
	power-element-text-margin: 0.5% 1.5% 0.5% 1.5%;
	power-element-selected-radius: 50%;
	power-element-background: @clipboard-element-background;
	power-list-spacing: 2%;
	power-list-margin: 73% 20% 0% 20%;

	windows-window-width: @network-window-width;
	windows-window-height: @network-window-height;
	windows-window-padding: @network-window-padding;
	windows-lines: 1;
	windows-columns: 4;
	windows-scrollbar: false;
	windows-prompt: @network-prompt;
	windows-element-orientation: @network-element-orientation;
	windows-element-horizontal-align: @network-element-horizontal-align;
	windows-element-vertical-align: @network-element-vertical-align;
	windows-element-text-margin: @network-element-text-margin;
	windows-element-selected-radius: @network-element-selected-radius;
	windows-element-background: @network-element-background;
	windows-element-background-active: @cyan;
	windows-list-spacing: @network-list-spacing;
	windows-inputbar-radius: @network-inputbar-radius;
	windows-inputbar-margin: @network-inputbar-margin;
	windows-inputbar-expand: @network-inputbar-expand;
	windows-prompt-radius: @network-prompt-radius;
}

window {
	width: @all-window-width;
	height: @all-window-height;
	location: center;
	padding: @list-window-padding;
	border: @list-window-border;
}

scrollbar {
    width:        6px;
    border:       0;
    handle-color: @grey;
    handle-width: 6px;
    padding:      0;
}
