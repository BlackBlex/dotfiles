/**
 * This theme is intended for a 16x2 items list menu with a headerbar.
 */
@import "theme_settings.rasi"

* {
	text-color: @foreground;
	font: @text-font;
}

window {
	background-color: @background;
	width: @windows-window-width;
	height: @windows-window-height;
	padding: @windows-window-padding;
}

inputbar,
prompt,
textbox-prompt-colon,
entry {
	background-color: @grey;
}

#inputbar {
  children: [ prompt, textbox-prompt-colon, entry ];
  margin: @windows-inputbar-margin;
  border-radius: @windows-inputbar-radius;
  expand: @windows-inputbar-expand;
}

#prompt {
  padding: @all-prompt-padding;
  background-color: @blue;
  vertical-align: 0.5;
  horizontal-align: 0.5;
  text-color: @grey;
  font: @icon-font-menu;
  border-radius: @windows-prompt-radius;
}

#textbox-prompt-colon {
  expand: false;
  str: @windows-prompt;
  padding: @textbox-prompt-colon-padding;
}

#entry {
  text-color: @blue;
  padding: @all-entry-padding;
  placeholder-color: @foreground;
}

listview {
  columns: @windows-columns;
  lines: @windows-lines;
  spacing: @windows-list-spacing;
  scrollbar: @windows-scrollbar;
  orientation: horizontal;
}

#element {
  padding: @list-element-padding;
  margin: @list-element-margin;
  border: @list-element-border;
  orientation: @windows-element-orientation;
}

element-text,
element-icon{
	horizontal-align: @windows-element-horizontal-align;
	vertical-align: @windows-element-vertical-align;
}

#element-icon {
	size: 10ch;
}

#element-text {
    margin: @windows-element-text-margin;
}

element.normal.normal,
element.alternate.normal {
  background-color: @windows-element-background;
  border-color: @windows-element-background;
  border-radius: @windows-element-selected-radius;
}

element.normal.active {
	background-color: @windows-element-background;
	text-color: @white;
	border-color: @windows-element-background-active;
	border-radius: @windows-element-selected-radius;
}

element.selected.normal, element.selected.active {
	background-color: @grey;
	text-color: @blue;
	border-color: @blue;
    border-radius: @windows-element-selected-radius;
}
