/**
 * This theme is intended for a 16x2 items list menu with a headerbar.
 */
@import "theme_settings.rasi"

* {
	text-color: @foreground;
	font: @text-font;
}

configuration {
	show-icons: false;
}

window {
	background-color: @background;
	width: @clipboard-window-width;
	height: @clipboard-window-height;
	padding: @clipboard-window-padding;
}

inputbar,
prompt,
textbox-prompt-colon,
entry {
	background-color: @grey;
}

#inputbar {
  children: [ prompt, textbox-prompt-colon, entry ];
  margin: @clipboard-inputbar-margin;
  border-radius: @clipboard-inputbar-radius;
  expand: @clipboard-inputbar-expand;
}

#prompt {
  padding: @all-prompt-padding;
  background-color: @cyan;
  vertical-align: 0.5;
  horizontal-align: 0.5;
  text-color: @grey;
  font: @icon-font-menu;
  border-radius: @clipboard-prompt-radius;
}

#textbox-prompt-colon {
  expand: false;
  str: @clipboard-prompt;
  padding: @textbox-prompt-colon-padding;
}

#entry {
  text-color: @cyan;
  padding: @all-entry-padding;
  placeholder-color: @foreground;
}

listview {
  columns: @clipboard-columns;
  lines: @clipboard-lines;
  spacing: @clipboard-list-spacing;
  scrollbar: @clipboard-scrollbar;
}

#element {
  padding: @list-element-padding;
  margin: @list-element-margin;
  border: @list-element-border;
  orientation: @clipboard-element-orientation;
}

element-text,
element-icon{
	horizontal-align: @clipboard-element-horizontal-align;
	vertical-align: @clipboard-element-vertical-align;
}

element-text {
    margin: @clipboard-element-text-margin;
}

element.normal.normal,
element.alternate.normal {
  background-color: @clipboard-element-background;
  border-color: @clipboard-element-background;
  border-radius: @clipboard-element-selected-radius;
}

element.selected.normal {
  background-color: @grey;
  text-color: @cyan;
  border-color: @cyan;
  border-radius: @clipboard-element-selected-radius;
}
