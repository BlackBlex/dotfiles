#!/usr/bin/env sh

# Kill all running polybars
while pgrep -xu $EUID "polybar" &> /dev/null; do
    killall "polybar"
done

# Run polybar on each monitor
for m in $(polybar --list-monitors | cut -d":" -f1); do
    BAR_MONITOR=$m polybar --reload $m -c ~/.config/polybar/config-bar.ini &
done

polybar --reload "eDP-main" -c ~/.config/polybar/config-bar.ini &
