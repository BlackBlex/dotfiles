#!/bin/bash

ETHERNET="$(/usr/bin/ip a show enp1s0 2>/dev/null| grep "inet " | awk '{print $2}')"

if [ -z "$ETHERNET" ]
then
    echo -e "$(/usr/bin/ip a show wlo1 2>/dev/null| grep "inet " | awk '{print $2}')"
else
    echo -e $ETHERNET
fi
