-- Automatically generated packer.nvim plugin loader code

if vim.api.nvim_call_function('has', {'nvim-0.5'}) ~= 1 then
  vim.api.nvim_command('echohl WarningMsg | echom "Invalid Neovim version for packer.nvim! | echohl None"')
  return
end

vim.api.nvim_command('packadd packer.nvim')

local no_errors, error_msg = pcall(function()

  local time
  local profile_info
  local should_profile = false
  if should_profile then
    local hrtime = vim.loop.hrtime
    profile_info = {}
    time = function(chunk, start)
      if start then
        profile_info[chunk] = hrtime()
      else
        profile_info[chunk] = (hrtime() - profile_info[chunk]) / 1e6
      end
    end
  else
    time = function(chunk, start) end
  end
  
local function save_profiles(threshold)
  local sorted_times = {}
  for chunk_name, time_taken in pairs(profile_info) do
    sorted_times[#sorted_times + 1] = {chunk_name, time_taken}
  end
  table.sort(sorted_times, function(a, b) return a[2] > b[2] end)
  local results = {}
  for i, elem in ipairs(sorted_times) do
    if not threshold or threshold and elem[2] > threshold then
      results[i] = elem[1] .. ' took ' .. elem[2] .. 'ms'
    end
  end

  _G._packer = _G._packer or {}
  _G._packer.profile_output = results
end

time([[Luarocks path setup]], true)
local package_path_str = "/home/blackblex/.cache/nvim/packer_hererocks/2.1.0-beta3/share/lua/5.1/?.lua;/home/blackblex/.cache/nvim/packer_hererocks/2.1.0-beta3/share/lua/5.1/?/init.lua;/home/blackblex/.cache/nvim/packer_hererocks/2.1.0-beta3/lib/luarocks/rocks-5.1/?.lua;/home/blackblex/.cache/nvim/packer_hererocks/2.1.0-beta3/lib/luarocks/rocks-5.1/?/init.lua"
local install_cpath_pattern = "/home/blackblex/.cache/nvim/packer_hererocks/2.1.0-beta3/lib/lua/5.1/?.so"
if not string.find(package.path, package_path_str, 1, true) then
  package.path = package.path .. ';' .. package_path_str
end

if not string.find(package.cpath, install_cpath_pattern, 1, true) then
  package.cpath = package.cpath .. ';' .. install_cpath_pattern
end

time([[Luarocks path setup]], false)
time([[try_loadstring definition]], true)
local function try_loadstring(s, component, name)
  local success, result = pcall(loadstring(s), name, _G.packer_plugins[name])
  if not success then
    vim.schedule(function()
      vim.api.nvim_notify('packer.nvim: Error running ' .. component .. ' for ' .. name .. ': ' .. result, vim.log.levels.ERROR, {})
    end)
  end
  return result
end

time([[try_loadstring definition]], false)
time([[Defining packer_plugins]], true)
_G.packer_plugins = {
  ["AutoSave.nvim"] = {
    config = { "\27LJ\2\nn\0\0\3\0\4\0\a6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\3\0B\0\2\1K\0\1\0\1\0\2\19debounce_delay\3è\a clean_command_line_interval\3è\a\nsetup\rautosave\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/AutoSave.nvim",
    url = "https://github.com/Pocco81/AutoSave.nvim"
  },
  ["Comment.nvim"] = {
    config = { "\27LJ\2\n5\0\0\3\0\3\0\0066\0\0\0'\2\1\0B\0\2\0029\0\2\0B\0\1\1K\0\1\0\nsetup\fComment\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/Comment.nvim",
    url = "https://github.com/numToStr/Comment.nvim"
  },
  LuaSnip = {
    config = { "\27LJ\2\n?\0\0\3\1\2\0\n-\0\0\0009\0\0\0B\0\1\2\15\0\0\0X\1\4-\0\0\0009\0\1\0)\2\1\0B\0\2\1K\0\1\0\0À\tjump\rjumpable?\0\0\3\1\2\0\n-\0\0\0009\0\0\0B\0\1\2\15\0\0\0X\1\4-\0\0\0009\0\1\0)\2ÿÿB\0\2\1K\0\1\0\0À\tjump\rjumpableã\4\1\0\t\0\31\00096\0\0\0'\2\1\0B\0\2\0026\1\2\0009\1\3\0019\1\4\0016\2\2\0009\2\5\0029\2\6\0029\3\a\0009\3\b\0035\5\t\0B\3\2\0016\3\0\0'\5\n\0B\3\2\0029\3\v\0035\5\r\0005\6\f\0=\6\14\5B\3\2\0016\3\0\0'\5\15\0B\3\2\0029\3\v\0035\5\17\0005\6\16\0=\6\14\5B\3\2\0016\3\0\0'\5\18\0B\3\2\0029\3\v\0035\5\20\0005\6\19\0=\6\14\5B\3\2\1\18\3\2\0'\5\21\0006\6\0\0'\b\22\0B\6\2\0029\6\23\0065\a\24\0B\3\4\1\18\3\1\0005\5\25\0'\6\26\0003\a\27\0B\3\4\1\18\3\1\0005\5\28\0'\6\29\0003\a\30\0B\3\4\0012\0\0K\0\1\0\0\n<C-k>\1\3\0\0\6s\6i\0\n<C-j>\1\3\0\0\6s\6i\1\0\1\tbang\2\23edit_snippet_files\20luasnip.loaders\rSnipEdit\1\0\0\1\2\0\0\23./snippets/angular luasnip.loaders.from_vscode\1\0\0\1\2\0\0\23./snippets/luasnip\29luasnip.loaders.from_lua\npaths\1\0\0\1\2\0\0\24./snippets/snipmate\14lazy_load\"luasnip.loaders.from_snipmate\1\0\1\18update_events\29TextChanged,TextChangedI\nsetup\vconfig\29nvim_create_user_command\bapi\bset\vkeymap\bvim\fluasnip\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/LuaSnip",
    url = "https://github.com/L3MON4D3/LuaSnip"
  },
  ["NeoZoom.lua"] = {
    config = { "\27LJ\2\nZ\0\0\3\0\4\0\a6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\3\0B\0\2\1K\0\1\0\1\0\2\17height_ratio\3\1\16width_ratio\3\1\nsetup\rneo-zoom\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/NeoZoom.lua",
    url = "https://github.com/nyngwang/NeoZoom.lua"
  },
  ["alpha-nvim"] = {
    config = { "\27LJ\2\n>\0\0\3\0\3\0\0056\0\0\0009\0\1\0'\2\2\0B\0\2\2L\0\2\0\31%A, %d %B %Y ~ %I:%M:%S %p\tdate\aosÂ¢\1\1\0\v\0W\1¯\0016\0\0\0'\2\1\0B\0\2\0026\1\0\0'\3\2\0B\1\2\0023\2\3\0004\3/\0005\4\4\0>\4\1\0035\4\5\0>\4\2\0035\4\6\0>\4\3\0035\4\a\0>\4\4\0035\4\b\0>\4\5\0035\4\t\0>\4\6\0035\4\n\0>\4\a\0035\4\v\0>\4\b\0035\4\f\0>\4\t\0035\4\r\0>\4\n\0035\4\14\0>\4\v\0035\4\15\0>\4\f\0035\4\16\0>\4\r\0035\4\17\0>\4\14\0035\4\18\0>\4\15\0035\4\19\0>\4\16\0035\4\20\0>\4\17\0035\4\21\0>\4\18\0035\4\22\0>\4\19\0035\4\23\0>\4\20\0035\4\24\0>\4\21\0035\4\25\0>\4\22\0035\4\26\0>\4\23\0035\4\27\0>\4\24\0035\4\28\0>\4\25\0035\4\29\0>\4\26\0035\4\30\0>\4\27\0035\4\31\0>\4\28\0035\4 \0>\4\29\0035\4!\0>\4\30\0035\4\"\0>\4\31\0035\4#\0>\4 \0035\4$\0>\4!\0035\4%\0>\4\"\0035\4&\0>\4#\0035\4'\0>\4$\0035\4(\0>\4%\0035\4)\0>\4&\0035\4*\0>\4'\0035\4+\0>\4(\0035\4,\0>\4)\0035\4-\0>\4*\0035\4.\0>\4+\0035\4/\0>\4,\0035\0040\0>\4-\0035\0041\0>\4.\0036\0042\0009\0043\0046\0064\0009\0065\6B\6\1\0A\4\0\0019\0046\0019\0047\0046\0052\0009\0059\5\21\a\3\0B\5\2\0028\5\5\3=\0058\0049\0046\0019\4:\0044\5\t\0009\6;\1'\b<\0'\t=\0'\n>\0B\6\4\2>\6\1\0059\6;\1'\b?\0'\t@\0'\nA\0B\6\4\2>\6\2\0059\6;\1'\bB\0'\tC\0'\nD\0B\6\4\2>\6\3\0059\6;\1'\bE\0'\tF\0'\nG\0B\6\4\2>\6\4\0059\6;\1'\bH\0'\tI\0'\nJ\0B\6\4\2>\6\5\0059\6;\1'\bK\0'\tL\0'\nM\0B\6\4\2>\6\6\0059\6;\1'\bN\0'\tO\0'\nP\0B\6\4\2>\6\a\0059\6;\1'\bQ\0'\tR\0'\nS\0B\6\4\0?\6\0\0=\0058\0049\0046\0019\4T\4\18\5\2\0B\5\1\2=\0058\0049\4U\0009\6V\1B\4\2\1K\0\1\0\topts\nsetup\vfooter\f:qa<CR>\16ï  > Quit\6q\22:PackerUpdate<CR>\26ï  > Update plugins\6u\20:e $MYVIMRC<CR>\20ï  > Settings\6s%:SessionManager load_session<CR>\24ï¤  > Open session\6c\28:Telescope oldfiles<CR>\31î  > Recently used files\6r\30:Telescope find_files<CR>\21ï  > Find file\6f\22:NvimTreeOpen<CR>\21ï  > Open file\6o\31:ene <BAR> startinsert<CR>\20ï  > New file\6e\vbutton\fbuttons\vrandom\bval\vheader\fsection\ttime\aos\15randomseed\tmath\1\f\0\0J   â£´â£¶â£¤â¡¤â ¦â£¤â£â£¤â      â£â£­â£¿â£¶â£¿â£¦â£¼â£          X    â â »â¢¿â£¿â ¿â£¿â£¿â£¶â£¦â ¤â â¡ â¢¾â£¿â£¿â¡¿â â â â »â£¿â£¿â¡â£¦       F          â â¢¿â£¿â£â ¦ â£¾â£¿â£¿â£·    â »â ¿â¢¿â£¿â£§â£     N           â£¸â£¿â£¿â¢§ â¢»â »â£¿â£¿â£·â£â£â â ¢â£â¡â â â ¿â     L          â¢ â£¿â£¿â£¿â     â£»â£¿â£¿â£¿â£¿â£¿â£¿â£¿â£â£³â£¤â£â£   ^   â¢ â£§â£¶â£¥â¡¤â¢ â£¸â£¿â£¿â   â¢â£´â£¿â£¿â¡¿â â£¿â£¿â£§â â¢¿â ¿â â â »â ¿â   X  â£°â£¿â£¿â â »â£¿â£¿â¡¦â¢¹â£¿â£·   â¢â£¿â£¿â¡  â¢¸â£¿â£¿â¡ â¢â£ â£â£¾â    b â£ â£¿â ¿â  â¢â£¿â£¿â£·â â¢¿â£¿â£¦â¡ â¢¸â¢¿â£¿â£¿â£ â£¸â£¿â£¿â¡â£ªâ£¿â¡¿â ¿â£¿â£·â¡  \\ â â    â£¼â£¿â¡  â â »â£¿â£¿â£¦â£â¡â »â£¿â£¿â£·â£¿â£¿â£¿ â£¿â£¿â¡ â â »â¢·â£ P      â¢»â£¿â£¿â£   â â »â£¿â£¿â£¿â£·â£¿â£¿â£¿â£¿â£¿â¡ â «â¢¿â£¿â¡     V       â »â£¿â£¿â£¿â£¿â£¶â£¶â£¾â£¿â£¿â£¿â£¿â£¿â£¿â£¿â£¿â¡â¢â£â£¤â£¾â¡¿â      \1\a\0\0007                                __                7   ___     ___    ___   __  __ /\\_\\    ___ ___    7  / _ `\\  / __`\\ / __`\\/\\ \\/\\ \\\\/\\ \\  / __` __`\\  7 /\\ \\/\\ \\/\\  __//\\ \\_\\ \\ \\ \\_/ |\\ \\ \\/\\ \\/\\ \\/\\ \\ 7 \\ \\_\\ \\_\\ \\____\\ \\____/\\ \\___/  \\ \\_\\ \\_\\ \\_\\ \\_\\7  \\/_/\\/_/\\/____/\\/___/  \\/__/    \\/_/\\/_/\\/_/\\/_/\1\a\0\0001 _______             ____   ____.__         1 \\      \\   ____  ___\\   \\ /   /|__| _____  1 /   |   \\_/ __ \\/  _ \\   Y   / |  |/     \\ 1/    |    \\  ___(  <_> )     /  |  |  Y Y  \\1\\____|__  /\\___  >____/ \\___/   |__|__|_|  /1        \\/     \\/                        \\/ \1\t\0\0009                                                    \1 ââââ   âââââââââââ âââââââ âââ   ââââââââââ   ââââ \1 âââââ  âââââââââââââââââââââââ   âââââââââââ âââââ \1 ââââââ âââââââââ  âââ   ââââââ   âââââââââââââââââ \1 ââââââââââââââââ  âââ   âââââââ ââââââââââââââââââ \1 âââ âââââââââââââââââââââââ âââââââ ââââââ âââ âââ \1 âââ  âââââââââââââ âââââââ   âââââ  ââââââ     âââ 9                                                    \1\b\0\0X      ::::    :::   ::::::::::   ::::::::   :::     :::   :::::::::::     :::   :::X     :+:+:   :+:   :+:         :+:    :+:  :+:     :+:       :+:        :+:+: :+:+:X    :+:+:+  +:+   +:+         +:+    +:+  +:+     +:+       +:+       +:+ +:+:+ +:+X   +#+ +:+ +#+   +#++:++#    +#+    +:+  +#+     +:+       +#+       +#+  +:+  +#+ X  +#+  +#+#+#   +#+         +#+    +#+   +#+   +#+        +#+       +#+       +#+  X #+#   #+#+#   #+#         #+#    #+#    #+#+#+#         #+#       #+#       #+#   X###    ####   ##########   ########       ###       ###########   ###       ###    \1\b\0\0Tooooo      ooo oooooooooooo   .oooooo.   oooooo     oooo ooooo ooo        oooooT`888b.     `8' `888'     `8  d8P'  `Y8b   `888.     .8'  `888' `88.       .888'T 8 `88b.    8   888         888      888   `888.   .8'    888   888b     d'888 T 8   `88b.  8   888oooo8    888      888    `888. .8'     888   8 Y88. .P  888 T 8     `88b.8   888    \"    888      888     `888.8'      888   8  `888'   888 T 8       `888   888       o `88b    d88'      `888'       888   8    Y     888 To8o        `8  o888ooooood8  `Y8bood8P'        `8'       o888o o8o        o888o\1\6\0\0006_____   __                     _____             6___  | / /_____ ______ ___   _____(_)_______ ___ 6__   |/ / _  _ \\_  __ \\__ | / /__  / __  __ `__ \\6_  /|  /  /  __// /_/ /__ |/ / _  /  _  / / / / /6/_/ |_/   \\___/ \\____/ _____/  /_/   /_/ /_/ /_/ \1\6\0\0H_|      _|   _|_|_|_|     _|_|     _|      _|   _|_|_|   _|      _|H_|_|    _|   _|         _|    _|   _|      _|     _|     _|_|  _|_|H_|  _|  _|   _|_|_|     _|    _|   _|      _|     _|     _|  _|  _|H_|    _|_|   _|         _|    _|     _|  _|       _|     _|      _|H_|      _|   _|_|_|_|     _|_|         _|       _|_|_|   _|      _|\1\b\0\0S ________     _______     ________    ___      ___   ___    _____ ______      S|\\   ___  \\  |\\  ___ \\   |\\   __  \\  |\\  \\    /  /| |\\  \\  |\\   _ \\  _   \\    S\\ \\  \\\\ \\  \\ \\ \\   __/|  \\ \\  \\|\\  \\ \\ \\  \\  /  / / \\ \\  \\ \\ \\  \\\\\\__\\ \\  \\   S \\ \\  \\\\ \\  \\ \\ \\  \\_|/__ \\ \\  \\\\\\  \\ \\ \\  \\/  / /   \\ \\  \\ \\ \\  \\\\|__| \\  \\  S  \\ \\  \\\\ \\  \\ \\ \\  \\_|\\ \\ \\ \\  \\\\\\  \\ \\ \\    / /     \\ \\  \\ \\ \\  \\    \\ \\  \\ S   \\ \\__\\\\ \\__\\ \\ \\_______\\ \\ \\_______\\ \\ \\__/ /       \\ \\__\\ \\ \\__\\    \\ \\__\\S    \\|__| \\|__|  \\|_______|  \\|_______|  \\|__|/         \\|__|  \\|__|     \\|__|\1\b\0\0F  __   __      _____     _____      _     _     __     __    __  F /_/\\ /\\_\\   /\\_____\\   ) ___ (    /_/\\ /\\_\\   /\\_\\   /_/\\  /\\_\\ F ) ) \\ ( (  ( (_____/  / /\\_/\\ \\   ) ) ) ( (   \\/_/   ) ) \\/ ( ( F/_/   \\ \\_\\  \\ \\__\\   / /_/ (_\\ \\ /_/ / \\ \\_\\   /\\_\\ /_/ \\  / \\_\\F\\ \\ \\   / /  / /__/_  \\ \\ )_/ / / \\ \\ \\_/ / /  / / / \\ \\ \\\\// / /F )_) \\ (_(  ( (_____\\  \\ \\/_\\/ /   \\ \\   / /  ( (_(   )_) )( (_( F \\_\\/ \\/_/   \\/_____/   )_____(     \\_\\_/_/    \\/_/   \\_\\/  \\/_/ \1\14\0\0h _____   ______         ______            _____      ____      ____   ____       ______  _______   h|\\    \\ |\\     \\    ___|\\     \\      ____|\\    \\    |    |    |    | |    |     |      \\/       \\  h \\\\    \\| \\     \\  |     \\     \\    /     /\\    \\   |    |    |    | |    |    /          /\\     \\ h  \\|    \\  \\     | |     ,_____/|  /     /  \\    \\  |    |    |    | |    |   /     /\\   / /\\     |h   |     \\  |    | |     \\--'\\_|/ |     |    |    | |    |    |    | |    |  /     /\\ \\_/ / /    /|h   |      \\ |    | |     /___/|   |     |    |    | |    |    |    | |    | |     |  \\|_|/ /    / |h   |    |\\ \\|    | |     \\____|\\  |\\     \\  /    /| |\\    \\  /    /| |    | |     |       |    |  |h   |____||\\_____/| |____ '     /| | \\_____\\/____/ | | \\ ___\\/___ / | |____| |\\____\\       |____|  /h   |    |/ \\|   || |    /_____/ |  \\ |    ||    | /  \\ |   ||   | /  |    | | |    |      |    | / h   |____|   |___|/ |____|     | /   \\|____||____|/    \\|___||___|/   |____|  \\|____|      |____|/  h     \\(       )/     \\( |_____|/       \\(    )/         \\(    )/       \\(       \\(          )/     h      '       '       '    )/           '    '           '    '         '        '          '      h                           '                                                                       \1\b\0\0Q _____  ___     _______      ______     ___      ___   __      ___      ___ Q(\\\"   \\|\"  \\   /\"     \"|    /    \" \\   |\"  \\    /\"  | |\" \\    |\"  \\    /\"  |S|.\\\\   \\    | (: ______)   // ____  \\   \\   \\  //  /  |â  |    \\   \\  //   |Q|: \\.   \\\\  |  \\/    |    /  /    ) :)   \\\\  \\/. ./   |:  |    /\\\\  \\/.    |Q|.  \\    \\. |  // ___)_  (: (____/ //     \\.    //    |.  |   |: \\.        |Q|    \\    \\ | (:      \"|  \\        /       \\\\   /     /\\  |\\  |.  \\    /:  |Q \\___|\\____\\)  \\_______)   \\\"_____/         \\__/     (__\\_|_) |___|\\__/|___|\1\v\0\0c        /\\ \\     _      /\\ \\           /\\ \\     /\\ \\    _ / /\\        /\\ \\        /\\_\\/\\_\\ _  c       /  \\ \\   /\\_\\   /  \\ \\         /  \\ \\    \\ \\ \\  /_/ / /        \\ \\ \\      / / / / //\\_\\c      / /\\ \\ \\_/ / /  / /\\ \\ \\       / /\\ \\ \\    \\ \\ \\ \\___\\/         /\\ \\_\\    /\\ \\/ \\ \\/ / /c     / / /\\ \\___/ /  / / /\\ \\_\\     / / /\\ \\ \\   / / /  \\ \\ \\        / /\\/_/   /  \\____\\__/ / c    / / /  \\/____/  / /_/_ \\/_/    / / /  \\ \\_\\  \\ \\ \\   \\_\\ \\      / / /     / /\\/________/  c   / / /    / / /  / /____/\\      / / /   / / /   \\ \\ \\  / / /     / / /     / / /\\/_// / /   c  / / /    / / /  / /\\____\\/     / / /   / / /     \\ \\ \\/ / /     / / /     / / /    / / /    c / / /    / / /  / / /______    / / /___/ / /       \\ \\ \\/ /  ___/ / /__   / / /    / / /     c/ / /    / / /  / / /_______\\  / / /____\\/ /         \\ \\  /  /\\__\\/_/___\\  \\/_/    / / /      c\\/_/     \\/_/   \\/__________/  \\/_________/           \\_\\/   \\/_________/          \\/_/       \1\a\0\0E  _  _      ___      ___    __   __    ___    __  __  O         E | \\| |    | __|    / _ \\   \\ \\ / /   |_ _|  |  \\/  |   o       E | .` |    | _|    | (_) |   \\ V /     | |   | |\\/| |  ___      E |_|\\_|    |___|    \\___/    _\\_/_    |___|  |_|__|_|  |o|____  E_|\"\"\"\"\"| _|\"\"\"\"\"| _|\"\"\"\"\"| _|\"\"\"\"\"| _|\"\"\"\"\"| _|\"\"\"\"\"| _|\"\"\"\"\"\"| E\"`-0-0-' \"`-0-0-' \"`-0-0-' \"`-0-0-' \"`-0-0-' \"`-0-0-' \"`-0-0---\\\1\n\0\0M     .-') _     ('-.                      (`-.              _   .-')    M    ( OO ) )  _(  OO)                   _(OO  )_           ( '.( OO )_  M,--./ ,--,'  (,------.  .-'),-----. ,--(_/   ,. \\  ,-.-')   ,--.   ,--.)M|   \\ |  |\\   |  .---' ( OO'  .-.  '\\   \\   /(__/  |  |OO)  |   `.'   | M|    \\|  | )  |  |     /   |  | |  | \\   \\ /   /   |  |  \\  |         | M|  .     |/  (|  '--.  \\_) |  |\\|  |  \\   '   /,   |  |(_/  |  |'.'|  | M|  |\\    |    |  .--'    \\ |  | |  |   \\     /__) ,|  |_.'  |  |   |  | M|  | \\   |    |  `---.    `'  '-'  '    \\   /    (_|  |     |  |   |  | M`--'  `--'    `------'      `-----'      `-'       `--'     `--'   `--' \1\b\0\0C\\\\\\  ///              .-.     wWw    wWw    wW  Ww  \\\\\\    ///C((O)(O))    wWw     c(O_O)c   (O)    (O)    (O)(O)  ((O)  (O))C | \\ ||     (O)_   ,'.---.`,  ( \\    / )     (..)    | \\  / | C ||\\\\||    .' __) / /|_|_|\\ \\  \\ \\  / /       ||     ||\\\\//|| C || \\ |   (  _)   | \\_____/ |  /  \\/  \\      _||_    || \\/ || C ||  ||    `.__)  '. `---' .`  \\ `--' /     (_/\\_)   ||    || C(_/  \\_)            `-...-'     `-..-'              (_/    \\_)\1\t\0\0.    )                                    . ( /(                                    . )\\())    (            )     (       )   .((_)\\    ))\\    (     /((    )\\     (    . _((_)  /((_)   )\\   (_))\\  ((_)    )\\  '.| \\| | (_))    ((_)  _)((_)  (_)  _((_)) .| .` | / -_)  / _ \\  \\ V /   | | | '  \\().|_|\\_| \\___|  \\___/   \\_/    |_| |_|_|_| \1\t\0\0002    )            )             (        *    2 ( /(         ( /(             )\\ )   (  `   2 )\\())  (     )\\())   (   (   (()/(   )\\))(  2((_)\\   )\\   ((_)\\    )\\  )\\   /(_)) ((_)()\\ 2 _((_) ((_)    ((_)  ((_)((_) (_))   (_()((_)2| \\| | | __|  / _ \\  \\ \\ / /  |_ _|  |  \\/  |2| .` | | _|  | (_) |  \\ V /    | |   | |\\/| |2|_|\\_| |___|  \\___/    \\_/    |___|  |_|  |_|\1\b\0\0L  _   _     U _____ u    U  ___ u  __     __                   __  __  L | \\ |\"|    \\| ___\"|/     \\/\"_ \\/  \\ \\   /\"/u       ___      U|' \\/ '|uL<|  \\| |>    |  _|\"       | | | |   \\ \\ / //       |_\"_|     \\| |\\/| |/L |\\  |u    | |___   .-,_| |_| |   /\\ V /_,-.      | |       | |  | |   L |_| \\_|     |_____|   \\_)-\\___/   U  \\_/-(_/     U/| |\\u     |_|  |_| L ||   \\\\,-.  <<   >>        \\\\       //        .-,_|___|_,-. <<,-,,-.  L (_\")  (_/  (__) (__)      (__)     (__)        \\_)-' '-(_/   (./  \\.) \1\5\0\0, _  _  ____  _____  _  _  ____  __  __ ,( \\( )( ___)(  _  )( \\/ )(_  _)(  \\/  ), )  (  )__)  )(_)(  \\  /  _)(_  )    ( ,(_)\\_)(____)(_____)  \\/  (____)(_/\\/\\_)\1\b\0\0X ___   __       ______       ______       __   __      ________      ___ __ __     X/__/\\ /__/\\    /_____/\\     /_____/\\     /_/\\ /_/\\    /_______/\\    /__//_//_/\\    X\\::\\_\\\\  \\ \\   \\::::_\\/_    \\:::_ \\ \\    \\:\\ \\\\ \\ \\   \\__.::._\\/    \\::\\| \\| \\ \\   X \\:. `-\\  \\ \\   \\:\\/___/\\    \\:\\ \\ \\ \\    \\:\\ \\\\ \\ \\     \\::\\ \\      \\:.      \\ \\  X  \\:. _    \\ \\   \\::___\\/_    \\:\\ \\ \\ \\    \\:\\_/.:\\ \\    _\\::\\ \\__    \\:.\\-/\\  \\ \\ X   \\. \\`-\\  \\ \\   \\:\\____/\\    \\:\\_\\ \\ \\    \\ ..::/ /   /__\\::\\__/\\    \\. \\  \\  \\ \\X    \\__\\/ \\__\\/    \\_____\\/     \\_____\\/     \\___/_(    \\________\\/     \\__\\/ \\__\\/\1\6\0\0D __   __     ______     ______     __   __   __     __    __   D/\\ \"-.\\ \\   /\\  ___\\   /\\  __ \\   /\\ \\ / /  /\\ \\   /\\ \"-./  \\  D\\ \\ \\-.  \\  \\ \\  __\\   \\ \\ \\/\\ \\  \\ \\ \\'/   \\ \\ \\  \\ \\ \\-./\\ \\ D \\ \\_\\\\\"\\_\\  \\ \\_____\\  \\ \\_____\\  \\ \\__|    \\ \\_\\  \\ \\_\\ \\ \\_\\D  \\/_/ \\/_/   \\/_____/   \\/_____/   \\/_/      \\/_/   \\/_/  \\/_/\1\a\0\0@ /$$$$$$$   /$$$$$$   /$$$$$$  /$$    /$$ /$$ /$$$$$$/$$$$ @| $$__  $$ /$$__  $$ /$$__  $$|  $$  /$$/| $$| $$_  $$_  $$@| $$  \\ $$| $$$$$$$$| $$  \\ $$ \\  $$/$$/ | $$| $$ \\ $$ \\ $$@| $$  | $$| $$_____/| $$  | $$  \\  $$$/  | $$| $$ | $$ | $$@| $$  | $$|  $$$$$$$|  $$$$$$/   \\  $/   | $$| $$ | $$ | $$@|__/  |__/ \\_______/ \\______/     \\_/    |__/|__/ |__/ |__/\1\f\0\0W     ___           ___           ___                                      ___     W    /  /\\         /  /\\         /  /\\          ___            ___        /  /\\    W   /  /::|       /  /::\\       /  /::\\        /  /\\          /__/\\      /  /::|   W  /  /:|:|      /  /:/\\:\\     /  /:/\\:\\      /  /:/          \\__\\:\\    /  /:|:|   W /  /:/|:|__   /  /::\\ \\:\\   /  /:/  \\:\\    /  /:/           /  /::\\  /  /:/|:|__ W/__/:/ |:| /\\ /__/:/\\:\\ \\:\\ /__/:/ \\__\\:\\  /__/:/  ___    __/  /:/\\/ /__/:/_|::::\\_\\__\\/  |:|/:/ \\  \\:\\ \\:\\_\\/ \\  \\:\\ /  /:/  |  |:| /  /\\  /__/\\/:/â¾â¾  \\__\\/  /â¾â¾/:/W    |  |:/:/   \\  \\:\\ \\:\\    \\  \\:\\  /:/   |  |:|/  /:/  \\  \\::/           /  /:/ W    |__|::/     \\  \\:\\_\\/     \\  \\:\\/:/    |__|:|__/:/    \\  \\:\\          /  /:/  W    /__/:/       \\  \\:\\        \\  \\::/      \\__\\::::/      \\__\\/         /__/:/   _    \\__\\/         \\__\\/         \\__\\/           â¾â¾â¾â¾                     \\__\\/    \1\f\0\0U     ___           ___           ___                                    ___     U    /__/\\         /  /\\         /  /\\          ___        ___          /__/\\    U    \\  \\:\\       /  /:/_       /  /::\\        /__/\\      /  /\\        |  |::\\   U     \\  \\:\\     /  /:/ /\\     /  /:/\\:\\       \\  \\:\\    /  /:/        |  |:|:\\  U _____\\__\\:\\   /  /:/ /:/_   /  /:/  \\:\\       \\  \\:\\  /__/::\\      __|__|:|\\:\\ U/__/::::::::\\ /__/:/ /:/ /\\ /__/:/ \\__\\:\\  ___  \\__\\:\\ \\__\\/\\:\\__  /__/::::| \\:\\a\\  \\:\\â¾â¾\\â¾â¾\\/ \\  \\:\\/:/ /:/ \\  \\:\\ /  /:/ /__/\\ |  |:|    \\  \\:\\/\\ \\  \\:\\â¾â¾\\__\\/[ \\  \\:\\  â¾â¾â¾   \\  \\::/ /:/   \\  \\:\\  /:/  \\  \\:\\|  |:|     \\__\\::/  \\  \\:\\      U  \\  \\:\\        \\  \\:\\/:/     \\  \\:\\/:/    \\  \\:\\__|:|     /__/:/    \\  \\:\\     U   \\  \\:\\        \\  \\::/       \\  \\::/      \\__\\::::/      \\__\\/      \\  \\:\\    ]    \\__\\/         \\__\\/         \\__\\/           â¾â¾â¾â¾                   \\__\\/    \1\f\0\0V     ___           ___           ___                                     ___     V    /\\  \\         /\\__\\         /\\  \\          ___                      /\\  \\    V    \\:\\  \\       /:/ _/_       /::\\  \\        /\\  \\        ___         |::\\  \\   V     \\:\\  \\     /:/ /\\__\\     /:/\\:\\  \\       \\:\\  \\      /\\__\\        |:|:\\  \\  V _____\\:\\  \\   /:/ /:/ _/_   /:/  \\:\\  \\       \\:\\  \\    /:/__/      __|:|\\:\\  \\ V/::::::::\\__\\ /:/_/:/ /\\__\\ /:/__/ \\:\\__\\  ___  \\:\\__\\  /::\\  \\     /::::|_\\:\\__\\b\\:\\â¾â¾\\â¾â¾\\/__/ \\:\\/:/ /:/  / \\:\\  \\ /:/  / /\\  \\ |:|  |  \\/\\:\\  \\__  \\:\\â¾â¾\\  \\/__/Z \\:\\  \\        \\::/_/:/  /   \\:\\  /:/  /  \\:\\  \\|:|  |   â¾â¾\\:\\/\\__\\  \\:\\  \\      V  \\:\\  \\        \\:\\/:/  /     \\:\\/:/  /    \\:\\__|:|__|      \\::/  /   \\:\\  \\     V   \\:\\__\\        \\::/  /       \\::/  /      \\::::/__/       /:/  /     \\:\\__\\    ^    \\/__/         \\/__/         \\/__/        â¾â¾â¾â¾           \\/__/       \\/__/    \1\f\0\0V     ___           ___           ___           ___                       ___     V    /\\__\\         /\\  \\         /\\  \\         /\\__\\          ___        /\\__\\    V   /::|  |       /::\\  \\       /::\\  \\       /:/  /         /\\  \\      /::|  |   V  /:|:|  |      /:/\\:\\  \\     /:/\\:\\  \\     /:/  /          \\:\\  \\    /:|:|  |   X /:/|:|  |__   /::\\â¾\\:\\  \\   /:/  \\:\\  \\   /:/__/  ___      /::\\__\\  /:/|:|__|__ V/:/ |:| /\\__\\ /:/\\:\\ \\:\\__\\ /:/__/ \\:\\__\\  |:|  | /\\__\\  __/:/\\/__/ /:/ |::::\\__\\\\\\/__|:|/:/  / \\:\\â¾\\:\\ \\/__/ \\:\\  \\ /:/  /  |:|  |/:/  / /\\/:/  /    \\/__/â¾â¾/:/  /V    |:/:/  /   \\:\\ \\:\\__\\    \\:\\  /:/  /   |:|__/:/  /  \\::/__/           /:/  / V    |::/  /     \\:\\ \\/__/     \\:\\/:/  /     \\::::/__/    \\:\\__\\          /:/  /  ^    /:/  /       \\:\\__\\        \\::/  /       â¾â¾â¾â¾         \\/__/         /:/  /   V    \\/__/         \\/__/         \\/__/                                   \\/__/    \1\a\0\0006                               __                6  ___     ___    ___   __  __ /\\_\\    ___ ___    6 / _ `\\  / __`\\ / __`\\/\\ \\/\\ \\\\/\\ \\  / __` __`\\  6/\\ \\/\\ \\/\\  __//\\ \\_\\ \\ \\ \\_/ |\\ \\ \\/\\ \\/\\ \\/\\ \\ 6\\ \\_\\ \\_\\ \\____\\ \\____/\\ \\___/  \\ \\_\\ \\_\\ \\_\\ \\_\\6 \\/_/\\/_/\\/____/\\/___/  \\/__/    \\/_/\\/_/\\/_/\\/_/\1\6\0\0000                               _           0   ____   ___   ____  _   _   (_) ____ ___ 0  / __ \\ / _ \\ / __ \\| | / / / / / __ `__ \\0 / / / //  __// /_/ /| |/ / / / / / / / / /0/_/ /_/ \\___/ \\____/ |___/ /_/ /_/ /_/ /_/ \1\6\0\0003                                _             3 _ __     ___    ___   __   __ (_)  _ __ ___  3| '_ \\   / _ \\  / _ \\  \\ \\ / / | | | '_ ` _ \\ 3| | | | |  __/ | (_) |  \\ V /  | | | | | | | |3|_| |_|  \\___|  \\___/    \\_/   |_| |_| |_| |_|\1\a\0\0002 ______                            _         2|  ___ \\                          (_)        2| |   | |   ____    ___    _   _   _   ____  2| |   | |  / _  )  / _ \\  | | | | | | |    \\ 2| |   | | ( (/ /  | |_| |  \\ V /  | | | | | |2|_|   |_|  \\____)  \\___/    \\_/   |_| |_|_|_|\1\4\0\0) _      ____  ___   _      _   _    )| |\\ | | |_  / / \\ \\ \\  / | | | |\\/|)|_| \\| |_|__ \\_\\_/  \\_\\/  |_| |_|  |\1\4\0\0#       _   _         ___      #|\\ |  |_  / \\  \\  /   |   |\\/|#| \\|  |_  \\_/   \\/   _|_  |  |\1\4\0\0/__   _ ______  _____  _    _ _____ _______/| \\  | |_____ |     |  \\  /    |   |  |  |/|  \\_| |_____ |_____|   \\/   __|__ |  |  |\1\4\0\0004ââââ¦ââââââ¦  â¦â¦ââ¦â4ââââ â£ â âââââââââ2âââââââââ ââ â©â© â©\1\4\0\0004ââââ¬ââââââ¬  â¬â¬ââ¬â4âââââ¤ â âââââââââ2âââââââââ ââ â´â´ â´\1\4\0\0004â­â®â­â¬ââ®â­ââ®â¬  â¬â¬â­â¬â®4âââââ¤ â ââ°âââ¯ââââ2â¯â°â¯â°ââ¯â°ââ¯ â°â¯ â´â´ â´\1\a\0\0)0    1    1    0    1    1    1    0)0    1    1    0    0    1    0    1)0    1    1    0    1    1    1    1)0    1    1    1    0    1    1    0)0    1    1    0    1    0    0    1)0    1    1    0    1    1    0    1\1\2\0\0%4E    45    4F    56    49    4D\1\2\0\0+116    105    117    126    111    115\1\2\0\0\24-. . --- ...- .. --\1\5\0\0/â¬â¬â¬â¬â¬â¬â¬â¬â¬â¬â¬ââ   5âââ¦â¦ââ¦ââ¦ââ¦ââ¬â¬âââ5âââââ©â£â¬â ââââ£ââââ5ââ©ââ©ââ©âââââââ©â©â©â\1\5\0\0,âââââââââââââ5âââ³â³ââ³ââ³ââ³ââââââ5âââââ»â«ââ£ââââ«ââââ5ââ»ââ»ââ»âââââââ»â»â»â\1\5\0\0008âââââââ¦âââ¦âââ¦â¦âââ8ââââ£âââ£ââââââ â£âââ8âââââââ£âââ âââ£ââââ8ââââ©âââ©âââââââ©â©â©â\1\5\0\0008âââââââ³âââ³âââ³â³âââ8ââââ«âââ«ââââââ£â«âââ8âââââââ«âââ£âââ«ââââ8ââââ»âââ»âââââââ»â»â»â\1\a\0\0\1ââââ  âââ ââââââââ  ââââââ  âââ   âââ âââ ââââ   ââââ\1âââââ âââ ââââââââ ââââââââ âââ   âââ âââ âââââ âââââ\1âââââââââ ââââââ   âââ  âââ ââââ ââââ âââ âââââââââââ\1âââââââââ ââââââ   âââ  âââ  âââââââ  âââ âââââââââââ\1âââ âââââ ââââââââ ââââââââ   âââââ   âââ âââ âââ âââ|âââ  ââââ ââââââââ  ââââââ     âââ    âââ âââ     âââ\0\27alpha.themes.dashboard\nalpha\frequire\17À\4\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/alpha-nvim",
    url = "https://github.com/goolord/alpha-nvim"
  },
  ["barbar.nvim"] = {
    config = { "\27LJ\2\n¹\3\0\0\4\0\6\0\t6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\3\0005\3\4\0=\3\5\2B\0\2\1K\0\1\0\15exclude_ft\1\2\0\0\15NvimTree_1\1\0\18\16icon_pinned\bï¤\28icon_close_tab_modified\bâ\19icon_close_tab\bï\28icon_separator_inactive\bâ\nicons\2\26icon_separator_active\bâ\23icon_custom_colors\1\14clickable\2\rclosable\2\rtabpages\2\14auto_hide\1\14animation\2\fletters:asdfjkl;ghnmxcvbziowerutyqpASDFJKLGHNMXCVBZIOWERUTYQP\21semantic_letters\2\19maximum_length\3\30\20maximum_padding\3\2\20insert_at_start\1\18insert_at_end\1\nsetup\15bufferline\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/barbar.nvim",
    url = "https://github.com/romgrk/barbar.nvim"
  },
  ["blamer.nvim"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/blamer.nvim",
    url = "https://github.com/APZelos/blamer.nvim"
  },
  ["bufdelete.nvim"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/bufdelete.nvim",
    url = "https://github.com/famiu/bufdelete.nvim"
  },
  ["cmp-buffer"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/cmp-buffer",
    url = "https://github.com/hrsh7th/cmp-buffer"
  },
  ["cmp-cmdline"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/cmp-cmdline",
    url = "https://github.com/hrsh7th/cmp-cmdline"
  },
  ["cmp-dictionary"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/cmp-dictionary",
    url = "https://github.com/uga-rosa/cmp-dictionary"
  },
  ["cmp-nvim-lsp"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/cmp-nvim-lsp",
    url = "https://github.com/hrsh7th/cmp-nvim-lsp"
  },
  ["cmp-nvim-lua"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/cmp-nvim-lua",
    url = "https://github.com/hrsh7th/cmp-nvim-lua"
  },
  ["cmp-path"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/cmp-path",
    url = "https://github.com/hrsh7th/cmp-path"
  },
  cmp_luasnip = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/cmp_luasnip",
    url = "https://github.com/saadparwaiz1/cmp_luasnip"
  },
  ["diffview.nvim"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/diffview.nvim",
    url = "https://github.com/sindrets/diffview.nvim"
  },
  ["dracula.nvim"] = {
    config = { "\27LJ\2\n¡\1\0\0\3\0\a\0\0176\0\0\0009\0\1\0+\1\2\0=\1\2\0006\0\0\0009\0\1\0+\1\1\0=\1\3\0006\0\0\0009\0\4\0'\2\5\0B\0\2\0016\0\0\0009\0\1\0)\1\1\0=\1\6\0K\0\1\0\14rehash256\24colorscheme dracula\bcmd\27dracula_transparent_bg\27dracula_italic_comment\6g\bvim\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/dracula.nvim",
    url = "https://github.com/Mofiqul/dracula.nvim"
  },
  ["editorconfig.nvim"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/editorconfig.nvim",
    url = "https://github.com/gpanders/editorconfig.nvim"
  },
  ["fidget.nvim"] = {
    config = { "\27LJ\2\n4\0\0\3\0\3\0\0066\0\0\0'\2\1\0B\0\2\0029\0\2\0B\0\1\1K\0\1\0\nsetup\vfidget\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/fidget.nvim",
    url = "https://github.com/j-hui/fidget.nvim"
  },
  ["filetype.nvim"] = {
    config = { "\27LJ\2\nx\0\0\5\0\b\0\v6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\6\0005\3\4\0005\4\3\0=\4\5\3=\3\a\2B\0\2\1K\0\1\0\14overrides\1\0\0\15extensions\1\0\0\1\0\1\tconf\15Properties\nsetup\rfiletype\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/filetype.nvim",
    url = "https://github.com/nathom/filetype.nvim"
  },
  ["friendly-snippets"] = {
    config = { "\27LJ\2\nM\0\0\3\0\3\0\0066\0\0\0'\2\1\0B\0\2\0029\0\2\0B\0\1\1K\0\1\0\14lazy_load luasnip.loaders.from_vscode\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/friendly-snippets",
    url = "https://github.com/rafamadriz/friendly-snippets"
  },
  ["git-messenger.vim"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/git-messenger.vim",
    url = "https://github.com/rhysd/git-messenger.vim"
  },
  ["gitsigns.nvim"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/gitsigns.nvim",
    url = "https://github.com/lewis6991/gitsigns.nvim"
  },
  ["hop.nvim"] = {
    config = { "\27LJ\2\nU\0\0\3\0\4\0\a6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\3\0B\0\2\1K\0\1\0\1\0\1\tkeys\28etovxqpdygfblzhckisuran\nsetup\bhop\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/hop.nvim",
    url = "https://github.com/phaazon/hop.nvim"
  },
  ["indent-blankline.nvim"] = {
    config = { "\27LJ\2\ná\3\0\0\4\0\14\0\0226\0\0\0009\0\1\0+\1\2\0=\1\2\0006\0\0\0009\0\1\0009\0\3\0\18\2\0\0009\0\4\0'\3\5\0B\0\3\0016\0\6\0'\2\a\0B\0\2\0029\0\b\0005\2\t\0005\3\n\0=\3\v\0025\3\f\0=\3\r\2B\0\2\1K\0\1\0\20buftype_exclude\1\4\0\0\rterminal\vnofile\vprompt\21filetype_exclude\1\f\0\0\thelp\bgit\rmarkdown\ttext\rterminal\flspinfo\vpacker\nalpha\20TelescopePrompt\21TelescopeResults\18lsp-installer\1\0\a\tchar\bâ\19use_treesitter\2\31show_current_context_start\2\25show_current_context\2\21show_end_of_line\2#show_trailing_blankline_indent\1\28show_first_indent_level\1\nsetup\21indent_blankline\frequire\feol:â´\vappend\14listchars\tlist\bopt\bvim\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/indent-blankline.nvim",
    url = "https://github.com/lukas-reineke/indent-blankline.nvim"
  },
  ["lsp_signature.nvim"] = {
    config = { "\27LJ\2\n\2\0\0\3\0\a\0\v5\0\0\0005\1\1\0=\1\2\0007\0\3\0006\0\4\0'\2\5\0B\0\2\0029\0\6\0006\2\3\0B\0\2\1K\0\1\0\nsetup\18lsp_signature\frequire\bcfg\17handler_opts\1\0\1\vborder\vsingle\1\0\f\ffix_pos\2\20floating_window\2\14doc_lines\3\0\16hint_enable\2\fpadding\5\vzindex\3È\1\14max_width\3x\15max_height\3\22\17hi_parameter\vSearch\16hint_scheme\vString\16hint_prefix\tï» \tbind\2\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/lsp_signature.nvim",
    url = "https://github.com/ray-x/lsp_signature.nvim"
  },
  ["lualine.nvim"] = {
    config = { "\27LJ\2\nÚ\5\0\0\b\0%\00086\0\0\0'\2\1\0B\0\2\0026\1\0\0'\3\2\0B\1\2\0029\1\3\0015\3\v\0005\4\4\0005\5\5\0=\5\6\0045\5\a\0=\5\b\0045\5\t\0=\5\n\4=\4\f\0035\4\14\0005\5\r\0=\5\15\0045\5\16\0=\5\17\0045\5\18\0005\6\21\0009\a\19\0>\a\1\0069\a\20\0=\a\22\6>\6\4\5=\5\23\0045\5\24\0=\5\25\0045\5\26\0=\5\27\0045\5\28\0=\5\29\4=\4\30\0035\4\31\0004\5\0\0=\5\15\0044\5\0\0=\5\17\0045\5 \0=\5\23\0045\5!\0=\5\25\0044\5\0\0=\5\27\0044\5\0\0=\5\29\4=\4\"\0034\4\0\0=\4#\0034\4\0\0=\4$\3B\1\2\1K\0\1\0\15extensions\ftabline\22inactive_sections\1\2\0\0\rlocation\1\2\0\0\rfilename\1\0\0\rsections\14lualine_z\1\2\0\0\rlocation\14lualine_y\1\2\0\0\rprogress\14lualine_x\1\4\0\0\rencoding\15fileformat\rfiletype\14lualine_c\tcond\1\0\0\17is_available\17get_location\1\4\0\0\rfilename\tdiff\16diagnostics\14lualine_b\1\2\0\0\vbranch\14lualine_a\1\0\0\1\2\0\0\tmode\foptions\1\0\0\23disabled_filetypes\1\3\0\0\vpacker\rNvimTree\23section_separators\1\0\2\tleft\bî°\nright\bî²\25component_separators\1\0\2\tleft\bî±\nright\bî³\1\0\3\ntheme\17dracula-nvim\18icons_enabled\2\17globalstatus\2\nsetup\flualine\15nvim-navic\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/lualine.nvim",
    url = "https://github.com/nvim-lualine/lualine.nvim"
  },
  neogit = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/neogit",
    url = "https://github.com/TimUntersberger/neogit"
  },
  ["neovim-session-manager"] = {
    config = { "\27LJ\2\n¸\3\0\0\n\0\18\0\0286\0\0\0'\2\1\0B\0\2\0026\1\0\0'\3\2\0B\1\2\0029\1\3\0015\3\n\0\18\6\0\0009\4\4\0006\a\5\0009\a\6\a9\a\a\a'\t\b\0B\a\2\2'\b\t\0B\4\4\2=\4\v\0036\4\0\0'\6\f\0B\4\2\0029\4\r\0049\4\14\4=\4\15\0035\4\16\0=\4\17\3B\1\2\1K\0\1\0\30autosave_ignore_filetypes\1\2\0\0\14gitcommit\18autoload_mode\rDisabled\17AutoloadMode\27session_manager.config\17sessions_dir\1\0\6\20max_path_length\3P\29autosave_only_in_session\2\31autosave_ignore_not_normal\2\26autosave_last_session\2\19colon_replacer\a++\18path_replacer\a__\rsessions\tdata\fstdpath\afn\bvim\bnew\nsetup\20session_manager\17plenary.path\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/neovim-session-manager",
    url = "https://github.com/Shatur/neovim-session-manager"
  },
  ["numb.nvim"] = {
    config = { "\27LJ\2\nF\0\0\3\0\4\0\a6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\3\0B\0\2\1K\0\1\0\1\0\1\16number_only\2\nsetup\tnumb\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/numb.nvim",
    url = "https://github.com/nacro90/numb.nvim"
  },
  ["nvim-autopairs"] = {
    config = { "\27LJ\2\n<\0\0\3\0\3\0\0066\0\0\0'\2\1\0B\0\2\0029\0\2\0B\0\1\1K\0\1\0\nsetup\19nvim-autopairs\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/nvim-autopairs",
    url = "https://github.com/windwp/nvim-autopairs"
  },
  ["nvim-cmp"] = {
    config = { "\27LJ\2\nÐ\1\0\0\b\0\b\2!6\0\0\0006\2\1\0009\2\2\0029\2\3\2)\4\0\0B\2\2\0A\0\0\3\b\1\0\0X\2\206\2\1\0009\2\2\0029\2\4\2)\4\0\0\23\5\1\0\18\6\0\0+\a\2\0B\2\5\2:\2\1\2\18\4\2\0009\2\5\2\18\5\1\0\18\6\1\0B\2\4\2\18\4\2\0009\2\6\2'\5\a\0B\2\3\2\n\2\0\0X\2\2+\2\1\0X\3\1+\2\2\0L\2\2\0\a%s\nmatch\bsub\23nvim_buf_get_lines\24nvim_win_get_cursor\bapi\bvim\vunpack\0\2-\0\1\4\1\2\0\5-\1\0\0009\1\0\0019\3\1\0B\1\2\1K\0\1\0\2À\tbody\15lsp_expandÁ\1\0\2\b\0\n\0\0176\2\0\0'\4\1\0B\2\2\0026\3\3\0009\3\4\3'\5\5\0009\6\2\0018\6\6\0029\a\2\1B\3\4\2=\3\2\0015\3\a\0009\4\b\0009\4\t\0048\3\4\3=\3\6\1L\1\2\0\tname\vsource\1\0\3\rnvim_lsp\n[LSP]\vbuffer\n[BUF]\rnvim_lua\n[Lua]\tmenu\n%s %s\vformat\vstring\tkind\26plugins.lspkind-icons\frequireÅ\1\0\1\3\3\5\0\29-\1\0\0009\1\0\1B\1\1\2\15\0\1\0X\2\4-\1\0\0009\1\1\1B\1\1\1X\1\19-\1\1\0009\1\2\1B\1\1\2\15\0\1\0X\2\4-\1\1\0009\1\3\1B\1\1\1X\1\n-\1\2\0B\1\1\2\15\0\1\0X\2\4-\1\0\0009\1\4\1B\1\1\1X\1\2\18\1\0\0B\1\1\1K\0\1\0\0À\2À\4À\rcomplete\19expand_or_jump\23expand_or_jumpable\21select_next_item\fvisible\1\0\1\4\2\4\0\23-\1\0\0009\1\0\1B\1\1\2\15\0\1\0X\2\4-\1\0\0009\1\1\1B\1\1\1X\1\r-\1\1\0009\1\2\1)\3ÿÿB\1\2\2\15\0\1\0X\2\5-\1\1\0009\1\3\1)\3ÿÿB\1\2\1X\1\2\18\1\0\0B\1\1\1K\0\1\0\0À\2À\tjump\rjumpable\21select_prev_item\fvisible\1\0\0\5\0\a\0\v6\0\0\0'\2\1\0B\0\2\0029\1\2\0005\3\4\0005\4\3\0=\4\5\3B\1\2\0019\1\6\0B\1\1\1K\0\1\0\vupdate\bdic\1\0\0\1\0\1\rphp,html$~/.config/nvim/dicts/bootstrap5\nsetup\19cmp_dictionary\frequireú\t\1\0\r\0F\0}6\0\0\0'\2\1\0B\0\2\0026\1\0\0'\3\2\0B\1\2\0026\2\0\0'\4\3\0B\2\2\0026\3\4\0009\3\5\0033\4\6\0009\5\a\0\18\a\5\0009\5\b\5'\b\t\0009\t\n\1B\t\1\0A\5\2\0019\5\v\0005\a\15\0005\b\r\0003\t\f\0=\t\14\b=\b\16\a5\b\18\0003\t\17\0=\t\19\b=\b\20\a5\b\21\0=\b\22\a5\b\25\0009\t\23\0009\t\24\tB\t\1\2=\t\26\b9\t\23\0009\t\27\tB\t\1\2=\t\28\b9\t\23\0009\t\29\t)\vüÿB\t\2\2=\t\30\b9\t\23\0009\t\29\t)\v\4\0B\t\2\2=\t\31\b9\t\23\0009\t \tB\t\1\2=\t!\b9\t\23\0009\t\"\tB\t\1\2=\t#\b9\t\23\0009\t$\t5\v'\0009\f%\0009\f&\f=\f(\vB\t\2\2=\t)\b9\t\23\0003\v*\0005\f+\0B\t\3\2=\t,\b9\t\23\0003\v-\0005\f.\0B\t\3\2=\t/\b=\b\23\a4\b\a\0005\t0\0>\t\1\b5\t1\0>\t\2\b5\t2\0>\t\3\b5\t3\0>\t\4\b5\t4\0>\t\5\b5\t5\0>\t\6\b=\b6\aB\5\2\0019\5\v\0009\0057\5'\a8\0005\b:\0005\t9\0=\t6\bB\5\3\0019\5\v\0009\0057\5'\a;\0005\b>\0004\t\3\0005\n<\0>\n\1\t5\n=\0>\n\2\t=\t6\bB\5\3\0016\5\0\0'\a?\0B\5\2\0029\5\v\0055\a@\0004\b\0\0=\bA\aB\5\2\0013\5B\0007\5C\0009\5D\3'\aE\0B\5\2\0012\0\0K\0\1\0000command! DictBoostrap5 :lua DictBoostrap5()\17nvim_command\18DictBoostrap5\0\bdic\1\0\1\nasync\2\19cmp_dictionary\1\0\0\1\0\2\20keyword_pattern5[^0-9]\\%(-\\?\\d\\+\\%(\\.\\d\\+\\)\\?\\|\\h\\w*\\%(-\\w*\\)*\\)\tname\fcmdline\1\0\1\tname\tpath\6:\1\0\0\1\0\1\tname\vbuffer\6/\fcmdline\fsources\1\0\2\19keyword_length\3\2\tname\15dictionary\1\0\1\tname\tpath\1\0\1\tname\vbuffer\1\0\1\tname\rnvim_lua\1\0\1\tname\rnvim_lsp\1\0\1\tname\fluasnip\f<S-Tab>\1\4\0\0\6i\6s\6c\0\n<Tab>\1\4\0\0\6i\6s\6c\0\t<CR>\rbehavior\1\0\1\vselect\2\fReplace\20ConfirmBehavior\fconfirm\n<C-e>\nclose\14<C-Space>\rcomplete\n<C-f>\n<C-d>\16scroll_docs\n<C-p>\21select_prev_item\n<C-n>\1\0\0\21select_next_item\fmapping\15completion\1\0\2\19keyword_length\3\2\16completeopt\26menu,menuone,noselect\15formatting\vformat\1\0\0\0\fsnippet\1\0\0\vexpand\1\0\0\0\nsetup\20on_confirm_done\17confirm_done\aon\nevent\0\bapi\bvim\fluasnip\"nvim-autopairs.completion.cmp\bcmp\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/nvim-cmp",
    url = "https://github.com/hrsh7th/nvim-cmp"
  },
  ["nvim-colorizer.lua"] = {
    config = { "\27LJ\2\n7\0\0\3\0\3\0\0066\0\0\0'\2\1\0B\0\2\0029\0\2\0B\0\1\1K\0\1\0\nsetup\14colorizer\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/nvim-colorizer.lua",
    url = "https://github.com/norcalli/nvim-colorizer.lua"
  },
  ["nvim-lsp-installer"] = {
    config = { "\27LJ\2\nA\2\0\4\1\3\0\a6\0\0\0009\0\1\0009\0\2\0-\2\0\0G\3\0\0A\0\1\1K\0\1\0\1À\24nvim_buf_set_keymap\bapi\bvimA\2\0\4\1\3\0\a6\0\0\0009\0\1\0009\0\2\0-\2\0\0G\3\0\0A\0\1\1K\0\1\0\1À\24nvim_buf_set_option\bapi\bvim¼\4\1\2\v\1\19\00003\2\0\0003\3\1\0\18\4\3\0'\6\2\0'\a\3\0B\4\3\0015\4\4\0\18\5\2\0'\a\5\0'\b\6\0'\t\a\0\18\n\4\0B\5\5\1\18\5\2\0'\a\5\0'\b\b\0'\t\t\0\18\n\4\0B\5\5\1\18\5\2\0'\a\5\0'\b\n\0'\t\v\0\18\n\4\0B\5\5\1\18\5\2\0'\a\5\0'\b\f\0'\t\r\0\18\n\4\0B\5\5\1\18\5\2\0'\a\5\0'\b\14\0'\t\15\0\18\n\4\0B\5\5\0019\5\16\0009\5\17\5\15\0\5\0X\6\5-\5\0\0009\5\18\5\18\a\0\0\18\b\1\0B\5\3\0012\0\0K\0\1\0\1À\vattach\27documentSymbolProvider\24server_capabilities*<cmd>lua vim.lsp.buf.formatting()<CR>\19<localleader>f&<cmd>lua vim.lsp.buf.rename()<CR>\20<localleader>rn.<cmd>lua vim.lsp.buf.signature_help()<CR>\20<localleader>ls%<cmd>lua vim.lsp.buf.hover()<CR>\20<localleader>lh+<cmd>lua vim.lsp.buf.declaration()<CR>\20<localleader>ld\6n\1\0\2\vsilent\1\fnoremap\2\27v:lua.vim.lsp.omnifunc\romnifunc\0\0\1\0\1\6\1\v\0\0165\1\0\0-\2\0\0=\2\1\0015\2\a\0005\3\5\0005\4\3\0005\5\2\0=\5\4\4=\4\6\3=\3\b\2=\2\t\1\18\4\0\0009\2\n\0\18\5\1\0B\2\3\1K\0\1\0\2À\nsetup\rsettings\bLua\1\0\0\16diagnostics\1\0\0\fglobals\1\0\0\1\2\0\0\bvim\14on_attach\1\0\0å\1\1\0\b\0\f\0\0196\0\0\0'\2\1\0B\0\2\0026\1\0\0'\3\2\0B\1\2\0023\2\3\0009\3\4\0003\5\5\0B\3\2\0019\3\6\0005\5\n\0005\6\b\0005\a\a\0=\a\t\6=\6\v\5B\3\2\0012\0\0K\0\1\0\aui\1\0\0\nicons\1\0\0\1\0\3\23server_uninstalled\bâ\19server_pending\bâ\21server_installed\bâ\rsettings\0\20on_server_ready\0\15nvim-navic\23nvim-lsp-installer\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/nvim-lsp-installer",
    url = "https://github.com/williamboman/nvim-lsp-installer"
  },
  ["nvim-lspconfig"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/nvim-lspconfig",
    url = "https://github.com/neovim/nvim-lspconfig"
  },
  ["nvim-navic"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/nvim-navic",
    url = "https://github.com/SmiteshP/nvim-navic"
  },
  ["nvim-tree.lua"] = {
    config = { "\27LJ\2\n(\0\1\5\0\3\0\0056\1\0\0009\1\1\1'\3\2\0\18\4\0\0D\1\3\0\6 \brep\vstringZ\0\0\6\1\3\0\n-\0\0\0009\0\0\0)\2)\0006\3\1\0)\5\r\0B\3\2\2'\4\2\0&\3\4\3B\0\3\1K\0\1\0\1À\18File Explorer\20Add_whitespaces\15set_offset(\0\0\3\1\1\0\5-\0\0\0009\0\0\0)\2\0\0B\0\2\1K\0\1\0\1À\15set_offset¿\5\1\0\b\0 \0*6\0\0\0'\2\1\0B\0\2\0026\1\0\0'\3\2\0B\1\2\0026\2\0\0'\4\3\0B\2\2\0029\2\4\0025\4\5\0005\5\6\0=\5\a\0045\5\b\0005\6\t\0=\6\n\5=\5\v\0045\5\f\0=\5\r\0045\5\15\0005\6\14\0=\6\16\5=\5\17\0045\5\18\0=\5\19\0045\5\23\0005\6\21\0005\a\20\0=\a\22\6=\6\24\5=\5\25\4B\2\2\0013\2\26\0007\2\27\0009\2\28\0003\4\29\0B\2\2\0019\2\30\0003\4\31\0B\2\2\0012\0\0K\0\1\0\0\18on_tree_close\0\17on_tree_open\20Add_whitespaces\0\factions\14open_file\1\0\0\18window_picker\1\0\0\1\0\1\venable\1\24update_focused_file\1\0\1\venable\2\rrenderer\19indent_markers\1\0\2\27highlight_opened_files\ball\18highlight_git\2\1\0\1\venable\2\tview\1\0\4\nwidth\3(\21hide_root_folder\2\tside\tleft\18adaptive_size\2\ffilters\vcustom\1\5\0\0\t.git\17node_modules\v.cache\t.bin\1\0\1\rdotfiles\2\16diagnostics\1\0\1\venable\1\1\0\6'hijack_unnamed_buffer_when_opening\2\17hijack_netrw\2\18hijack_cursor\2\20respect_buf_cwd\2\23sync_root_with_cwd\2\18disable_netrw\1\nsetup\14nvim-tree\21bufferline.state\21nvim-tree.events\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/nvim-tree.lua",
    url = "https://github.com/kyazdani42/nvim-tree.lua"
  },
  ["nvim-treesitter"] = {
    config = { "\27LJ\2\n§\2\0\0\4\0\f\0\0156\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\4\0005\3\3\0=\3\5\0025\3\6\0=\3\a\0025\3\b\0=\3\t\0025\3\n\0=\3\v\2B\0\2\1K\0\1\0\frainbow\1\0\2\venable\2\18extended_mode\2\14highlight\1\0\2\venable\2\21use_languagetree\2\fautotag\1\0\1\venable\2\21ensure_installed\1\0\0\1\r\0\0\blua\bvim\thtml\15javascript\vpython\tjava\fc_sharp\bcss\tbash\tjson\thttp\15typescript\nsetup\28nvim-treesitter.configs\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/nvim-treesitter",
    url = "https://github.com/nvim-treesitter/nvim-treesitter"
  },
  ["nvim-ts-autotag"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/nvim-ts-autotag",
    url = "https://github.com/windwp/nvim-ts-autotag"
  },
  ["nvim-ts-rainbow"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/nvim-ts-rainbow",
    url = "https://github.com/p00f/nvim-ts-rainbow"
  },
  ["nvim-web-devicons"] = {
    config = { "\27LJ\2\n\b\0\0\5\0<\0?6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2:\0005\3\4\0005\4\3\0=\4\5\0035\4\6\0=\4\a\0035\4\b\0=\4\t\0035\4\n\0=\4\v\0035\4\f\0=\4\r\0035\4\14\0=\4\15\0035\4\16\0=\4\17\0035\4\18\0=\4\19\0035\4\20\0=\4\21\0035\4\22\0=\4\23\0035\4\24\0=\4\25\0035\4\26\0=\4\27\0035\4\28\0=\4\29\0035\4\30\0=\4\31\0035\4 \0=\4!\0035\4\"\0=\4#\0035\4$\0=\4%\0035\4&\0=\4'\0035\4(\0=\4)\0035\4*\0=\4+\0035\4,\0=\4-\0035\4.\0=\4/\0035\0040\0=\0041\0035\0042\0=\0043\0035\0044\0=\0045\0035\0046\0=\0047\0035\0048\0=\0049\3=\3;\2B\0\2\1K\0\1\0\roverride\1\0\0\bzip\1\0\2\tname\bzip\ticon\bï\axz\1\0\2\tname\axz\ticon\bï\nwoff2\1\0\2\tname\23WebOpenFontFormat2\ticon\bï±\twoff\1\0\2\tname\22WebOpenFontFormat\ticon\bï±\bvue\1\0\2\tname\bvue\ticon\bïµ\brpm\1\0\2\tname\brpm\ticon\bï\arb\1\0\2\tname\arb\ticon\bî\bttf\1\0\2\tname\17TrueTypeFont\ticon\bï±\ats\1\0\2\tname\ats\ticon\bï¯¤\ttoml\1\0\2\tname\ttoml\ticon\bï\15robots.txt\1\0\2\tname\vrobots\ticon\bï®§\apy\1\0\2\tname\apy\ticon\bî\bpng\1\0\2\tname\bpng\ticon\bï\bout\1\0\2\tname\bout\ticon\bî\bmp4\1\0\2\tname\bmp4\ticon\bï\bmp3\1\0\2\tname\bmp3\ticon\bï¢\blua\1\0\2\tname\blua\ticon\bî \tlock\1\0\2\tname\tlock\ticon\bï ½\akt\1\0\2\tname\akt\ticon\tó±\ajs\1\0\2\tname\ajs\ticon\bï \bjpg\1\0\2\tname\bjpg\ticon\bï\tjpeg\1\0\2\tname\tjpeg\ticon\bï\thtml\1\0\2\tname\thtml\ticon\bï»\15Dockerfile\1\0\2\tname\15Dockerfile\ticon\bï\bdeb\1\0\2\tname\bdeb\ticon\bï\bcss\1\0\2\tname\bcss\ticon\bî\6c\1\0\0\1\0\2\tname\6c\ticon\bî\nsetup\22nvim-web-devicons\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/nvim-web-devicons",
    url = "https://github.com/kyazdani42/nvim-web-devicons"
  },
  ["packer.nvim"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/packer.nvim",
    url = "https://github.com/wbthomason/packer.nvim"
  },
  ["plenary.nvim"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/plenary.nvim",
    url = "https://github.com/nvim-lua/plenary.nvim"
  },
  ["refactoring.nvim"] = {
    config = { "\27LJ\2\n8\0\0\3\0\2\0\0046\0\0\0'\2\1\0B\0\2\1K\0\1\0\29plugins.plug-refactoring\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/refactoring.nvim",
    url = "https://github.com/ThePrimeagen/refactoring.nvim"
  },
  ["rest.nvim"] = {
    config = { "\27LJ\2\n7\0\0\3\0\3\0\0066\0\0\0'\2\1\0B\0\2\0029\0\2\0B\0\1\1K\0\1\0\nsetup\14rest-nvim\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/rest.nvim",
    url = "https://github.com/NTBBloodbath/rest.nvim"
  },
  tabular = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/tabular",
    url = "https://github.com/godlygeek/tabular"
  },
  ["telescope-fzf-native.nvim"] = {
    config = { "\27LJ\2\nH\0\0\3\0\4\0\a6\0\0\0'\2\1\0B\0\2\0029\0\2\0'\2\3\0B\0\2\1K\0\1\0\bfzf\19load_extension\14telescope\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/telescope-fzf-native.nvim",
    url = "https://github.com/nvim-telescope/telescope-fzf-native.nvim"
  },
  ["telescope-ui-select.nvim"] = {
    config = { "\27LJ\2\nN\0\0\3\0\4\0\a6\0\0\0'\2\1\0B\0\2\0029\0\2\0'\2\3\0B\0\2\1K\0\1\0\14ui-select\19load_extension\14telescope\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/telescope-ui-select.nvim",
    url = "https://github.com/nvim-telescope/telescope-ui-select.nvim"
  },
  ["telescope.nvim"] = {
    config = { "\27LJ\2\nï\t\0\0\t\0,\1I6\0\0\0'\2\1\0B\0\2\0029\1\2\0005\3#\0005\4\4\0005\5\3\0=\5\5\0045\5\a\0005\6\6\0=\6\b\0055\6\t\0=\6\n\5=\5\v\0046\5\0\0'\a\f\0B\5\2\0029\5\r\5=\5\14\0045\5\15\0=\5\16\0046\5\0\0'\a\f\0B\5\2\0029\5\17\5=\5\18\0045\5\19\0=\5\20\0044\5\0\0=\5\21\0045\5\22\0=\5\23\0045\5\24\0=\5\25\0046\5\0\0'\a\26\0B\5\2\0029\5\27\0059\5\28\5=\5\29\0046\5\0\0'\a\26\0B\5\2\0029\5\30\0059\5\28\5=\5\31\0046\5\0\0'\a\26\0B\5\2\0029\5 \0059\5\28\5=\5!\0046\5\0\0'\a\26\0B\5\2\0029\5\"\5=\5\"\4=\4$\0035\4'\0004\5\3\0006\6\0\0'\b%\0B\6\2\0029\6&\0064\b\0\0B\6\2\0?\6\0\0=\5(\0045\5)\0=\5*\4=\4+\3B\1\2\1K\0\1\0\15extensions\bfzf\1\0\1\nfuzzy\1\14ui-select\1\0\0\17get_dropdown\21telescope.themes\rdefaults\1\0\0\27buffer_previewer_maker\21qflist_previewer\22vim_buffer_qflist\19grep_previewer\23vim_buffer_vimgrep\19file_previewer\bnew\19vim_buffer_cat\25telescope.previewers\fset_env\1\0\1\14COLORTERM\14truecolor\16borderchars\1\t\0\0\bâ\bâ\bâ\bâ\bâ­\bâ®\bâ¯\bâ°\vborder\17path_display\1\2\0\0\rtruncate\19generic_sorter\29get_generic_fuzzy_sorter\25file_ignore_patterns\1\2\0\0\17node_modules\16file_sorter\19get_fuzzy_file\22telescope.sorters\18layout_config\rvertical\1\0\1\vmirror\1\15horizontal\1\0\3\nwidth\4×ÇÂë\3®¯ÿ\3\vheight\4³æÌ\t³¦ÿ\3\19preview_cutoff\3x\1\0\3\18results_width\4³æÌ\t³¦ÿ\3\18preview_width\4³æÌ\t³ÿ\3\20prompt_position\btop\22vimgrep_arguments\1\0\n\23selection_strategy\nreset\17initial_mode\vinsert\rwinblend\3\0\20selection_caret\a  \19color_devicons\2\18prompt_prefix\v ï  \ruse_less\2\17entry_prefix\a  \20layout_strategy\15horizontal\21sorting_strategy\14ascending\1\b\0\0\arg\18--color=never\17--no-heading\20--with-filename\18--line-number\r--column\17--smart-case\nsetup\14telescope\frequire\3À\4\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/telescope.nvim",
    url = "https://github.com/nvim-telescope/telescope.nvim"
  },
  ["text-case.nvim"] = {
    config = { "\27LJ\2\n\2\0\0\6\0\14\0\0296\0\0\0'\2\1\0B\0\2\0029\0\2\0004\2\0\0B\0\2\0016\0\0\0'\2\3\0B\0\2\0029\0\4\0'\2\1\0B\0\2\0016\0\5\0009\0\6\0009\0\a\0'\2\b\0'\3\t\0'\4\n\0005\5\v\0B\0\5\0016\0\5\0009\0\6\0009\0\a\0'\2\f\0'\3\t\0'\4\n\0005\5\r\0B\0\5\1K\0\1\0\1\0\1\tdesc\14Telescope\6v\1\0\1\tdesc\14Telescope#<cmd>TextCaseOpenTelescope<CR>\bga.\6n\20nvim_set_keymap\bapi\bvim\19load_extension\14telescope\nsetup\rtextcase\frequire\0" },
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/text-case.nvim",
    url = "https://github.com/johmsalas/text-case.nvim"
  },
  ["unicode.vim"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/unicode.vim",
    url = "https://github.com/chrisbra/unicode.vim"
  },
  ["vim-eunuch"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/vim-eunuch",
    url = "https://github.com/tpope/vim-eunuch"
  },
  ["vim-surround"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/vim-surround",
    url = "https://github.com/tpope/vim-surround"
  },
  ["vim-visual-multi"] = {
    loaded = true,
    path = "/home/blackblex/.local/share/nvim/site/pack/packer/start/vim-visual-multi",
    url = "https://github.com/mg979/vim-visual-multi"
  }
}

time([[Defining packer_plugins]], false)
-- Config for: nvim-autopairs
time([[Config for nvim-autopairs]], true)
try_loadstring("\27LJ\2\n<\0\0\3\0\3\0\0066\0\0\0'\2\1\0B\0\2\0029\0\2\0B\0\1\1K\0\1\0\nsetup\19nvim-autopairs\frequire\0", "config", "nvim-autopairs")
time([[Config for nvim-autopairs]], false)
-- Config for: nvim-lsp-installer
time([[Config for nvim-lsp-installer]], true)
try_loadstring("\27LJ\2\nA\2\0\4\1\3\0\a6\0\0\0009\0\1\0009\0\2\0-\2\0\0G\3\0\0A\0\1\1K\0\1\0\1À\24nvim_buf_set_keymap\bapi\bvimA\2\0\4\1\3\0\a6\0\0\0009\0\1\0009\0\2\0-\2\0\0G\3\0\0A\0\1\1K\0\1\0\1À\24nvim_buf_set_option\bapi\bvim¼\4\1\2\v\1\19\00003\2\0\0003\3\1\0\18\4\3\0'\6\2\0'\a\3\0B\4\3\0015\4\4\0\18\5\2\0'\a\5\0'\b\6\0'\t\a\0\18\n\4\0B\5\5\1\18\5\2\0'\a\5\0'\b\b\0'\t\t\0\18\n\4\0B\5\5\1\18\5\2\0'\a\5\0'\b\n\0'\t\v\0\18\n\4\0B\5\5\1\18\5\2\0'\a\5\0'\b\f\0'\t\r\0\18\n\4\0B\5\5\1\18\5\2\0'\a\5\0'\b\14\0'\t\15\0\18\n\4\0B\5\5\0019\5\16\0009\5\17\5\15\0\5\0X\6\5-\5\0\0009\5\18\5\18\a\0\0\18\b\1\0B\5\3\0012\0\0K\0\1\0\1À\vattach\27documentSymbolProvider\24server_capabilities*<cmd>lua vim.lsp.buf.formatting()<CR>\19<localleader>f&<cmd>lua vim.lsp.buf.rename()<CR>\20<localleader>rn.<cmd>lua vim.lsp.buf.signature_help()<CR>\20<localleader>ls%<cmd>lua vim.lsp.buf.hover()<CR>\20<localleader>lh+<cmd>lua vim.lsp.buf.declaration()<CR>\20<localleader>ld\6n\1\0\2\vsilent\1\fnoremap\2\27v:lua.vim.lsp.omnifunc\romnifunc\0\0\1\0\1\6\1\v\0\0165\1\0\0-\2\0\0=\2\1\0015\2\a\0005\3\5\0005\4\3\0005\5\2\0=\5\4\4=\4\6\3=\3\b\2=\2\t\1\18\4\0\0009\2\n\0\18\5\1\0B\2\3\1K\0\1\0\2À\nsetup\rsettings\bLua\1\0\0\16diagnostics\1\0\0\fglobals\1\0\0\1\2\0\0\bvim\14on_attach\1\0\0å\1\1\0\b\0\f\0\0196\0\0\0'\2\1\0B\0\2\0026\1\0\0'\3\2\0B\1\2\0023\2\3\0009\3\4\0003\5\5\0B\3\2\0019\3\6\0005\5\n\0005\6\b\0005\a\a\0=\a\t\6=\6\v\5B\3\2\0012\0\0K\0\1\0\aui\1\0\0\nicons\1\0\0\1\0\3\23server_uninstalled\bâ\19server_pending\bâ\21server_installed\bâ\rsettings\0\20on_server_ready\0\15nvim-navic\23nvim-lsp-installer\frequire\0", "config", "nvim-lsp-installer")
time([[Config for nvim-lsp-installer]], false)
-- Config for: hop.nvim
time([[Config for hop.nvim]], true)
try_loadstring("\27LJ\2\nU\0\0\3\0\4\0\a6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\3\0B\0\2\1K\0\1\0\1\0\1\tkeys\28etovxqpdygfblzhckisuran\nsetup\bhop\frequire\0", "config", "hop.nvim")
time([[Config for hop.nvim]], false)
-- Config for: alpha-nvim
time([[Config for alpha-nvim]], true)
try_loadstring("\27LJ\2\n>\0\0\3\0\3\0\0056\0\0\0009\0\1\0'\2\2\0B\0\2\2L\0\2\0\31%A, %d %B %Y ~ %I:%M:%S %p\tdate\aosÂ¢\1\1\0\v\0W\1¯\0016\0\0\0'\2\1\0B\0\2\0026\1\0\0'\3\2\0B\1\2\0023\2\3\0004\3/\0005\4\4\0>\4\1\0035\4\5\0>\4\2\0035\4\6\0>\4\3\0035\4\a\0>\4\4\0035\4\b\0>\4\5\0035\4\t\0>\4\6\0035\4\n\0>\4\a\0035\4\v\0>\4\b\0035\4\f\0>\4\t\0035\4\r\0>\4\n\0035\4\14\0>\4\v\0035\4\15\0>\4\f\0035\4\16\0>\4\r\0035\4\17\0>\4\14\0035\4\18\0>\4\15\0035\4\19\0>\4\16\0035\4\20\0>\4\17\0035\4\21\0>\4\18\0035\4\22\0>\4\19\0035\4\23\0>\4\20\0035\4\24\0>\4\21\0035\4\25\0>\4\22\0035\4\26\0>\4\23\0035\4\27\0>\4\24\0035\4\28\0>\4\25\0035\4\29\0>\4\26\0035\4\30\0>\4\27\0035\4\31\0>\4\28\0035\4 \0>\4\29\0035\4!\0>\4\30\0035\4\"\0>\4\31\0035\4#\0>\4 \0035\4$\0>\4!\0035\4%\0>\4\"\0035\4&\0>\4#\0035\4'\0>\4$\0035\4(\0>\4%\0035\4)\0>\4&\0035\4*\0>\4'\0035\4+\0>\4(\0035\4,\0>\4)\0035\4-\0>\4*\0035\4.\0>\4+\0035\4/\0>\4,\0035\0040\0>\4-\0035\0041\0>\4.\0036\0042\0009\0043\0046\0064\0009\0065\6B\6\1\0A\4\0\0019\0046\0019\0047\0046\0052\0009\0059\5\21\a\3\0B\5\2\0028\5\5\3=\0058\0049\0046\0019\4:\0044\5\t\0009\6;\1'\b<\0'\t=\0'\n>\0B\6\4\2>\6\1\0059\6;\1'\b?\0'\t@\0'\nA\0B\6\4\2>\6\2\0059\6;\1'\bB\0'\tC\0'\nD\0B\6\4\2>\6\3\0059\6;\1'\bE\0'\tF\0'\nG\0B\6\4\2>\6\4\0059\6;\1'\bH\0'\tI\0'\nJ\0B\6\4\2>\6\5\0059\6;\1'\bK\0'\tL\0'\nM\0B\6\4\2>\6\6\0059\6;\1'\bN\0'\tO\0'\nP\0B\6\4\2>\6\a\0059\6;\1'\bQ\0'\tR\0'\nS\0B\6\4\0?\6\0\0=\0058\0049\0046\0019\4T\4\18\5\2\0B\5\1\2=\0058\0049\4U\0009\6V\1B\4\2\1K\0\1\0\topts\nsetup\vfooter\f:qa<CR>\16ï  > Quit\6q\22:PackerUpdate<CR>\26ï  > Update plugins\6u\20:e $MYVIMRC<CR>\20ï  > Settings\6s%:SessionManager load_session<CR>\24ï¤  > Open session\6c\28:Telescope oldfiles<CR>\31î  > Recently used files\6r\30:Telescope find_files<CR>\21ï  > Find file\6f\22:NvimTreeOpen<CR>\21ï  > Open file\6o\31:ene <BAR> startinsert<CR>\20ï  > New file\6e\vbutton\fbuttons\vrandom\bval\vheader\fsection\ttime\aos\15randomseed\tmath\1\f\0\0J   â£´â£¶â£¤â¡¤â ¦â£¤â£â£¤â      â£â£­â£¿â£¶â£¿â£¦â£¼â£          X    â â »â¢¿â£¿â ¿â£¿â£¿â£¶â£¦â ¤â â¡ â¢¾â£¿â£¿â¡¿â â â â »â£¿â£¿â¡â£¦       F          â â¢¿â£¿â£â ¦ â£¾â£¿â£¿â£·    â »â ¿â¢¿â£¿â£§â£     N           â£¸â£¿â£¿â¢§ â¢»â »â£¿â£¿â£·â£â£â â ¢â£â¡â â â ¿â     L          â¢ â£¿â£¿â£¿â     â£»â£¿â£¿â£¿â£¿â£¿â£¿â£¿â£â£³â£¤â£â£   ^   â¢ â£§â£¶â£¥â¡¤â¢ â£¸â£¿â£¿â   â¢â£´â£¿â£¿â¡¿â â£¿â£¿â£§â â¢¿â ¿â â â »â ¿â   X  â£°â£¿â£¿â â »â£¿â£¿â¡¦â¢¹â£¿â£·   â¢â£¿â£¿â¡  â¢¸â£¿â£¿â¡ â¢â£ â£â£¾â    b â£ â£¿â ¿â  â¢â£¿â£¿â£·â â¢¿â£¿â£¦â¡ â¢¸â¢¿â£¿â£¿â£ â£¸â£¿â£¿â¡â£ªâ£¿â¡¿â ¿â£¿â£·â¡  \\ â â    â£¼â£¿â¡  â â »â£¿â£¿â£¦â£â¡â »â£¿â£¿â£·â£¿â£¿â£¿ â£¿â£¿â¡ â â »â¢·â£ P      â¢»â£¿â£¿â£   â â »â£¿â£¿â£¿â£·â£¿â£¿â£¿â£¿â£¿â¡ â «â¢¿â£¿â¡     V       â »â£¿â£¿â£¿â£¿â£¶â£¶â£¾â£¿â£¿â£¿â£¿â£¿â£¿â£¿â£¿â¡â¢â£â£¤â£¾â¡¿â      \1\a\0\0007                                __                7   ___     ___    ___   __  __ /\\_\\    ___ ___    7  / _ `\\  / __`\\ / __`\\/\\ \\/\\ \\\\/\\ \\  / __` __`\\  7 /\\ \\/\\ \\/\\  __//\\ \\_\\ \\ \\ \\_/ |\\ \\ \\/\\ \\/\\ \\/\\ \\ 7 \\ \\_\\ \\_\\ \\____\\ \\____/\\ \\___/  \\ \\_\\ \\_\\ \\_\\ \\_\\7  \\/_/\\/_/\\/____/\\/___/  \\/__/    \\/_/\\/_/\\/_/\\/_/\1\a\0\0001 _______             ____   ____.__         1 \\      \\   ____  ___\\   \\ /   /|__| _____  1 /   |   \\_/ __ \\/  _ \\   Y   / |  |/     \\ 1/    |    \\  ___(  <_> )     /  |  |  Y Y  \\1\\____|__  /\\___  >____/ \\___/   |__|__|_|  /1        \\/     \\/                        \\/ \1\t\0\0009                                                    \1 ââââ   âââââââââââ âââââââ âââ   ââââââââââ   ââââ \1 âââââ  âââââââââââââââââââââââ   âââââââââââ âââââ \1 ââââââ âââââââââ  âââ   ââââââ   âââââââââââââââââ \1 ââââââââââââââââ  âââ   âââââââ ââââââââââââââââââ \1 âââ âââââââââââââââââââââââ âââââââ ââââââ âââ âââ \1 âââ  âââââââââââââ âââââââ   âââââ  ââââââ     âââ 9                                                    \1\b\0\0X      ::::    :::   ::::::::::   ::::::::   :::     :::   :::::::::::     :::   :::X     :+:+:   :+:   :+:         :+:    :+:  :+:     :+:       :+:        :+:+: :+:+:X    :+:+:+  +:+   +:+         +:+    +:+  +:+     +:+       +:+       +:+ +:+:+ +:+X   +#+ +:+ +#+   +#++:++#    +#+    +:+  +#+     +:+       +#+       +#+  +:+  +#+ X  +#+  +#+#+#   +#+         +#+    +#+   +#+   +#+        +#+       +#+       +#+  X #+#   #+#+#   #+#         #+#    #+#    #+#+#+#         #+#       #+#       #+#   X###    ####   ##########   ########       ###       ###########   ###       ###    \1\b\0\0Tooooo      ooo oooooooooooo   .oooooo.   oooooo     oooo ooooo ooo        oooooT`888b.     `8' `888'     `8  d8P'  `Y8b   `888.     .8'  `888' `88.       .888'T 8 `88b.    8   888         888      888   `888.   .8'    888   888b     d'888 T 8   `88b.  8   888oooo8    888      888    `888. .8'     888   8 Y88. .P  888 T 8     `88b.8   888    \"    888      888     `888.8'      888   8  `888'   888 T 8       `888   888       o `88b    d88'      `888'       888   8    Y     888 To8o        `8  o888ooooood8  `Y8bood8P'        `8'       o888o o8o        o888o\1\6\0\0006_____   __                     _____             6___  | / /_____ ______ ___   _____(_)_______ ___ 6__   |/ / _  _ \\_  __ \\__ | / /__  / __  __ `__ \\6_  /|  /  /  __// /_/ /__ |/ / _  /  _  / / / / /6/_/ |_/   \\___/ \\____/ _____/  /_/   /_/ /_/ /_/ \1\6\0\0H_|      _|   _|_|_|_|     _|_|     _|      _|   _|_|_|   _|      _|H_|_|    _|   _|         _|    _|   _|      _|     _|     _|_|  _|_|H_|  _|  _|   _|_|_|     _|    _|   _|      _|     _|     _|  _|  _|H_|    _|_|   _|         _|    _|     _|  _|       _|     _|      _|H_|      _|   _|_|_|_|     _|_|         _|       _|_|_|   _|      _|\1\b\0\0S ________     _______     ________    ___      ___   ___    _____ ______      S|\\   ___  \\  |\\  ___ \\   |\\   __  \\  |\\  \\    /  /| |\\  \\  |\\   _ \\  _   \\    S\\ \\  \\\\ \\  \\ \\ \\   __/|  \\ \\  \\|\\  \\ \\ \\  \\  /  / / \\ \\  \\ \\ \\  \\\\\\__\\ \\  \\   S \\ \\  \\\\ \\  \\ \\ \\  \\_|/__ \\ \\  \\\\\\  \\ \\ \\  \\/  / /   \\ \\  \\ \\ \\  \\\\|__| \\  \\  S  \\ \\  \\\\ \\  \\ \\ \\  \\_|\\ \\ \\ \\  \\\\\\  \\ \\ \\    / /     \\ \\  \\ \\ \\  \\    \\ \\  \\ S   \\ \\__\\\\ \\__\\ \\ \\_______\\ \\ \\_______\\ \\ \\__/ /       \\ \\__\\ \\ \\__\\    \\ \\__\\S    \\|__| \\|__|  \\|_______|  \\|_______|  \\|__|/         \\|__|  \\|__|     \\|__|\1\b\0\0F  __   __      _____     _____      _     _     __     __    __  F /_/\\ /\\_\\   /\\_____\\   ) ___ (    /_/\\ /\\_\\   /\\_\\   /_/\\  /\\_\\ F ) ) \\ ( (  ( (_____/  / /\\_/\\ \\   ) ) ) ( (   \\/_/   ) ) \\/ ( ( F/_/   \\ \\_\\  \\ \\__\\   / /_/ (_\\ \\ /_/ / \\ \\_\\   /\\_\\ /_/ \\  / \\_\\F\\ \\ \\   / /  / /__/_  \\ \\ )_/ / / \\ \\ \\_/ / /  / / / \\ \\ \\\\// / /F )_) \\ (_(  ( (_____\\  \\ \\/_\\/ /   \\ \\   / /  ( (_(   )_) )( (_( F \\_\\/ \\/_/   \\/_____/   )_____(     \\_\\_/_/    \\/_/   \\_\\/  \\/_/ \1\14\0\0h _____   ______         ______            _____      ____      ____   ____       ______  _______   h|\\    \\ |\\     \\    ___|\\     \\      ____|\\    \\    |    |    |    | |    |     |      \\/       \\  h \\\\    \\| \\     \\  |     \\     \\    /     /\\    \\   |    |    |    | |    |    /          /\\     \\ h  \\|    \\  \\     | |     ,_____/|  /     /  \\    \\  |    |    |    | |    |   /     /\\   / /\\     |h   |     \\  |    | |     \\--'\\_|/ |     |    |    | |    |    |    | |    |  /     /\\ \\_/ / /    /|h   |      \\ |    | |     /___/|   |     |    |    | |    |    |    | |    | |     |  \\|_|/ /    / |h   |    |\\ \\|    | |     \\____|\\  |\\     \\  /    /| |\\    \\  /    /| |    | |     |       |    |  |h   |____||\\_____/| |____ '     /| | \\_____\\/____/ | | \\ ___\\/___ / | |____| |\\____\\       |____|  /h   |    |/ \\|   || |    /_____/ |  \\ |    ||    | /  \\ |   ||   | /  |    | | |    |      |    | / h   |____|   |___|/ |____|     | /   \\|____||____|/    \\|___||___|/   |____|  \\|____|      |____|/  h     \\(       )/     \\( |_____|/       \\(    )/         \\(    )/       \\(       \\(          )/     h      '       '       '    )/           '    '           '    '         '        '          '      h                           '                                                                       \1\b\0\0Q _____  ___     _______      ______     ___      ___   __      ___      ___ Q(\\\"   \\|\"  \\   /\"     \"|    /    \" \\   |\"  \\    /\"  | |\" \\    |\"  \\    /\"  |S|.\\\\   \\    | (: ______)   // ____  \\   \\   \\  //  /  |â  |    \\   \\  //   |Q|: \\.   \\\\  |  \\/    |    /  /    ) :)   \\\\  \\/. ./   |:  |    /\\\\  \\/.    |Q|.  \\    \\. |  // ___)_  (: (____/ //     \\.    //    |.  |   |: \\.        |Q|    \\    \\ | (:      \"|  \\        /       \\\\   /     /\\  |\\  |.  \\    /:  |Q \\___|\\____\\)  \\_______)   \\\"_____/         \\__/     (__\\_|_) |___|\\__/|___|\1\v\0\0c        /\\ \\     _      /\\ \\           /\\ \\     /\\ \\    _ / /\\        /\\ \\        /\\_\\/\\_\\ _  c       /  \\ \\   /\\_\\   /  \\ \\         /  \\ \\    \\ \\ \\  /_/ / /        \\ \\ \\      / / / / //\\_\\c      / /\\ \\ \\_/ / /  / /\\ \\ \\       / /\\ \\ \\    \\ \\ \\ \\___\\/         /\\ \\_\\    /\\ \\/ \\ \\/ / /c     / / /\\ \\___/ /  / / /\\ \\_\\     / / /\\ \\ \\   / / /  \\ \\ \\        / /\\/_/   /  \\____\\__/ / c    / / /  \\/____/  / /_/_ \\/_/    / / /  \\ \\_\\  \\ \\ \\   \\_\\ \\      / / /     / /\\/________/  c   / / /    / / /  / /____/\\      / / /   / / /   \\ \\ \\  / / /     / / /     / / /\\/_// / /   c  / / /    / / /  / /\\____\\/     / / /   / / /     \\ \\ \\/ / /     / / /     / / /    / / /    c / / /    / / /  / / /______    / / /___/ / /       \\ \\ \\/ /  ___/ / /__   / / /    / / /     c/ / /    / / /  / / /_______\\  / / /____\\/ /         \\ \\  /  /\\__\\/_/___\\  \\/_/    / / /      c\\/_/     \\/_/   \\/__________/  \\/_________/           \\_\\/   \\/_________/          \\/_/       \1\a\0\0E  _  _      ___      ___    __   __    ___    __  __  O         E | \\| |    | __|    / _ \\   \\ \\ / /   |_ _|  |  \\/  |   o       E | .` |    | _|    | (_) |   \\ V /     | |   | |\\/| |  ___      E |_|\\_|    |___|    \\___/    _\\_/_    |___|  |_|__|_|  |o|____  E_|\"\"\"\"\"| _|\"\"\"\"\"| _|\"\"\"\"\"| _|\"\"\"\"\"| _|\"\"\"\"\"| _|\"\"\"\"\"| _|\"\"\"\"\"\"| E\"`-0-0-' \"`-0-0-' \"`-0-0-' \"`-0-0-' \"`-0-0-' \"`-0-0-' \"`-0-0---\\\1\n\0\0M     .-') _     ('-.                      (`-.              _   .-')    M    ( OO ) )  _(  OO)                   _(OO  )_           ( '.( OO )_  M,--./ ,--,'  (,------.  .-'),-----. ,--(_/   ,. \\  ,-.-')   ,--.   ,--.)M|   \\ |  |\\   |  .---' ( OO'  .-.  '\\   \\   /(__/  |  |OO)  |   `.'   | M|    \\|  | )  |  |     /   |  | |  | \\   \\ /   /   |  |  \\  |         | M|  .     |/  (|  '--.  \\_) |  |\\|  |  \\   '   /,   |  |(_/  |  |'.'|  | M|  |\\    |    |  .--'    \\ |  | |  |   \\     /__) ,|  |_.'  |  |   |  | M|  | \\   |    |  `---.    `'  '-'  '    \\   /    (_|  |     |  |   |  | M`--'  `--'    `------'      `-----'      `-'       `--'     `--'   `--' \1\b\0\0C\\\\\\  ///              .-.     wWw    wWw    wW  Ww  \\\\\\    ///C((O)(O))    wWw     c(O_O)c   (O)    (O)    (O)(O)  ((O)  (O))C | \\ ||     (O)_   ,'.---.`,  ( \\    / )     (..)    | \\  / | C ||\\\\||    .' __) / /|_|_|\\ \\  \\ \\  / /       ||     ||\\\\//|| C || \\ |   (  _)   | \\_____/ |  /  \\/  \\      _||_    || \\/ || C ||  ||    `.__)  '. `---' .`  \\ `--' /     (_/\\_)   ||    || C(_/  \\_)            `-...-'     `-..-'              (_/    \\_)\1\t\0\0.    )                                    . ( /(                                    . )\\())    (            )     (       )   .((_)\\    ))\\    (     /((    )\\     (    . _((_)  /((_)   )\\   (_))\\  ((_)    )\\  '.| \\| | (_))    ((_)  _)((_)  (_)  _((_)) .| .` | / -_)  / _ \\  \\ V /   | | | '  \\().|_|\\_| \\___|  \\___/   \\_/    |_| |_|_|_| \1\t\0\0002    )            )             (        *    2 ( /(         ( /(             )\\ )   (  `   2 )\\())  (     )\\())   (   (   (()/(   )\\))(  2((_)\\   )\\   ((_)\\    )\\  )\\   /(_)) ((_)()\\ 2 _((_) ((_)    ((_)  ((_)((_) (_))   (_()((_)2| \\| | | __|  / _ \\  \\ \\ / /  |_ _|  |  \\/  |2| .` | | _|  | (_) |  \\ V /    | |   | |\\/| |2|_|\\_| |___|  \\___/    \\_/    |___|  |_|  |_|\1\b\0\0L  _   _     U _____ u    U  ___ u  __     __                   __  __  L | \\ |\"|    \\| ___\"|/     \\/\"_ \\/  \\ \\   /\"/u       ___      U|' \\/ '|uL<|  \\| |>    |  _|\"       | | | |   \\ \\ / //       |_\"_|     \\| |\\/| |/L |\\  |u    | |___   .-,_| |_| |   /\\ V /_,-.      | |       | |  | |   L |_| \\_|     |_____|   \\_)-\\___/   U  \\_/-(_/     U/| |\\u     |_|  |_| L ||   \\\\,-.  <<   >>        \\\\       //        .-,_|___|_,-. <<,-,,-.  L (_\")  (_/  (__) (__)      (__)     (__)        \\_)-' '-(_/   (./  \\.) \1\5\0\0, _  _  ____  _____  _  _  ____  __  __ ,( \\( )( ___)(  _  )( \\/ )(_  _)(  \\/  ), )  (  )__)  )(_)(  \\  /  _)(_  )    ( ,(_)\\_)(____)(_____)  \\/  (____)(_/\\/\\_)\1\b\0\0X ___   __       ______       ______       __   __      ________      ___ __ __     X/__/\\ /__/\\    /_____/\\     /_____/\\     /_/\\ /_/\\    /_______/\\    /__//_//_/\\    X\\::\\_\\\\  \\ \\   \\::::_\\/_    \\:::_ \\ \\    \\:\\ \\\\ \\ \\   \\__.::._\\/    \\::\\| \\| \\ \\   X \\:. `-\\  \\ \\   \\:\\/___/\\    \\:\\ \\ \\ \\    \\:\\ \\\\ \\ \\     \\::\\ \\      \\:.      \\ \\  X  \\:. _    \\ \\   \\::___\\/_    \\:\\ \\ \\ \\    \\:\\_/.:\\ \\    _\\::\\ \\__    \\:.\\-/\\  \\ \\ X   \\. \\`-\\  \\ \\   \\:\\____/\\    \\:\\_\\ \\ \\    \\ ..::/ /   /__\\::\\__/\\    \\. \\  \\  \\ \\X    \\__\\/ \\__\\/    \\_____\\/     \\_____\\/     \\___/_(    \\________\\/     \\__\\/ \\__\\/\1\6\0\0D __   __     ______     ______     __   __   __     __    __   D/\\ \"-.\\ \\   /\\  ___\\   /\\  __ \\   /\\ \\ / /  /\\ \\   /\\ \"-./  \\  D\\ \\ \\-.  \\  \\ \\  __\\   \\ \\ \\/\\ \\  \\ \\ \\'/   \\ \\ \\  \\ \\ \\-./\\ \\ D \\ \\_\\\\\"\\_\\  \\ \\_____\\  \\ \\_____\\  \\ \\__|    \\ \\_\\  \\ \\_\\ \\ \\_\\D  \\/_/ \\/_/   \\/_____/   \\/_____/   \\/_/      \\/_/   \\/_/  \\/_/\1\a\0\0@ /$$$$$$$   /$$$$$$   /$$$$$$  /$$    /$$ /$$ /$$$$$$/$$$$ @| $$__  $$ /$$__  $$ /$$__  $$|  $$  /$$/| $$| $$_  $$_  $$@| $$  \\ $$| $$$$$$$$| $$  \\ $$ \\  $$/$$/ | $$| $$ \\ $$ \\ $$@| $$  | $$| $$_____/| $$  | $$  \\  $$$/  | $$| $$ | $$ | $$@| $$  | $$|  $$$$$$$|  $$$$$$/   \\  $/   | $$| $$ | $$ | $$@|__/  |__/ \\_______/ \\______/     \\_/    |__/|__/ |__/ |__/\1\f\0\0W     ___           ___           ___                                      ___     W    /  /\\         /  /\\         /  /\\          ___            ___        /  /\\    W   /  /::|       /  /::\\       /  /::\\        /  /\\          /__/\\      /  /::|   W  /  /:|:|      /  /:/\\:\\     /  /:/\\:\\      /  /:/          \\__\\:\\    /  /:|:|   W /  /:/|:|__   /  /::\\ \\:\\   /  /:/  \\:\\    /  /:/           /  /::\\  /  /:/|:|__ W/__/:/ |:| /\\ /__/:/\\:\\ \\:\\ /__/:/ \\__\\:\\  /__/:/  ___    __/  /:/\\/ /__/:/_|::::\\_\\__\\/  |:|/:/ \\  \\:\\ \\:\\_\\/ \\  \\:\\ /  /:/  |  |:| /  /\\  /__/\\/:/â¾â¾  \\__\\/  /â¾â¾/:/W    |  |:/:/   \\  \\:\\ \\:\\    \\  \\:\\  /:/   |  |:|/  /:/  \\  \\::/           /  /:/ W    |__|::/     \\  \\:\\_\\/     \\  \\:\\/:/    |__|:|__/:/    \\  \\:\\          /  /:/  W    /__/:/       \\  \\:\\        \\  \\::/      \\__\\::::/      \\__\\/         /__/:/   _    \\__\\/         \\__\\/         \\__\\/           â¾â¾â¾â¾                     \\__\\/    \1\f\0\0U     ___           ___           ___                                    ___     U    /__/\\         /  /\\         /  /\\          ___        ___          /__/\\    U    \\  \\:\\       /  /:/_       /  /::\\        /__/\\      /  /\\        |  |::\\   U     \\  \\:\\     /  /:/ /\\     /  /:/\\:\\       \\  \\:\\    /  /:/        |  |:|:\\  U _____\\__\\:\\   /  /:/ /:/_   /  /:/  \\:\\       \\  \\:\\  /__/::\\      __|__|:|\\:\\ U/__/::::::::\\ /__/:/ /:/ /\\ /__/:/ \\__\\:\\  ___  \\__\\:\\ \\__\\/\\:\\__  /__/::::| \\:\\a\\  \\:\\â¾â¾\\â¾â¾\\/ \\  \\:\\/:/ /:/ \\  \\:\\ /  /:/ /__/\\ |  |:|    \\  \\:\\/\\ \\  \\:\\â¾â¾\\__\\/[ \\  \\:\\  â¾â¾â¾   \\  \\::/ /:/   \\  \\:\\  /:/  \\  \\:\\|  |:|     \\__\\::/  \\  \\:\\      U  \\  \\:\\        \\  \\:\\/:/     \\  \\:\\/:/    \\  \\:\\__|:|     /__/:/    \\  \\:\\     U   \\  \\:\\        \\  \\::/       \\  \\::/      \\__\\::::/      \\__\\/      \\  \\:\\    ]    \\__\\/         \\__\\/         \\__\\/           â¾â¾â¾â¾                   \\__\\/    \1\f\0\0V     ___           ___           ___                                     ___     V    /\\  \\         /\\__\\         /\\  \\          ___                      /\\  \\    V    \\:\\  \\       /:/ _/_       /::\\  \\        /\\  \\        ___         |::\\  \\   V     \\:\\  \\     /:/ /\\__\\     /:/\\:\\  \\       \\:\\  \\      /\\__\\        |:|:\\  \\  V _____\\:\\  \\   /:/ /:/ _/_   /:/  \\:\\  \\       \\:\\  \\    /:/__/      __|:|\\:\\  \\ V/::::::::\\__\\ /:/_/:/ /\\__\\ /:/__/ \\:\\__\\  ___  \\:\\__\\  /::\\  \\     /::::|_\\:\\__\\b\\:\\â¾â¾\\â¾â¾\\/__/ \\:\\/:/ /:/  / \\:\\  \\ /:/  / /\\  \\ |:|  |  \\/\\:\\  \\__  \\:\\â¾â¾\\  \\/__/Z \\:\\  \\        \\::/_/:/  /   \\:\\  /:/  /  \\:\\  \\|:|  |   â¾â¾\\:\\/\\__\\  \\:\\  \\      V  \\:\\  \\        \\:\\/:/  /     \\:\\/:/  /    \\:\\__|:|__|      \\::/  /   \\:\\  \\     V   \\:\\__\\        \\::/  /       \\::/  /      \\::::/__/       /:/  /     \\:\\__\\    ^    \\/__/         \\/__/         \\/__/        â¾â¾â¾â¾           \\/__/       \\/__/    \1\f\0\0V     ___           ___           ___           ___                       ___     V    /\\__\\         /\\  \\         /\\  \\         /\\__\\          ___        /\\__\\    V   /::|  |       /::\\  \\       /::\\  \\       /:/  /         /\\  \\      /::|  |   V  /:|:|  |      /:/\\:\\  \\     /:/\\:\\  \\     /:/  /          \\:\\  \\    /:|:|  |   X /:/|:|  |__   /::\\â¾\\:\\  \\   /:/  \\:\\  \\   /:/__/  ___      /::\\__\\  /:/|:|__|__ V/:/ |:| /\\__\\ /:/\\:\\ \\:\\__\\ /:/__/ \\:\\__\\  |:|  | /\\__\\  __/:/\\/__/ /:/ |::::\\__\\\\\\/__|:|/:/  / \\:\\â¾\\:\\ \\/__/ \\:\\  \\ /:/  /  |:|  |/:/  / /\\/:/  /    \\/__/â¾â¾/:/  /V    |:/:/  /   \\:\\ \\:\\__\\    \\:\\  /:/  /   |:|__/:/  /  \\::/__/           /:/  / V    |::/  /     \\:\\ \\/__/     \\:\\/:/  /     \\::::/__/    \\:\\__\\          /:/  /  ^    /:/  /       \\:\\__\\        \\::/  /       â¾â¾â¾â¾         \\/__/         /:/  /   V    \\/__/         \\/__/         \\/__/                                   \\/__/    \1\a\0\0006                               __                6  ___     ___    ___   __  __ /\\_\\    ___ ___    6 / _ `\\  / __`\\ / __`\\/\\ \\/\\ \\\\/\\ \\  / __` __`\\  6/\\ \\/\\ \\/\\  __//\\ \\_\\ \\ \\ \\_/ |\\ \\ \\/\\ \\/\\ \\/\\ \\ 6\\ \\_\\ \\_\\ \\____\\ \\____/\\ \\___/  \\ \\_\\ \\_\\ \\_\\ \\_\\6 \\/_/\\/_/\\/____/\\/___/  \\/__/    \\/_/\\/_/\\/_/\\/_/\1\6\0\0000                               _           0   ____   ___   ____  _   _   (_) ____ ___ 0  / __ \\ / _ \\ / __ \\| | / / / / / __ `__ \\0 / / / //  __// /_/ /| |/ / / / / / / / / /0/_/ /_/ \\___/ \\____/ |___/ /_/ /_/ /_/ /_/ \1\6\0\0003                                _             3 _ __     ___    ___   __   __ (_)  _ __ ___  3| '_ \\   / _ \\  / _ \\  \\ \\ / / | | | '_ ` _ \\ 3| | | | |  __/ | (_) |  \\ V /  | | | | | | | |3|_| |_|  \\___|  \\___/    \\_/   |_| |_| |_| |_|\1\a\0\0002 ______                            _         2|  ___ \\                          (_)        2| |   | |   ____    ___    _   _   _   ____  2| |   | |  / _  )  / _ \\  | | | | | | |    \\ 2| |   | | ( (/ /  | |_| |  \\ V /  | | | | | |2|_|   |_|  \\____)  \\___/    \\_/   |_| |_|_|_|\1\4\0\0) _      ____  ___   _      _   _    )| |\\ | | |_  / / \\ \\ \\  / | | | |\\/|)|_| \\| |_|__ \\_\\_/  \\_\\/  |_| |_|  |\1\4\0\0#       _   _         ___      #|\\ |  |_  / \\  \\  /   |   |\\/|#| \\|  |_  \\_/   \\/   _|_  |  |\1\4\0\0/__   _ ______  _____  _    _ _____ _______/| \\  | |_____ |     |  \\  /    |   |  |  |/|  \\_| |_____ |_____|   \\/   __|__ |  |  |\1\4\0\0004ââââ¦ââââââ¦  â¦â¦ââ¦â4ââââ â£ â âââââââââ2âââââââââ ââ â©â© â©\1\4\0\0004ââââ¬ââââââ¬  â¬â¬ââ¬â4âââââ¤ â âââââââââ2âââââââââ ââ â´â´ â´\1\4\0\0004â­â®â­â¬ââ®â­ââ®â¬  â¬â¬â­â¬â®4âââââ¤ â ââ°âââ¯ââââ2â¯â°â¯â°ââ¯â°ââ¯ â°â¯ â´â´ â´\1\a\0\0)0    1    1    0    1    1    1    0)0    1    1    0    0    1    0    1)0    1    1    0    1    1    1    1)0    1    1    1    0    1    1    0)0    1    1    0    1    0    0    1)0    1    1    0    1    1    0    1\1\2\0\0%4E    45    4F    56    49    4D\1\2\0\0+116    105    117    126    111    115\1\2\0\0\24-. . --- ...- .. --\1\5\0\0/â¬â¬â¬â¬â¬â¬â¬â¬â¬â¬â¬ââ   5âââ¦â¦ââ¦ââ¦ââ¦ââ¬â¬âââ5âââââ©â£â¬â ââââ£ââââ5ââ©ââ©ââ©âââââââ©â©â©â\1\5\0\0,âââââââââââââ5âââ³â³ââ³ââ³ââ³ââââââ5âââââ»â«ââ£ââââ«ââââ5ââ»ââ»ââ»âââââââ»â»â»â\1\5\0\0008âââââââ¦âââ¦âââ¦â¦âââ8ââââ£âââ£ââââââ â£âââ8âââââââ£âââ âââ£ââââ8ââââ©âââ©âââââââ©â©â©â\1\5\0\0008âââââââ³âââ³âââ³â³âââ8ââââ«âââ«ââââââ£â«âââ8âââââââ«âââ£âââ«ââââ8ââââ»âââ»âââââââ»â»â»â\1\a\0\0\1ââââ  âââ ââââââââ  ââââââ  âââ   âââ âââ ââââ   ââââ\1âââââ âââ ââââââââ ââââââââ âââ   âââ âââ âââââ âââââ\1âââââââââ ââââââ   âââ  âââ ââââ ââââ âââ âââââââââââ\1âââââââââ ââââââ   âââ  âââ  âââââââ  âââ âââââââââââ\1âââ âââââ ââââââââ ââââââââ   âââââ   âââ âââ âââ âââ|âââ  ââââ ââââââââ  ââââââ     âââ    âââ âââ     âââ\0\27alpha.themes.dashboard\nalpha\frequire\17À\4\0", "config", "alpha-nvim")
time([[Config for alpha-nvim]], false)
-- Config for: refactoring.nvim
time([[Config for refactoring.nvim]], true)
try_loadstring("\27LJ\2\n8\0\0\3\0\2\0\0046\0\0\0'\2\1\0B\0\2\1K\0\1\0\29plugins.plug-refactoring\frequire\0", "config", "refactoring.nvim")
time([[Config for refactoring.nvim]], false)
-- Config for: indent-blankline.nvim
time([[Config for indent-blankline.nvim]], true)
try_loadstring("\27LJ\2\ná\3\0\0\4\0\14\0\0226\0\0\0009\0\1\0+\1\2\0=\1\2\0006\0\0\0009\0\1\0009\0\3\0\18\2\0\0009\0\4\0'\3\5\0B\0\3\0016\0\6\0'\2\a\0B\0\2\0029\0\b\0005\2\t\0005\3\n\0=\3\v\0025\3\f\0=\3\r\2B\0\2\1K\0\1\0\20buftype_exclude\1\4\0\0\rterminal\vnofile\vprompt\21filetype_exclude\1\f\0\0\thelp\bgit\rmarkdown\ttext\rterminal\flspinfo\vpacker\nalpha\20TelescopePrompt\21TelescopeResults\18lsp-installer\1\0\a\tchar\bâ\19use_treesitter\2\31show_current_context_start\2\25show_current_context\2\21show_end_of_line\2#show_trailing_blankline_indent\1\28show_first_indent_level\1\nsetup\21indent_blankline\frequire\feol:â´\vappend\14listchars\tlist\bopt\bvim\0", "config", "indent-blankline.nvim")
time([[Config for indent-blankline.nvim]], false)
-- Config for: barbar.nvim
time([[Config for barbar.nvim]], true)
try_loadstring("\27LJ\2\n¹\3\0\0\4\0\6\0\t6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\3\0005\3\4\0=\3\5\2B\0\2\1K\0\1\0\15exclude_ft\1\2\0\0\15NvimTree_1\1\0\18\16icon_pinned\bï¤\28icon_close_tab_modified\bâ\19icon_close_tab\bï\28icon_separator_inactive\bâ\nicons\2\26icon_separator_active\bâ\23icon_custom_colors\1\14clickable\2\rclosable\2\rtabpages\2\14auto_hide\1\14animation\2\fletters:asdfjkl;ghnmxcvbziowerutyqpASDFJKLGHNMXCVBZIOWERUTYQP\21semantic_letters\2\19maximum_length\3\30\20maximum_padding\3\2\20insert_at_start\1\18insert_at_end\1\nsetup\15bufferline\frequire\0", "config", "barbar.nvim")
time([[Config for barbar.nvim]], false)
-- Config for: telescope.nvim
time([[Config for telescope.nvim]], true)
try_loadstring("\27LJ\2\nï\t\0\0\t\0,\1I6\0\0\0'\2\1\0B\0\2\0029\1\2\0005\3#\0005\4\4\0005\5\3\0=\5\5\0045\5\a\0005\6\6\0=\6\b\0055\6\t\0=\6\n\5=\5\v\0046\5\0\0'\a\f\0B\5\2\0029\5\r\5=\5\14\0045\5\15\0=\5\16\0046\5\0\0'\a\f\0B\5\2\0029\5\17\5=\5\18\0045\5\19\0=\5\20\0044\5\0\0=\5\21\0045\5\22\0=\5\23\0045\5\24\0=\5\25\0046\5\0\0'\a\26\0B\5\2\0029\5\27\0059\5\28\5=\5\29\0046\5\0\0'\a\26\0B\5\2\0029\5\30\0059\5\28\5=\5\31\0046\5\0\0'\a\26\0B\5\2\0029\5 \0059\5\28\5=\5!\0046\5\0\0'\a\26\0B\5\2\0029\5\"\5=\5\"\4=\4$\0035\4'\0004\5\3\0006\6\0\0'\b%\0B\6\2\0029\6&\0064\b\0\0B\6\2\0?\6\0\0=\5(\0045\5)\0=\5*\4=\4+\3B\1\2\1K\0\1\0\15extensions\bfzf\1\0\1\nfuzzy\1\14ui-select\1\0\0\17get_dropdown\21telescope.themes\rdefaults\1\0\0\27buffer_previewer_maker\21qflist_previewer\22vim_buffer_qflist\19grep_previewer\23vim_buffer_vimgrep\19file_previewer\bnew\19vim_buffer_cat\25telescope.previewers\fset_env\1\0\1\14COLORTERM\14truecolor\16borderchars\1\t\0\0\bâ\bâ\bâ\bâ\bâ­\bâ®\bâ¯\bâ°\vborder\17path_display\1\2\0\0\rtruncate\19generic_sorter\29get_generic_fuzzy_sorter\25file_ignore_patterns\1\2\0\0\17node_modules\16file_sorter\19get_fuzzy_file\22telescope.sorters\18layout_config\rvertical\1\0\1\vmirror\1\15horizontal\1\0\3\nwidth\4×ÇÂë\3®¯ÿ\3\vheight\4³æÌ\t³¦ÿ\3\19preview_cutoff\3x\1\0\3\18results_width\4³æÌ\t³¦ÿ\3\18preview_width\4³æÌ\t³ÿ\3\20prompt_position\btop\22vimgrep_arguments\1\0\n\23selection_strategy\nreset\17initial_mode\vinsert\rwinblend\3\0\20selection_caret\a  \19color_devicons\2\18prompt_prefix\v ï  \ruse_less\2\17entry_prefix\a  \20layout_strategy\15horizontal\21sorting_strategy\14ascending\1\b\0\0\arg\18--color=never\17--no-heading\20--with-filename\18--line-number\r--column\17--smart-case\nsetup\14telescope\frequire\3À\4\0", "config", "telescope.nvim")
time([[Config for telescope.nvim]], false)
-- Config for: dracula.nvim
time([[Config for dracula.nvim]], true)
try_loadstring("\27LJ\2\n¡\1\0\0\3\0\a\0\0176\0\0\0009\0\1\0+\1\2\0=\1\2\0006\0\0\0009\0\1\0+\1\1\0=\1\3\0006\0\0\0009\0\4\0'\2\5\0B\0\2\0016\0\0\0009\0\1\0)\1\1\0=\1\6\0K\0\1\0\14rehash256\24colorscheme dracula\bcmd\27dracula_transparent_bg\27dracula_italic_comment\6g\bvim\0", "config", "dracula.nvim")
time([[Config for dracula.nvim]], false)
-- Config for: lsp_signature.nvim
time([[Config for lsp_signature.nvim]], true)
try_loadstring("\27LJ\2\n\2\0\0\3\0\a\0\v5\0\0\0005\1\1\0=\1\2\0007\0\3\0006\0\4\0'\2\5\0B\0\2\0029\0\6\0006\2\3\0B\0\2\1K\0\1\0\nsetup\18lsp_signature\frequire\bcfg\17handler_opts\1\0\1\vborder\vsingle\1\0\f\ffix_pos\2\20floating_window\2\14doc_lines\3\0\16hint_enable\2\fpadding\5\vzindex\3È\1\14max_width\3x\15max_height\3\22\17hi_parameter\vSearch\16hint_scheme\vString\16hint_prefix\tï» \tbind\2\0", "config", "lsp_signature.nvim")
time([[Config for lsp_signature.nvim]], false)
-- Config for: AutoSave.nvim
time([[Config for AutoSave.nvim]], true)
try_loadstring("\27LJ\2\nn\0\0\3\0\4\0\a6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\3\0B\0\2\1K\0\1\0\1\0\2\19debounce_delay\3è\a clean_command_line_interval\3è\a\nsetup\rautosave\frequire\0", "config", "AutoSave.nvim")
time([[Config for AutoSave.nvim]], false)
-- Config for: friendly-snippets
time([[Config for friendly-snippets]], true)
try_loadstring("\27LJ\2\nM\0\0\3\0\3\0\0066\0\0\0'\2\1\0B\0\2\0029\0\2\0B\0\1\1K\0\1\0\14lazy_load luasnip.loaders.from_vscode\frequire\0", "config", "friendly-snippets")
time([[Config for friendly-snippets]], false)
-- Config for: nvim-colorizer.lua
time([[Config for nvim-colorizer.lua]], true)
try_loadstring("\27LJ\2\n7\0\0\3\0\3\0\0066\0\0\0'\2\1\0B\0\2\0029\0\2\0B\0\1\1K\0\1\0\nsetup\14colorizer\frequire\0", "config", "nvim-colorizer.lua")
time([[Config for nvim-colorizer.lua]], false)
-- Config for: telescope-fzf-native.nvim
time([[Config for telescope-fzf-native.nvim]], true)
try_loadstring("\27LJ\2\nH\0\0\3\0\4\0\a6\0\0\0'\2\1\0B\0\2\0029\0\2\0'\2\3\0B\0\2\1K\0\1\0\bfzf\19load_extension\14telescope\frequire\0", "config", "telescope-fzf-native.nvim")
time([[Config for telescope-fzf-native.nvim]], false)
-- Config for: nvim-tree.lua
time([[Config for nvim-tree.lua]], true)
try_loadstring("\27LJ\2\n(\0\1\5\0\3\0\0056\1\0\0009\1\1\1'\3\2\0\18\4\0\0D\1\3\0\6 \brep\vstringZ\0\0\6\1\3\0\n-\0\0\0009\0\0\0)\2)\0006\3\1\0)\5\r\0B\3\2\2'\4\2\0&\3\4\3B\0\3\1K\0\1\0\1À\18File Explorer\20Add_whitespaces\15set_offset(\0\0\3\1\1\0\5-\0\0\0009\0\0\0)\2\0\0B\0\2\1K\0\1\0\1À\15set_offset¿\5\1\0\b\0 \0*6\0\0\0'\2\1\0B\0\2\0026\1\0\0'\3\2\0B\1\2\0026\2\0\0'\4\3\0B\2\2\0029\2\4\0025\4\5\0005\5\6\0=\5\a\0045\5\b\0005\6\t\0=\6\n\5=\5\v\0045\5\f\0=\5\r\0045\5\15\0005\6\14\0=\6\16\5=\5\17\0045\5\18\0=\5\19\0045\5\23\0005\6\21\0005\a\20\0=\a\22\6=\6\24\5=\5\25\4B\2\2\0013\2\26\0007\2\27\0009\2\28\0003\4\29\0B\2\2\0019\2\30\0003\4\31\0B\2\2\0012\0\0K\0\1\0\0\18on_tree_close\0\17on_tree_open\20Add_whitespaces\0\factions\14open_file\1\0\0\18window_picker\1\0\0\1\0\1\venable\1\24update_focused_file\1\0\1\venable\2\rrenderer\19indent_markers\1\0\2\27highlight_opened_files\ball\18highlight_git\2\1\0\1\venable\2\tview\1\0\4\nwidth\3(\21hide_root_folder\2\tside\tleft\18adaptive_size\2\ffilters\vcustom\1\5\0\0\t.git\17node_modules\v.cache\t.bin\1\0\1\rdotfiles\2\16diagnostics\1\0\1\venable\1\1\0\6'hijack_unnamed_buffer_when_opening\2\17hijack_netrw\2\18hijack_cursor\2\20respect_buf_cwd\2\23sync_root_with_cwd\2\18disable_netrw\1\nsetup\14nvim-tree\21bufferline.state\21nvim-tree.events\frequire\0", "config", "nvim-tree.lua")
time([[Config for nvim-tree.lua]], false)
-- Config for: fidget.nvim
time([[Config for fidget.nvim]], true)
try_loadstring("\27LJ\2\n4\0\0\3\0\3\0\0066\0\0\0'\2\1\0B\0\2\0029\0\2\0B\0\1\1K\0\1\0\nsetup\vfidget\frequire\0", "config", "fidget.nvim")
time([[Config for fidget.nvim]], false)
-- Config for: rest.nvim
time([[Config for rest.nvim]], true)
try_loadstring("\27LJ\2\n7\0\0\3\0\3\0\0066\0\0\0'\2\1\0B\0\2\0029\0\2\0B\0\1\1K\0\1\0\nsetup\14rest-nvim\frequire\0", "config", "rest.nvim")
time([[Config for rest.nvim]], false)
-- Config for: telescope-ui-select.nvim
time([[Config for telescope-ui-select.nvim]], true)
try_loadstring("\27LJ\2\nN\0\0\3\0\4\0\a6\0\0\0'\2\1\0B\0\2\0029\0\2\0'\2\3\0B\0\2\1K\0\1\0\14ui-select\19load_extension\14telescope\frequire\0", "config", "telescope-ui-select.nvim")
time([[Config for telescope-ui-select.nvim]], false)
-- Config for: nvim-treesitter
time([[Config for nvim-treesitter]], true)
try_loadstring("\27LJ\2\n§\2\0\0\4\0\f\0\0156\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\4\0005\3\3\0=\3\5\0025\3\6\0=\3\a\0025\3\b\0=\3\t\0025\3\n\0=\3\v\2B\0\2\1K\0\1\0\frainbow\1\0\2\venable\2\18extended_mode\2\14highlight\1\0\2\venable\2\21use_languagetree\2\fautotag\1\0\1\venable\2\21ensure_installed\1\0\0\1\r\0\0\blua\bvim\thtml\15javascript\vpython\tjava\fc_sharp\bcss\tbash\tjson\thttp\15typescript\nsetup\28nvim-treesitter.configs\frequire\0", "config", "nvim-treesitter")
time([[Config for nvim-treesitter]], false)
-- Config for: filetype.nvim
time([[Config for filetype.nvim]], true)
try_loadstring("\27LJ\2\nx\0\0\5\0\b\0\v6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\6\0005\3\4\0005\4\3\0=\4\5\3=\3\a\2B\0\2\1K\0\1\0\14overrides\1\0\0\15extensions\1\0\0\1\0\1\tconf\15Properties\nsetup\rfiletype\frequire\0", "config", "filetype.nvim")
time([[Config for filetype.nvim]], false)
-- Config for: neovim-session-manager
time([[Config for neovim-session-manager]], true)
try_loadstring("\27LJ\2\n¸\3\0\0\n\0\18\0\0286\0\0\0'\2\1\0B\0\2\0026\1\0\0'\3\2\0B\1\2\0029\1\3\0015\3\n\0\18\6\0\0009\4\4\0006\a\5\0009\a\6\a9\a\a\a'\t\b\0B\a\2\2'\b\t\0B\4\4\2=\4\v\0036\4\0\0'\6\f\0B\4\2\0029\4\r\0049\4\14\4=\4\15\0035\4\16\0=\4\17\3B\1\2\1K\0\1\0\30autosave_ignore_filetypes\1\2\0\0\14gitcommit\18autoload_mode\rDisabled\17AutoloadMode\27session_manager.config\17sessions_dir\1\0\6\20max_path_length\3P\29autosave_only_in_session\2\31autosave_ignore_not_normal\2\26autosave_last_session\2\19colon_replacer\a++\18path_replacer\a__\rsessions\tdata\fstdpath\afn\bvim\bnew\nsetup\20session_manager\17plenary.path\frequire\0", "config", "neovim-session-manager")
time([[Config for neovim-session-manager]], false)
-- Config for: nvim-web-devicons
time([[Config for nvim-web-devicons]], true)
try_loadstring("\27LJ\2\n\b\0\0\5\0<\0?6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2:\0005\3\4\0005\4\3\0=\4\5\0035\4\6\0=\4\a\0035\4\b\0=\4\t\0035\4\n\0=\4\v\0035\4\f\0=\4\r\0035\4\14\0=\4\15\0035\4\16\0=\4\17\0035\4\18\0=\4\19\0035\4\20\0=\4\21\0035\4\22\0=\4\23\0035\4\24\0=\4\25\0035\4\26\0=\4\27\0035\4\28\0=\4\29\0035\4\30\0=\4\31\0035\4 \0=\4!\0035\4\"\0=\4#\0035\4$\0=\4%\0035\4&\0=\4'\0035\4(\0=\4)\0035\4*\0=\4+\0035\4,\0=\4-\0035\4.\0=\4/\0035\0040\0=\0041\0035\0042\0=\0043\0035\0044\0=\0045\0035\0046\0=\0047\0035\0048\0=\0049\3=\3;\2B\0\2\1K\0\1\0\roverride\1\0\0\bzip\1\0\2\tname\bzip\ticon\bï\axz\1\0\2\tname\axz\ticon\bï\nwoff2\1\0\2\tname\23WebOpenFontFormat2\ticon\bï±\twoff\1\0\2\tname\22WebOpenFontFormat\ticon\bï±\bvue\1\0\2\tname\bvue\ticon\bïµ\brpm\1\0\2\tname\brpm\ticon\bï\arb\1\0\2\tname\arb\ticon\bî\bttf\1\0\2\tname\17TrueTypeFont\ticon\bï±\ats\1\0\2\tname\ats\ticon\bï¯¤\ttoml\1\0\2\tname\ttoml\ticon\bï\15robots.txt\1\0\2\tname\vrobots\ticon\bï®§\apy\1\0\2\tname\apy\ticon\bî\bpng\1\0\2\tname\bpng\ticon\bï\bout\1\0\2\tname\bout\ticon\bî\bmp4\1\0\2\tname\bmp4\ticon\bï\bmp3\1\0\2\tname\bmp3\ticon\bï¢\blua\1\0\2\tname\blua\ticon\bî \tlock\1\0\2\tname\tlock\ticon\bï ½\akt\1\0\2\tname\akt\ticon\tó±\ajs\1\0\2\tname\ajs\ticon\bï \bjpg\1\0\2\tname\bjpg\ticon\bï\tjpeg\1\0\2\tname\tjpeg\ticon\bï\thtml\1\0\2\tname\thtml\ticon\bï»\15Dockerfile\1\0\2\tname\15Dockerfile\ticon\bï\bdeb\1\0\2\tname\bdeb\ticon\bï\bcss\1\0\2\tname\bcss\ticon\bî\6c\1\0\0\1\0\2\tname\6c\ticon\bî\nsetup\22nvim-web-devicons\frequire\0", "config", "nvim-web-devicons")
time([[Config for nvim-web-devicons]], false)
-- Config for: Comment.nvim
time([[Config for Comment.nvim]], true)
try_loadstring("\27LJ\2\n5\0\0\3\0\3\0\0066\0\0\0'\2\1\0B\0\2\0029\0\2\0B\0\1\1K\0\1\0\nsetup\fComment\frequire\0", "config", "Comment.nvim")
time([[Config for Comment.nvim]], false)
-- Config for: numb.nvim
time([[Config for numb.nvim]], true)
try_loadstring("\27LJ\2\nF\0\0\3\0\4\0\a6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\3\0B\0\2\1K\0\1\0\1\0\1\16number_only\2\nsetup\tnumb\frequire\0", "config", "numb.nvim")
time([[Config for numb.nvim]], false)
-- Config for: text-case.nvim
time([[Config for text-case.nvim]], true)
try_loadstring("\27LJ\2\n\2\0\0\6\0\14\0\0296\0\0\0'\2\1\0B\0\2\0029\0\2\0004\2\0\0B\0\2\0016\0\0\0'\2\3\0B\0\2\0029\0\4\0'\2\1\0B\0\2\0016\0\5\0009\0\6\0009\0\a\0'\2\b\0'\3\t\0'\4\n\0005\5\v\0B\0\5\0016\0\5\0009\0\6\0009\0\a\0'\2\f\0'\3\t\0'\4\n\0005\5\r\0B\0\5\1K\0\1\0\1\0\1\tdesc\14Telescope\6v\1\0\1\tdesc\14Telescope#<cmd>TextCaseOpenTelescope<CR>\bga.\6n\20nvim_set_keymap\bapi\bvim\19load_extension\14telescope\nsetup\rtextcase\frequire\0", "config", "text-case.nvim")
time([[Config for text-case.nvim]], false)
-- Config for: nvim-cmp
time([[Config for nvim-cmp]], true)
try_loadstring("\27LJ\2\nÐ\1\0\0\b\0\b\2!6\0\0\0006\2\1\0009\2\2\0029\2\3\2)\4\0\0B\2\2\0A\0\0\3\b\1\0\0X\2\206\2\1\0009\2\2\0029\2\4\2)\4\0\0\23\5\1\0\18\6\0\0+\a\2\0B\2\5\2:\2\1\2\18\4\2\0009\2\5\2\18\5\1\0\18\6\1\0B\2\4\2\18\4\2\0009\2\6\2'\5\a\0B\2\3\2\n\2\0\0X\2\2+\2\1\0X\3\1+\2\2\0L\2\2\0\a%s\nmatch\bsub\23nvim_buf_get_lines\24nvim_win_get_cursor\bapi\bvim\vunpack\0\2-\0\1\4\1\2\0\5-\1\0\0009\1\0\0019\3\1\0B\1\2\1K\0\1\0\2À\tbody\15lsp_expandÁ\1\0\2\b\0\n\0\0176\2\0\0'\4\1\0B\2\2\0026\3\3\0009\3\4\3'\5\5\0009\6\2\0018\6\6\0029\a\2\1B\3\4\2=\3\2\0015\3\a\0009\4\b\0009\4\t\0048\3\4\3=\3\6\1L\1\2\0\tname\vsource\1\0\3\rnvim_lsp\n[LSP]\vbuffer\n[BUF]\rnvim_lua\n[Lua]\tmenu\n%s %s\vformat\vstring\tkind\26plugins.lspkind-icons\frequireÅ\1\0\1\3\3\5\0\29-\1\0\0009\1\0\1B\1\1\2\15\0\1\0X\2\4-\1\0\0009\1\1\1B\1\1\1X\1\19-\1\1\0009\1\2\1B\1\1\2\15\0\1\0X\2\4-\1\1\0009\1\3\1B\1\1\1X\1\n-\1\2\0B\1\1\2\15\0\1\0X\2\4-\1\0\0009\1\4\1B\1\1\1X\1\2\18\1\0\0B\1\1\1K\0\1\0\0À\2À\4À\rcomplete\19expand_or_jump\23expand_or_jumpable\21select_next_item\fvisible\1\0\1\4\2\4\0\23-\1\0\0009\1\0\1B\1\1\2\15\0\1\0X\2\4-\1\0\0009\1\1\1B\1\1\1X\1\r-\1\1\0009\1\2\1)\3ÿÿB\1\2\2\15\0\1\0X\2\5-\1\1\0009\1\3\1)\3ÿÿB\1\2\1X\1\2\18\1\0\0B\1\1\1K\0\1\0\0À\2À\tjump\rjumpable\21select_prev_item\fvisible\1\0\0\5\0\a\0\v6\0\0\0'\2\1\0B\0\2\0029\1\2\0005\3\4\0005\4\3\0=\4\5\3B\1\2\0019\1\6\0B\1\1\1K\0\1\0\vupdate\bdic\1\0\0\1\0\1\rphp,html$~/.config/nvim/dicts/bootstrap5\nsetup\19cmp_dictionary\frequireú\t\1\0\r\0F\0}6\0\0\0'\2\1\0B\0\2\0026\1\0\0'\3\2\0B\1\2\0026\2\0\0'\4\3\0B\2\2\0026\3\4\0009\3\5\0033\4\6\0009\5\a\0\18\a\5\0009\5\b\5'\b\t\0009\t\n\1B\t\1\0A\5\2\0019\5\v\0005\a\15\0005\b\r\0003\t\f\0=\t\14\b=\b\16\a5\b\18\0003\t\17\0=\t\19\b=\b\20\a5\b\21\0=\b\22\a5\b\25\0009\t\23\0009\t\24\tB\t\1\2=\t\26\b9\t\23\0009\t\27\tB\t\1\2=\t\28\b9\t\23\0009\t\29\t)\vüÿB\t\2\2=\t\30\b9\t\23\0009\t\29\t)\v\4\0B\t\2\2=\t\31\b9\t\23\0009\t \tB\t\1\2=\t!\b9\t\23\0009\t\"\tB\t\1\2=\t#\b9\t\23\0009\t$\t5\v'\0009\f%\0009\f&\f=\f(\vB\t\2\2=\t)\b9\t\23\0003\v*\0005\f+\0B\t\3\2=\t,\b9\t\23\0003\v-\0005\f.\0B\t\3\2=\t/\b=\b\23\a4\b\a\0005\t0\0>\t\1\b5\t1\0>\t\2\b5\t2\0>\t\3\b5\t3\0>\t\4\b5\t4\0>\t\5\b5\t5\0>\t\6\b=\b6\aB\5\2\0019\5\v\0009\0057\5'\a8\0005\b:\0005\t9\0=\t6\bB\5\3\0019\5\v\0009\0057\5'\a;\0005\b>\0004\t\3\0005\n<\0>\n\1\t5\n=\0>\n\2\t=\t6\bB\5\3\0016\5\0\0'\a?\0B\5\2\0029\5\v\0055\a@\0004\b\0\0=\bA\aB\5\2\0013\5B\0007\5C\0009\5D\3'\aE\0B\5\2\0012\0\0K\0\1\0000command! DictBoostrap5 :lua DictBoostrap5()\17nvim_command\18DictBoostrap5\0\bdic\1\0\1\nasync\2\19cmp_dictionary\1\0\0\1\0\2\20keyword_pattern5[^0-9]\\%(-\\?\\d\\+\\%(\\.\\d\\+\\)\\?\\|\\h\\w*\\%(-\\w*\\)*\\)\tname\fcmdline\1\0\1\tname\tpath\6:\1\0\0\1\0\1\tname\vbuffer\6/\fcmdline\fsources\1\0\2\19keyword_length\3\2\tname\15dictionary\1\0\1\tname\tpath\1\0\1\tname\vbuffer\1\0\1\tname\rnvim_lua\1\0\1\tname\rnvim_lsp\1\0\1\tname\fluasnip\f<S-Tab>\1\4\0\0\6i\6s\6c\0\n<Tab>\1\4\0\0\6i\6s\6c\0\t<CR>\rbehavior\1\0\1\vselect\2\fReplace\20ConfirmBehavior\fconfirm\n<C-e>\nclose\14<C-Space>\rcomplete\n<C-f>\n<C-d>\16scroll_docs\n<C-p>\21select_prev_item\n<C-n>\1\0\0\21select_next_item\fmapping\15completion\1\0\2\19keyword_length\3\2\16completeopt\26menu,menuone,noselect\15formatting\vformat\1\0\0\0\fsnippet\1\0\0\vexpand\1\0\0\0\nsetup\20on_confirm_done\17confirm_done\aon\nevent\0\bapi\bvim\fluasnip\"nvim-autopairs.completion.cmp\bcmp\frequire\0", "config", "nvim-cmp")
time([[Config for nvim-cmp]], false)
-- Config for: LuaSnip
time([[Config for LuaSnip]], true)
try_loadstring("\27LJ\2\n?\0\0\3\1\2\0\n-\0\0\0009\0\0\0B\0\1\2\15\0\0\0X\1\4-\0\0\0009\0\1\0)\2\1\0B\0\2\1K\0\1\0\0À\tjump\rjumpable?\0\0\3\1\2\0\n-\0\0\0009\0\0\0B\0\1\2\15\0\0\0X\1\4-\0\0\0009\0\1\0)\2ÿÿB\0\2\1K\0\1\0\0À\tjump\rjumpableã\4\1\0\t\0\31\00096\0\0\0'\2\1\0B\0\2\0026\1\2\0009\1\3\0019\1\4\0016\2\2\0009\2\5\0029\2\6\0029\3\a\0009\3\b\0035\5\t\0B\3\2\0016\3\0\0'\5\n\0B\3\2\0029\3\v\0035\5\r\0005\6\f\0=\6\14\5B\3\2\0016\3\0\0'\5\15\0B\3\2\0029\3\v\0035\5\17\0005\6\16\0=\6\14\5B\3\2\0016\3\0\0'\5\18\0B\3\2\0029\3\v\0035\5\20\0005\6\19\0=\6\14\5B\3\2\1\18\3\2\0'\5\21\0006\6\0\0'\b\22\0B\6\2\0029\6\23\0065\a\24\0B\3\4\1\18\3\1\0005\5\25\0'\6\26\0003\a\27\0B\3\4\1\18\3\1\0005\5\28\0'\6\29\0003\a\30\0B\3\4\0012\0\0K\0\1\0\0\n<C-k>\1\3\0\0\6s\6i\0\n<C-j>\1\3\0\0\6s\6i\1\0\1\tbang\2\23edit_snippet_files\20luasnip.loaders\rSnipEdit\1\0\0\1\2\0\0\23./snippets/angular luasnip.loaders.from_vscode\1\0\0\1\2\0\0\23./snippets/luasnip\29luasnip.loaders.from_lua\npaths\1\0\0\1\2\0\0\24./snippets/snipmate\14lazy_load\"luasnip.loaders.from_snipmate\1\0\1\18update_events\29TextChanged,TextChangedI\nsetup\vconfig\29nvim_create_user_command\bapi\bset\vkeymap\bvim\fluasnip\frequire\0", "config", "LuaSnip")
time([[Config for LuaSnip]], false)
-- Config for: NeoZoom.lua
time([[Config for NeoZoom.lua]], true)
try_loadstring("\27LJ\2\nZ\0\0\3\0\4\0\a6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\3\0B\0\2\1K\0\1\0\1\0\2\17height_ratio\3\1\16width_ratio\3\1\nsetup\rneo-zoom\frequire\0", "config", "NeoZoom.lua")
time([[Config for NeoZoom.lua]], false)
-- Config for: lualine.nvim
time([[Config for lualine.nvim]], true)
try_loadstring("\27LJ\2\nÚ\5\0\0\b\0%\00086\0\0\0'\2\1\0B\0\2\0026\1\0\0'\3\2\0B\1\2\0029\1\3\0015\3\v\0005\4\4\0005\5\5\0=\5\6\0045\5\a\0=\5\b\0045\5\t\0=\5\n\4=\4\f\0035\4\14\0005\5\r\0=\5\15\0045\5\16\0=\5\17\0045\5\18\0005\6\21\0009\a\19\0>\a\1\0069\a\20\0=\a\22\6>\6\4\5=\5\23\0045\5\24\0=\5\25\0045\5\26\0=\5\27\0045\5\28\0=\5\29\4=\4\30\0035\4\31\0004\5\0\0=\5\15\0044\5\0\0=\5\17\0045\5 \0=\5\23\0045\5!\0=\5\25\0044\5\0\0=\5\27\0044\5\0\0=\5\29\4=\4\"\0034\4\0\0=\4#\0034\4\0\0=\4$\3B\1\2\1K\0\1\0\15extensions\ftabline\22inactive_sections\1\2\0\0\rlocation\1\2\0\0\rfilename\1\0\0\rsections\14lualine_z\1\2\0\0\rlocation\14lualine_y\1\2\0\0\rprogress\14lualine_x\1\4\0\0\rencoding\15fileformat\rfiletype\14lualine_c\tcond\1\0\0\17is_available\17get_location\1\4\0\0\rfilename\tdiff\16diagnostics\14lualine_b\1\2\0\0\vbranch\14lualine_a\1\0\0\1\2\0\0\tmode\foptions\1\0\0\23disabled_filetypes\1\3\0\0\vpacker\rNvimTree\23section_separators\1\0\2\tleft\bî°\nright\bî²\25component_separators\1\0\2\tleft\bî±\nright\bî³\1\0\3\ntheme\17dracula-nvim\18icons_enabled\2\17globalstatus\2\nsetup\flualine\15nvim-navic\frequire\0", "config", "lualine.nvim")
time([[Config for lualine.nvim]], false)
if should_profile then save_profiles() end

end)

if not no_errors then
  error_msg = error_msg:gsub('"', '\\"')
  vim.api.nvim_command('echohl ErrorMsg | echom "Error in packer_compiled: '..error_msg..'" | echom "Please check your config for correctness" | echohl None')
end
