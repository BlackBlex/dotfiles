-----------------------------------------------------------
-- Plugin manager configuration file
-----------------------------------------------------------
-- Plugin manager: packer.nvim
-- https://github.com/wbthomason/packer.nvim
-- For information about installed plugins see the README
--- neovim-lua/README.md
--- https://github.com/brainfucksec/neovim-lua#readme
-- Automatically install packer
local fn = vim.fn
local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
local packer_bootstrap = nil

if fn.empty(fn.glob(install_path)) > 0 then
    packer_bootstrap = fn.system({
        'git', 'clone', '--depth', '1',
        'https://github.com/wbthomason/packer.nvim', install_path
    })
end

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, 'packer')
if not status_ok then print('error packer') return end

-- Install plugins
return packer.startup(function(use)
    use {'wbthomason/packer.nvim'} -- packer can manage itself

    use {'nvim-lua/plenary.nvim'}

    -- file explorer
    use {
        'kyazdani42/nvim-tree.lua',
        requires = {
            'kyazdani42/nvim-web-devicons' -- optional, for file icons
        },
        config = require('plugins.plug-nvim-tree')
    }

    -- indent line
    use {
        'lukas-reineke/indent-blankline.nvim',
        config = require('plugins.indent-blankline')
    }

    -- AutoSave
    use {'Pocco81/AutoSave.nvim', config = require('plugins.autosave')}

    -- autopair
    use {
        'windwp/nvim-ts-autotag',
        requires = {
            'nvim-treesitter/nvim-treesitter' -- For treesitter setup
        }
    }
    use {
        'windwp/nvim-autopairs',
        config = function() require('nvim-autopairs').setup() end
    }

    -- icons
    use {
        'kyazdani42/nvim-web-devicons',
        config = require('plugins.nvim-web-devicons')
    }

    -- treesitter interface
    use {
        'nvim-treesitter/nvim-treesitter',
        run = function()
            require('nvim-treesitter.install').update({with_sync = true})
        end,
        config = require('plugins.nvim-treesitter')
    }

    -- colorschemes
    use {'Mofiqul/dracula.nvim', config = require('plugins.dracula')}

    -- Text case UPPER lower CamelCase etc
    use {'johmsalas/text-case.nvim', config = require('plugins.plug-textcase')}

    -- EasyMotion
    use {
        'phaazon/hop.nvim',
        branch = 'v2', -- optional but strongly recommended
        config = function()
            -- you can configure Hop the way you like here; see :h hop-config
            require'hop'.setup {keys = 'etovxqpdygfblzhckisuran'}
        end
    }

    -- LANGUAGES SERVER
    -- LSP
    use {'neovim/nvim-lspconfig'}

    use {
        'williamboman/nvim-lsp-installer',
        config = require('plugins.lsp-installer')
    }

    -- autocomplete
    use {
        'hrsh7th/nvim-cmp',
        requires = {
            'neovim/nvim-lspconfig', 'hrsh7th/cmp-nvim-lsp', 'hrsh7th/cmp-path',
            'hrsh7th/cmp-buffer', 'hrsh7th/cmp-nvim-lua', 'hrsh7th/cmp-cmdline',
            'uga-rosa/cmp-dictionary'
        },
        config = require('plugins.nvim-cmp')
    }

    -- Snippets
    use {
		'L3MON4D3/LuaSnip',
		config = require('plugins.plug-luasnip'),
	}

	use {
		'saadparwaiz1/cmp_luasnip',
		requires = {
			'L3MON4D3/LuaSnip',
			'hrsh7th/nvim-cmp',
		}
	}

    use { 'rafamadriz/friendly-snippets',
        requires = { 'L3MON4D3/LuaSnip' },
        config = function() require('luasnip.loaders.from_vscode').lazy_load() end
    }

    use {'ray-x/lsp_signature.nvim', config = require('plugins.lsp-signature')}


    -- LANGUAGES SERVER

    -- statusline
    use {
        'nvim-lualine/lualine.nvim',
        requires = {'kyazdani42/nvim-web-devicons', opt = true},
        config = require('plugins.lualine')
    }

    use {
        'SmiteshP/nvim-navic', -- config into lsp-installer
        requires = 'neovim/nvim-lspconfig'
    }

    -- dashboard
    use {
        'goolord/alpha-nvim',
        requires = {'kyazdani42/nvim-web-devicons'},
        config = require('plugins.alpha-nvim')
    }

    -- Tabs buffers
    -- use {
    --     'akinsho/bufferline.nvim',
    --     tag = 'v2.*',
    --     requires = 'kyazdani42/nvim-web-devicons',
    --     config = require('plugins.bufferline')
    -- }
    use {
        'romgrk/barbar.nvim',
        requires = {'kyazdani42/nvim-web-devicons'},
        config = require('plugins.plug-barbar')
    }

    -- Telescope
    use {
        'nvim-telescope/telescope.nvim',
        requires = 'nvim-lua/plenary.nvim',
        config = require('plugins.nvim-telescope')
    }
    use {'nvim-telescope/telescope-fzf-native.nvim', run = 'make',
        requires = 'nvim-lua/telescope.nvim',
        config = function() require('telescope').load_extension('fzf') end }
    use {'nvim-telescope/telescope-ui-select.nvim',
        requires = 'nvim-lua/telescope.nvim',
        config = function() require('telescope').load_extension('ui-select') end }

    -- Sessions
    -- use {
	-- 	'jedrzejboczar/possession.nvim',
	-- 	requires = { 'nvim-lua/plenary.nvim' },
	-- 	config = require('plugins.plug-possession')
	-- }
    use { 'Shatur/neovim-session-manager', config = require('plugins.nvim-session-manager') }

    -- Comments mapping
    use {
        'numToStr/Comment.nvim',
        config = function() require('Comment').setup() end
    }

    -- Filetypes
    use {'nathom/filetype.nvim', config = require('plugins.filetype')}

    -- EditorConfig
    use {'gpanders/editorconfig.nvim'}

    -- Rainbow brackets
    use {'p00f/nvim-ts-rainbow'}

    -- Color highlight
    use {
        'norcalli/nvim-colorizer.lua',
        config = function() require('colorizer').setup() end
    }

    -- BufferTricks
    use {'famiu/bufdelete.nvim'}

    -- Peeklines
    use {
        'nacro90/numb.nvim',
        config = function() require('numb').setup {number_only = true} end
    }

    -- Rest client
    use {
        'NTBBloodbath/rest.nvim',
        requires = {'nvim-lua/plenary.nvim'},
        config = function() require('rest-nvim').setup() end
    }

    -- Progress status
    use {'j-hui/fidget.nvim', config = function() require('fidget').setup() end}

    -- Useful commands
    use {'tpope/vim-eunuch'}

    -- Unicode helper
    use {'chrisbra/unicode.vim'}

    -- Replace helper
    use {'tpope/vim-surround'}

    -- Refactor code
    use {
        'ThePrimeagen/refactoring.nvim',
        requires = {
            {'nvim-lua/plenary.nvim'}, {'nvim-treesitter/nvim-treesitter'}
        },
        config = function() require('plugins.plug-refactoring') end
    }

    use {'mg979/vim-visual-multi'}

    -- Align in | with :Tabularize /|
	use {'godlygeek/tabular'}

    -- Git
    use {'sindrets/diffview.nvim', requires = 'nvim-lua/plenary.nvim'} -- Diffviewer
    use {'TimUntersberger/neogit', requires = 'nvim-lua/plenary.nvim'} -- Text GUI git
    use {'APZelos/blamer.nvim'} -- Message commit on line
    use {'rhysd/git-messenger.vim'} -- Message commit popup
    use {'lewis6991/gitsigns.nvim'} -- git labels

    -- Zoom file
    use { 'nyngwang/NeoZoom.lua',
		config = require('plugins.plug-neozoom')
	}

    if packer_bootstrap then packer.sync() end
end)
