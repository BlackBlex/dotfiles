return function ()
	vim.g.dracula_italic_comment = true
    vim.g.dracula_transparent_bg = false
	vim.cmd[[colorscheme dracula]]
	vim.g.rehash256 = 1
end
