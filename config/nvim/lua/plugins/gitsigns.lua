-----------------------------------------------------------
-- Gitsigns configuration file
-----------------------------------------------------------
-- Plugin: gitsigns.nvim
-- https://github.com/lewis6991/gitsigns.nvim
require('gitsigns').setup {
    trouble = true,
    signs = {
        add = {hl = 'DiffAdd', text = '│', numhl = 'GitSignsAddNr'},
        change = {hl = 'DiffChange', text = '│', numhl = 'GitSignsChangeNr'},
        delete = {hl = 'DiffDelete', text = '', numhl = 'GitSignsDeleteNr'},
        topdelete = {
            hl = 'DiffDelete',
            text = '‾',
            numhl = 'GitSignsDeleteNr'
        },
        changedelete = {
            hl = 'DiffChangeDelete',
            text = '~',
            numhl = 'GitSignsChangeNr'
        }
    }
}
