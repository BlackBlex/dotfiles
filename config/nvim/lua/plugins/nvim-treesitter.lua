-----------------------------------------------------------
-- Treesitter configuration file
-----------------------------------------------------------
-- Plugin: nvim-treesitter
-- https://github.com/nvim-treesitter/nvim-treesitter

return function ()
	require('nvim-treesitter.configs').setup {
		ensure_installed = {
			'lua', 'vim', 'html', 'javascript', 'python', 'java', 'c_sharp', 'css',
			'bash', 'json', 'http', 'typescript'
		},
		autotag = {enable = true},
		highlight = { enable = true, use_languagetree = true },
		rainbow = {
			enable = true,
			-- disable = { 'jsx', 'cpp' }, list of languages you want to disable the plugin for
			extended_mode = true, -- Also highlight non-bracket delimiters like html tags, boolean or table: lang -> boolean
			max_file_lines = nil, -- Do not enable for files with more than n lines, int
			-- colors = {}, -- table of hex strings
			-- termcolors = {} -- table of colour name strings
		}
	}
end
