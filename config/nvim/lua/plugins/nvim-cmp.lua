-----------------------------------------------------------
-- Autocomplete configuration file
-----------------------------------------------------------
-- Plugin: nvim-cmp
-- https://github.com/hrsh7th/nvim-cmp

return function()
	-- require('cmp_nvim_ultisnips').setup {}

	local cmp = require ('cmp')
	local cmp_autopairs = require('nvim-autopairs.completion.cmp')
	local luasnip = require('luasnip')
	local api = vim.api

    local has_words_before = function()
        local line, col = unpack(vim.api.nvim_win_get_cursor(0))
        return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match('%s') == nil
    end

	cmp.event:on(
		'confirm_done',
		cmp_autopairs.on_confirm_done()
	)

	cmp.setup {
		-- load snippet support
		snippet = {
			expand = function(args)
				-- vim.fn['UltiSnips#Anon'](args.body) -- For `ultisnips` users.
                luasnip.lsp_expand(args.body)
			end
		},
		formatting = {
			format = function(entry, vim_item)
				local icons = require 'plugins.lspkind-icons'
				vim_item.kind = string.format('%s %s', icons[vim_item.kind],
					vim_item.kind)

				vim_item.menu = ({
					nvim_lsp = '[LSP]',
					nvim_lua = '[Lua]',
					buffer = '[BUF]'
				})[entry.source.name]

				return vim_item
			end
		},
		-- completion settings
		completion = {
			completeopt = 'menu,menuone,noselect',
			keyword_length = 2,
		},

		-- key mapping
		mapping = {
			['<C-n>'] = cmp.mapping.select_next_item(),
			['<C-p>'] = cmp.mapping.select_prev_item(),
			['<C-d>'] = cmp.mapping.scroll_docs(-4),
			['<C-f>'] = cmp.mapping.scroll_docs(4),
			['<C-Space>'] = cmp.mapping.complete(),
			['<C-e>'] = cmp.mapping.close(),
			['<CR>'] = cmp.mapping.confirm {
				behavior = cmp.ConfirmBehavior.Replace,
				select = true
			},

			-- Tab mapping
			['<Tab>'] = cmp.mapping(function(fallback)
				if cmp.visible() then
					cmp.select_next_item()
				elseif luasnip.expand_or_jumpable() then
					luasnip.expand_or_jump()
				elseif has_words_before() then
				    cmp.complete()
                else
					fallback()
				end
			end, { 'i', 's', 'c' }),
			['<S-Tab>'] = cmp.mapping(function(fallback)
				if cmp.visible() then
					cmp.select_prev_item()
				elseif luasnip.jumpable(-1) then
                    luasnip.jump(-1)
				else
					fallback()
				end
			end, { 'i', 's', 'c' })
		},

		-- load sources, see: https://github.com/topics/nvim-cmp
		sources = {
			{ name = 'luasnip' }, { name = 'nvim_lsp' }, { name = 'nvim_lua' },
			{ name = 'buffer' }, { name = 'path' },
			{ name = 'dictionary', keyword_length = 2}
		}
	}

	-- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
	cmp.setup.cmdline('/', { sources = { name = 'buffer' } })

	-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
	cmp.setup.cmdline(':', { sources = { { name = 'path' }, { name = 'cmdline', keyword_pattern = [[[^0-9]\%(-\?\d\+\%(\.\d\+\)\?\|\h\w*\%(-\w*\)*\)]] }}})

	-- Custom dictionaries
	require('cmp_dictionary').setup({
		dic = {},
		async = true,
	})

	function DictBoostrap5()
		local dic = require('cmp_dictionary')
		-- Download boostrap and generate classes:
		-- curl -s https://stackpath.bootstrapcdn.com/bootstrap@5.1.3/css/bootstrap.css | egrep '{' | egrep -o '\.[a-z0-9:-]+' | sed 's/\.//g' | sort -u > ~/.config/nvim/dicts/bootstrap5
		dic.setup({ dic = {
			['php,html'] = '~/.config/nvim/dicts/bootstrap5'
		} })
		dic.update()
	end

	api.nvim_command('command! DictBoostrap5 :lua DictBoostrap5()')
end
