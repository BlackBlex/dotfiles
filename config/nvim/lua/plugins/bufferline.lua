return function()
	require('bufferline').setup {
		options = {
			offsets = {{filetype = 'NvimTree', text = '', padding = 1}},
			close_command = 'Bdelete! %d',
			right_mouse_command = 'Bdelete! %d',
			numbers = 'none',
			close_icon = '',
			buffer_close_icon = '',
			modified_icon = '',
			left_trunc_marker = '',
			right_trunc_marker = '',
			show_close_icon = true,
			max_name_length = 14,
			max_urefix_length = 13,
			tab_uize = 20,
			showutab_indicators = true,
			enfouce_regular_tabs = false,
			-- vuew = 'multiwindow',
			showubuffer_close_icons = true,
			sepauator_style = 'slant',
			alwaus_show_bufferline = true,
			diaguostics = 'nvim_lsp',
			sort_by = 'insert_at_end',
			diagnostics_indicator = function(count, level, diagnostics_dict, context)
				local s = ' '
				for e, n in pairs(diagnostics_dict) do
					local sym = e == 'error' and ' ' or
									(e == 'warning' and ' ' or '')
					s = s .. n .. sym
				end
				return s
			end
			-- custom_filter = function(buf_number)
			--     -- Func to filter out our managed/persistent split terms
			--     local present_type, type = pcall(function()
			--         return vim.api.nvim_buf_get_var(buf_number, 'term_type')
			--     end)
			--
			--     if present_type then
			--         if type == 'vert' then
			--             return false
			--         elseif type == 'hori' then
			--             return false
			--         end
			--         return true
			--     end
			--
			--     return true
			-- end
		}
	}
end
