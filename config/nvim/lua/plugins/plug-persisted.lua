return function()
    vim.o.sessionoptions = 'buffers,curdir,folds'

    require('persisted').setup({
        before_save = function()
			vim.api.nvim_input('<ESC>:NvimTreeClose<CR>')
		end,
        before_source = function()
            vim.api.nvim_input('<ESC>:%bd<CR>')
        end,
        after_source = function(session)
            print('Loaded session ' .. session.name)
        end,
		ignored_dirs = {
			"~/"
		},
		use_git_branch = false
    })

    require('telescope').load_extension('persisted')
end
