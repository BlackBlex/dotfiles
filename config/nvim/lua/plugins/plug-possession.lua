return function()
    vim.o.sessionoptions = 'buffers,curdir,folds'

	require('possession').setup{
		autosave = {
			current = true
		},
		hooks = {
			before_save = function(name)
                vim.cmd('tabdo NvimTreeClose')
                return {}
            end,
			after_save = function(name, user_data, aborted) end,
			before_load = function(name, user_data) return user_data end,
			after_load = function(name, user_data)
                vim.cmd('tabdo NvimTreeRefresh')
            end,
		},
		plugins = {
			nvim_tree = true,
			delete_hidden_buffers = false
		}
	}

	require('telescope').load_extension('possession')
end
