return function()
  local l = require('luasnip')
  local map = vim.keymap.set
  local cmd = vim.api.nvim_create_user_command

  -- configs
  l.config.setup({
    update_events = 'TextChanged,TextChangedI', -- change on every keystroke
  })

  -- loaders
  -- https://github.com/L3MON4D3/LuaSnip/blob/master/DOC.md#loaders
  require('luasnip.loaders.from_snipmate').lazy_load({ paths = { './snippets/snipmate' } })
  -- https://github.com/L3MON4D3/LuaSnip/blob/master/DOC.md#lua
  require('luasnip.loaders.from_lua').lazy_load({ paths = { './snippets/luasnip' } })

  -- Angular
  require("luasnip.loaders.from_vscode").lazy_load({ paths = { './snippets/angular' } })

  -- commands
  cmd('SnipEdit', require('luasnip.loaders').edit_snippet_files, { bang = true })

  -- jump on snippet positions
  map({ 's', 'i' }, '<C-j>', function()
    if l.jumpable() then
      l.jump(1)
    end
  end)

  map({ 's', 'i' }, '<C-k>', function()
    if l.jumpable() then
      l.jump(-1)
    end
  end)
end
