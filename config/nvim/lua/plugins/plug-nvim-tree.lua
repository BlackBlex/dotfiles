-----------------------------------------------------------
-- File manager configuration file
-----------------------------------------------------------
-- Plugin: nvim-tree
-- https://github.com/kyazdani42/nvim-tree.lua
-- Keybindings are defined in `keymapping.lua`:
--- https://github.com/kyazdani42/nvim-tree.lua#keybindings
-- Note: options under the g: command should be set BEFORE running the
--- setup function:
--- https://github.com/kyazdani42/nvim-tree.lua#setup
--- See: `help NvimTree`

return function ()
    local nvim_tree_events = require('nvim-tree.events')
    local bufferline_state = require('bufferline.state')

    require('nvim-tree').setup {
		-- open_on_setup = true,
		-- open_on_tab = false,
        disable_netrw = false,
		sync_root_with_cwd = true,
		respect_buf_cwd = true,
		hijack_cursor = true,
        hijack_netrw = true,
  		hijack_unnamed_buffer_when_opening = true,
		diagnostics = {
			enable = false,
			-- show_on_dirs = true
			-- icons = {hint = '', info = '', warning = '', error = ''}
		},
		filters = {
			dotfiles = true,
			custom = {'.git', 'node_modules', '.cache', '.bin'}
		},
		-- git = {enable = true, ignore = true},
		view = {width = 40, adaptive_size = true, side = 'left', hide_root_folder = true},
		renderer = {
			indent_markers = {enable = true},
			highlight_git = true,
			highlight_opened_files = 'all',
			-- icons = {
			--     webdev_colors = true,
			--     git_placement = 'before',
			--     padding = ' ',
			--     symlink_arrow = ' ➛ ',
			--     show = {file = true, folder = true, folder_arrow = true, git = true},
			--     glyphs = {
			--         default = '',
			--         symlink = '',
			--         git = {
			--             unstaged = '✗',
			--             staged = '✓',
			--             unmerged = '',
			--             renamed = '➜',
			--             untracked = '★',
			--             deleted = '',
			--             ignored = '◌'
			--         },
			--         folder = {
			--             arrow_closed = '',
			--             arrow_open = '',
			--             default = '',
			--             open = '',
			--             empty = '',
			--             empty_open = '',
			--             symlink = '',
			--             symlink_open = ''
			--         }
			--     }
			-- }
		},
		update_focused_file = {enable = true
		-- , update_cwd = false -- Ya no va
		},
		actions = {
			open_file = {
				-- quit_on_open = false,
				-- resize_window = true,
				window_picker = {enable = false}
			}
		}
	}

    Add_whitespaces = function(number)
        return string.rep(' ', number)
    end

    nvim_tree_events.on_tree_open(function ()
        bufferline_state.set_offset(41, Add_whitespaces(13) .. 'File Explorer')
    end)

    nvim_tree_events.on_tree_close(function ()
        bufferline_state.set_offset(0)
    end)
end
