return function()
	local lsp_installer = require('nvim-lsp-installer')
	local navic = require('nvim-navic')

	local on_attach = function(client, bufnr)
		local function map(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
		local function set(...) vim.api.nvim_buf_set_option(bufnr, ...) end
		set('omnifunc', 'v:lua.vim.lsp.omnifunc')

		local opts = {noremap = true, silent = false}

		map('n', '<localleader>ld', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
		map('n', '<localleader>lh', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
		map('n', '<localleader>ls', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
		map('n', '<localleader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
		map('n', '<localleader>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)

		if client.server_capabilities.documentSymbolProvider then
			navic.attach(client, bufnr)
		end
	end

	lsp_installer.on_server_ready(function(server)
		local opts = {
			on_attach = on_attach,
			settings = {
				Lua = {
					diagnostics = {
						globals = { 'vim' } -- Get the language server to recognize the 'vim' global
					}
				}
			}
		}
		server:setup(opts)
	end)

	lsp_installer.settings({
		ui = {
			icons = {
				server_installed = '✓',
				server_pending = '➜',
				server_uninstalled = '✗'
			}
		},
	})
end
