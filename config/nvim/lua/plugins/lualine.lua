return function()
	local navic = require('nvim-navic')

	require('lualine').setup {
		options = {
			icons_enabled = true,
			theme = 'dracula-nvim',
			component_separators = { left = '', right = ''},
			section_separators = { left = '', right = ''},
			disabled_filetypes = {'packer', 'NvimTree'},
			globalstatus = true
		},
		sections = {
			lualine_a = {'mode'},
			lualine_b = {'branch'},
			lualine_c = {'filename', 'diff', 'diagnostics', {navic.get_location, cond = navic.is_available}},
			lualine_x = {'encoding', 'fileformat', 'filetype'},
			lualine_y = {'progress'},
			lualine_z = {'location'}
		}, --progress
		inactive_sections = {
			lualine_a = {},
			lualine_b = {},
			lualine_c = {'filename'},
			lualine_x = {'location'},
			lualine_y = {},
			lualine_z = {}
		},
		tabline = {},
		extensions = {}
	}
end
