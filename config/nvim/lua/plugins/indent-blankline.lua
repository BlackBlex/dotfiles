-----------------------------------------------------------
-- Indent line configuration file
-----------------------------------------------------------
-- Plugin: indent-blankline
-- https://github.com/lukas-reineke/indent-blankline.nvim

return function ()
	vim.opt.list = true
	vim.opt.listchars:append('eol:↴')

	require('indent_blankline').setup {
		char = '▏',
		show_first_indent_level = false,
		show_trailing_blankline_indent = false,
		show_end_of_line = true,
		show_current_context = true,
		show_current_context_start = true,
		use_treesitter = true,
		filetype_exclude = {
			'help', 'git', 'markdown', 'text', 'terminal', 'lspinfo', 'packer',
			'alpha', 'TelescopePrompt', 'TelescopeResults', 'lsp-installer'
		},
		buftype_exclude = {'terminal', 'nofile', 'prompt'}
	}
end
