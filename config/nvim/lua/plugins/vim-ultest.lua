vim.cmd('let test#strategy = \'neovim\'')
vim.cmd('let test#python#pytest#options = \'--color=yes\'')
vim.cmd('let g:ultest_use_pty = 1')
