local map = vim.api.nvim_set_keymap
local default_opts = {noremap = true, silent = true}
local NOREF_NOERR_TRUNC = { silent = true, nowait = true }

-----------------------------------------------------------
-- Neovim shortcuts:
-----------------------------------------------------------
 -- Write file
map('n', '<leader>w', ':w<CR>', default_opts)
 -- Close file
map('n', '<leader>q', ':BufferClose<CR>', default_opts)

-- Clear search highlighting
map('n', '<leader>c', ':nohl<CR>', default_opts)
map('n', '<leader><Esc>', ':let @/=\'\'<cr>', default_opts)

-- Esc to kk
map('i', 'kk', '<Esc>', default_opts)

-- Move around splits using Ctrl + {h,j,k,l}
map('n', '<C-h>', '<C-w>h', default_opts)
map('n', '<C-j>', '<C-w>j', default_opts)
map('n', '<C-k>', '<C-w>k', default_opts)
map('n', '<C-l>', '<C-w>l', default_opts)

-- Close all windows and exit from neovim
map('n', '<leader>qa', ':qa!<CR>', default_opts)

-----------------------------------------------------------
-- Applications & Plugins shortcuts:
-----------------------------------------------------------
-- open terminal
map('n', '<leader>ot', ':Term<CR>', default_opts)

-- nvim-tree
map('n', '<leader>nt', ':NvimTreeToggle<CR>', default_opts) -- open/close
map('n', '<leader>nr', ':NvimTreeRefresh<CR>', default_opts) -- refresh
map('n', '<leader>nf', ':NvimTreeFindFile<CR>', default_opts) -- search file

-- Telescope
map('n', '<leader>tf', ':Telescope find_files<CR>', default_opts)
map('n', '<leader>tl', ':Telescope live_grep<CR>', default_opts)
map('n', '<leader>tb', ':Telescope buffers<CR>', default_opts)
map('n', '<leader>th', ':Telescope help_tags<CR>', default_opts)
map('n', '<leader>tt', ':Telescope treesitter<CR>', default_opts)
map('n', '<leader>tr', ':Telescope refactoring refactors<CR>', default_opts)
map('n', '<leader>ts', ':SessionManager load_session/<CR>', default_opts)
map('n', '<localleader>tr', ':Telescope lsp_references<CR>', default_opts)
map('n', '<localleader>tsd', ':Telescope lsp_document_symbols<CR>', default_opts)
map('n', '<localleader>tsw', ':Telescope lsp_workspace_symbols<CR>', default_opts)
map('n', '<localleader>tsdw', ':Telescope lsp_dynamic_workspace_symbols<CR>', default_opts)
map('n', '<localleader>ta', ':Telescope lsp_code_actions<CR>', default_opts)
map('n', '<localleader>tar', ':Telescope lsp_range_code_actions<CR>', default_opts)
map('n', '<localleader>to', ':Telescope diagnostics bufnr=0<CR>', default_opts)
map('n', '<localleader>ti', ':Telescope lsp_implementations<CR>', default_opts)
map('n', '<localleader>td', ':Telescope lsp_definitions<CR>', default_opts)
map('n', '<localleader>tt', ':Telescope lsp_type_definitions<CR>', default_opts)

-- UltiSnippets
-- map('n', '<leader>u', ':UltiSnipsEdit<CR>', default_opts)

-- bufferline
map('n', '<TAB>', ':BufferNext<CR>', default_opts)
map('n', '<S-TAB>', ':BufferPrevious<CR>', default_opts)
map('n', '<leader>bmn', ':BufferMoveNext<CR>', default_opts)
map('n', '<leader>bmp', ':BufferMovePrevious<CR>', default_opts)
map('n', '<leader>bsl', ':BufferOrderByLanguage<CR>', default_opts)
map('n', '<leader>bsd', ':BufferOrderByDirectory<CR>', default_opts)
map('n', '<leader>bcp', ':BufferPin<CR>', default_opts)
map('n', '<leader>bp', ':BufferPick<CR>', default_opts)

-- New buffer
map('n', '<leader>bn', ':enew<CR>', default_opts)

-- Rest client
map('n', '<localleader>r', '<Plug>RestNvim', default_opts)
map('n', '<localleader>rp', '<Plug>RestNvimPreview', default_opts)
map('n', '<localleader>rl', '<Plug>RestNvimLast', default_opts)

-- GitSigns
map('n', '<leader>hd', ':Gitsigns preview_hunk<CR>', default_opts)
map('n', '<leader>hn', ':Gitsigns next_hunk<CR>', default_opts)
map('n', '<leader>hp', ':Gitsigns prev_hunk<CR>', default_opts)
map('n', '<leader>hs', ':Gitsigns setloclist<CR>', default_opts)

-- Hop
map('n', '<leader>mw', ':HopWord<CR>', default_opts)
map('n', '<leader>mW', ':HopWordCurrentLine<CR>', default_opts)
map('n', '<leader>mwa', ':HopWordAC<CR>', default_opts)
map('n', '<leader>mwb', ':HopWordBC<CR>', default_opts)
map('n', '<leader>mwm', ':HopWordMW<CR>', default_opts)

-- Ultest
-- map('n', '<localleader>ut', ':Ultest<CR>', default_opts)
-- map('n', '<localleader>us', ':UltestSummary<CR>', default_opts)
-- map('n', '<localleader>ud', ':UltestDebugNearest<CR>', default_opts)
-- map('n', '<localleader>uo', ':UltestOutput<CR>', default_opts)
-- map('n', '<localleader>un', ':UltestNearest<CR>', default_opts)
-- map('n', '<localleader>ul', ':UltestLast<CR>', default_opts)

-- AutoSave
map('n', '<leader>ast', ':ASToggle<CR>', default_opts)

-- Persisted
-- map('n', '<leader>pt', ':SessionToggle<CR>', default_opts)

-- Formatting
map('n', '<leader>ff', ':lua vim.lsp.buf.formatting()<CR>', default_opts)

-- Neozoom
map('n', '<CR>', ':NeoZoomToggle<CR>', NOREF_NOERR_TRUNC)
