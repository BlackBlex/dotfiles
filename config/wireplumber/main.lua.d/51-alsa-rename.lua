rule = {
	matches = {{{"device.name", "equals", "alsa_card.pci-0000_00_01.1"}}},
	apply_properties = {["device.description"] = "Monitor Samsung"}
}

rule2 = {
	matches = {{{"device.name", "equals", "alsa_card.pci-0000_00_09.2"}}},
	apply_properties = {["device.description"] = "Laptop"}
}

rule3 = {
	matches = {{{"device.name", "equals", "alsa_card.usb-Plantronics_Plantronics_BT600_8b59b70eccad1445a8a33fbdddff4c2c-00"}}},
	apply_properties = {["device.description"] = "Manos libres Plantronics USB"}
}

table.insert(alsa_monitor.rules, rule)
table.insert(alsa_monitor.rules, rule2)
table.insert(alsa_monitor.rules, rule3)
