#!/bin/sh
BAR_HEIGHT=30
BORDER_SIZE=1
YAD_WIDTH=400
YAD_HEIGHT=300

if [ "$(xdotool getwindowfocus getwindowname)" = "yad-calendar" ]; then
	exit 0
fi

eval "$(xdotool getmouselocation --shell)"
eval "$(xdotool getdisplaygeometry --shell)"

# X
if [ "$((X + YAD_WIDTH / 2 + BORDER_SIZE))" -gt "$WIDTH" ]; then #Right side
	: $((pos_x = WIDTH - YAD_WIDTH + 300))
elif [ "$((X - YAD_WIDTH / 2 - BORDER_SIZE))" -lt 0 ]; then #Left side
	: $((pos_x = BORDER_SIZE))
else #Center
	: $((pos_x = X - YAD_WIDTH / 2))
fi

# Y
if [ "$Y" -gt "$((HEIGHT / 2))" ]; then #Bottom
	: $((pos_y = HEIGHT - YAD_HEIGHT - BAR_HEIGHT - BORDER_SIZE))
else #Top
	: $((pos_y = BAR_HEIGHT + BORDER_SIZE))
fi

yad --calendar --undecorated --fixed --no-buttons \
	--width="$YAD_WIDTH" --height="$YAD_HEIGHT" --posx="$pos_x" --posy="$pos_y" \
	--title="yad-calendar" --borders=0 >/dev/null &
