import System.IO(hPutStrLn)
import System.Exit (exitSuccess)
import Control.Monad (liftM2)
import Text.Regex.Posix ((=~))

import XMonad

import XMonad.Actions.CopyWindow (killAllOtherCopies)
import XMonad.Actions.WithAll (killAll)
import XMonad.Actions.CycleWS (nextScreen, prevScreen)

import XMonad.Layout.MultiColumns(multiCol)
import XMonad.Layout.MultiToggle(mkToggle, EOT(EOT), Toggle(Toggle), (??))
import XMonad.Layout.MultiToggle.Instances(StdTransformers( FULL, NOBORDERS ))
import XMonad.Layout.Spacing(spacing)
import XMonad.Layout.NoBorders (noBorders)
import XMonad.Layout.PerWorkspace (onWorkspace)

import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat, isDialog, doCenterFloat, doRectFloat)
import XMonad.Hooks.DynamicLog(dynamicLogWithPP, ppOutput, ppOrder, ppWsSep, ppTitle, ppCurrent, ppVisible, ppUrgent, ppHidden, ppHiddenNoWindows, shorten, xmobarColor, xmobarPP, )--wrap
import XMonad.Hooks.ManageDocks(avoidStruts, docksEventHook, manageDocks)
import XMonad.Hooks.SetWMName

import XMonad.Util.Run(spawnPipe)
import XMonad.Util.SpawnOnce(spawnOnce)
import XMonad.Util.EZConfig (additionalKeysP)

import qualified XMonad.StackSet as W

-- | Regular expressions matching for ManageHooks
(~?) :: (Functor f) => f String -> String -> f Bool
q ~? x = fmap (=~ x) q

--comment
myTerminal :: String
myTerminal = "Kitty"

--Workspace Zone
workspaceTerminals = "  \xf120  "
workspaceBrowser   = "  \xe007  "
workspaceDev       = "  \xf121  "
workspaceVM        = "  \xf108  "
workspaceExplorer  = "  \xf07c  "
workspaceSocial    = "  \xf007  "
workspaceOffice    = "  \xf15b  "

myWorkspaces :: [String]
--myWorkspaces = ["1:term", "2:browser", "3:dev", "4:vm", "5:explorer", "6:social", "7:office"]
myWorkspaces = [workspaceTerminals, workspaceBrowser, workspaceDev, workspaceSocial, workspaceVM, workspaceExplorer, workspaceOffice]

--Variables
myBorderWidth :: Dimension
myBorderWidth = 2

-- Colors zone
myTitleColor      = "#4aa5f0"
myFocusColor      = "#7C8A99"
--myCurrentWSColor  = "#a5e075"
--myUrgentWSColor   = "#d19a66"
--myHiddenWSColor   = "#4aa5f0"
--myHiddenNoWSColor = "#d7dae0"
--myVisibleWSColor  = "#42b3c2"

myIconWSCurrentBg = "#14181c"
myIconWSUrgentBg  = "#d19a66"
myIconWSColor     = "#abb2bf"

--Format zone
--myCurrentWSLeft = "["
--myCurrentWSRight = "]"
--myUrgentWSLeft = "{"
--myUrgentWSRight = "}"
--myHiddenWSLeft = "<"
--myHiddenWSRight = ">"
--myVisibleWSLeft = "("
--myVisibleWSRight = ")"

myStartupHook :: X ()
myStartupHook = do
    spawnOnce "stalonetray"
    spawnOnce "xsetroot -cursor_name left_ptr"
    spawnOnce "~/autostart.sh"
    setWMName "LG3D"

myLayout = avoidStruts $
    spacing 3 $
    onWorkspace workspaceSocial (Tall 2 (3/100) (1/3) ||| (noBorders Full)) $
    onWorkspace workspaceVM (noBorders Full) $
    mkToggle (NOBORDERS ?? FULL ?? EOT) $
    Tall 1 (3/100) (1/2) ||| multiCol [1] 1 0.01 (-0.5)

myManageHook = manageDocks <+> (composeAll . concat $ [
    [resource  =? r         --> doIgnore                                 | r <- appWSIgnores],
    [className =? c         --> doShift workspaceTerminals               | c <- appWSTerminal],
    [role      =? "browser" --> viewShift workspaceBrowser],
    [className =? c         --> viewShift workspaceDev                   | c <- appWSDev],
    [className =? c         --> viewShift workspaceVM                    | c <- appWSVM],
    [className =? c         --> viewShift workspaceExplorer              | c <- appWSExplorer],
    [name      ~? n         --> viewShift workspaceSocial                | n <- appWSSocialName],
    [className =? c         --> viewShift workspaceSocial                | c <- appWSSocial],
    [name      ~? n         --> viewShift workspaceOffice                | n <- appWSOffice],

    [className =? c         --> doCenterFloat | c <- appWSCenterFloat],
    [name      =? n         --> doCenterFloat | n <- appWSCenterFloatName],

    [className =? "Galculator" --> doRectFloat (W.RationalRect 0.36 0.2 0.25 0.45)],
    [className =? "Mirage"     --> doFullFloat],

    [isDialog                --> doRectFloat (W.RationalRect 0.2 0.20 0.60 0.60)],
    [(name =? "Compare " <&&> className =? "Eclipse")   --> doRectFloat (W.RationalRect 0.2 0.20 0.60 0.60)],
    [isFullscreen --> doF W.focusDown <+> doFullFloat]
    ])
    where
        role    = stringProperty "WM_WINDOW_ROLE"
        name    = stringProperty "WM_NAME"

        appWSCenterFloat = ["Blueman-manager", "Pavucontrol", "Nm-connection-editor", "Polkit-gnome-authentication-agent-1", "com.imox.ui.Main", "Yad"]
        appWSCenterFloatName = ["Page Unresponsive", "Pick"]
        appWSIgnores     = ["desktop", "desktop_window", "stalonetray"]
        appWSTerminal    = ["Kitty"]
        appWSDev         = ["Code", "Eclipse", "DBeaver", "com.oracle.javafx.scenebuilder.app.SceneBuilderApp", "GitKraken", "Godot"]
        appWSVM          = ["TeamViewer", "Virt-manager"]
        appWSExplorer    = ["Filezilla", "Spacefm"]
        appWSSocial      = ["Thunderbird", "Skype", "TelegramDesktop", "ICE-SSB-whatsapp", "ICE-SSB-messenger"]
        appWSSocialName  = [".*Messenger.*", ".*WhatsApp.*"]
        appWSOffice      = [".*LibreOffice.*"]
        viewShift = doF . liftM2 (.) W.greedyView W.shift

myKeys :: [(String, X ())]
myKeys = [
        --- My Applications (Super+Alt+Key)
        ("M-q",    spawn "reconfigure xmonad"),
        ("M-e",    spawn "spacefm"),
        ("M-r",    spawn "rofi -disable-history -show"),
        ("C-M1-l", spawn "dm-tool lock"),
        ("C-M1-q", spawn "rofi_power_menu"),
        ("M-f",    sendMessage $ Toggle FULL),

        --Manager
        ("M-S-a", killAll),
        ("M-j", nextScreen),
        ("M-k", prevScreen),

        -- Multimedia Keys,
        ("<XF86AudioPlay>",         spawn "playerctl play-pause"),
        ("<XF86AudioPause>",        spawn "playerctl play-pause"),
        ("<XF86AudioPrev>",         spawn "playerctl previous"),
        ("<XF86AudioNext>",         spawn "playerctl next"),
        ("<XF86MonBrightnessUp>",   spawn "backlight up"),
        ("<XF86MonBrightnessDown>", spawn "backlight down"),
        ("<XF86AudioLowerVolume>",  spawn "change_volume down"),
        ("<XF86AudioRaiseVolume>",  spawn "change_volume up"),
        ("<XF86AudioMute>",         spawn "change_volume toggle"),
        ("S-<Print>",               spawn "flameshot gui -p ~/Pictures/Screenshots"),
        ("<Print>",                 spawn "flameshot full -p ~/Pictures/Screenshots"),
        ("C-<Print>",               spawn "flameshot full -c -p ~/Pictures/Screenshots")
    ]

main = do
    xmproc <- spawnPipe "xmobar"
    xmonad $ defaultConfig {
        terminal        = myTerminal,
        modMask         = mod4Mask,
        logHook         = dynamicLogWithPP $ xmobarPP {
            ppOutput    = hPutStrLn xmproc,
            ppOrder     = \(ws:l:t:_)   -> [ws],
            ppTitle     = xmobarColor myTitleColor "" . shorten 100,
            ppWsSep     = "",
            ppCurrent   = xmobarColor myIconWSColor myIconWSCurrentBg,-- .  --wrapmyCurrentWSLeft myCurrentWSRight,
            ppUrgent    = xmobarColor myIconWSColor myIconWSUrgentBg,-- . wrap myUrgentWSLeft myUrgentWSRight,
            ppHidden    = xmobarColor myIconWSColor "",-- . wrap myHiddenWSLeft myHiddenWSRight,
            ppHiddenNoWindows = xmobarColor myIconWSColor "",
            ppVisible   = xmobarColor myIconWSColor ""-- . wrap myVisibleWSLeft myVisibleWSRight,
        },
        --manageHook      = manageDocks <+> manageHook defaultConfig,
        --layoutHook      = avoidStruts $ layoutHook defaultConfig,
        manageHook      = myManageHook,
        layoutHook      = myLayout,
        handleEventHook = handleEventHook defaultConfig <+> docksEventHook,
        borderWidth     = myBorderWidth,
        focusedBorderColor = myFocusColor,
        workspaces      = myWorkspaces,
        startupHook     = myStartupHook
    } `additionalKeysP` myKeys
