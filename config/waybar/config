// -*- mode: jsonc -*-
{
    "layer": "top", // Waybar at top layer
    // "position": "bottom", // Waybar position (top|bottom|left|right)
    // "height": 30, // Waybar height (to be removed for auto height)
    // "width": 1280, // Waybar width
    "spacing": 20, // Gaps between modules (4px)
    // Choose the order of the modules
    //    "sway/mode", "sway/scratchpad", "custom/media"
    "modules-left": [
        "hyprland/workspaces",
        "hyprland/submap"
    ],
    "modules-center": [
        // "hyprland/window"
        "clock"
    ],
    "modules-right": [
        "keyboard-state",
        // "mpd",
        // "idle_inhibitor",
        "pulseaudio",
        // "network",
        "power-profiles-daemon",
        "cpu",
        "memory",
        "temperature",
        "backlight",
        // "keyboard-state",
        // "sway/language",
        "battery",
        // "battery#bat1",
        "tray"
    ],
    // Modules configuration
    "hyprland/workspaces": {
        "show-special": true,
        "format": "{icon}",
        "on-scroll-up": "hyprctl dispatch workspace e+1",
        "on-scroll-down": "hyprctl dispatch workspace e-1",
        "format-icons": {
            "1": "",
            "2": "",
            "3": "",
            "4": "",
            "5": "",
            "6": "",
            "7": "",
            "8": "",
            "urgent": "",
            "focused": "",
            "default": ""
        }
    },
    "hyprland/submap": {
        "format": "✌️ {}",
        "tooltip": false
    },
    "clock": {
        "format": "{:%I:%M %p}",
        "format-alt": "{:%A, %d %B, %Y}",
        "tooltip-format": "<tt><small>{calendar}</small></tt>",
        "calendar": {
            "mode": "year",
            "mode-mon-col": 3,
            "weeks-pos": "right",
            "on-scroll": 1,
            "on-click-right": "mode",
            "format": {
                "months": "<span class='months'><b>{}</b></span>",
                "days": "<span class='days'><b>{}</b></span>",
                "weeks": "<span class='weeks'><b>W{}</b></span>",
                "weekdays": "<span class='weekdays'><b>{}</b></span>",
                "today": "<span class='today'><b><u>{}</u></b></span>"
            }
        },
        "actions": {
            "on-click-right": "mode",
            "on-click-forward": "tz_up",
            "on-click-backward": "tz_down",
            "on-scroll-up": "shift_up",
            "on-scroll-down": "shift_down"
        }
    },
    "power-profiles-daemon": {
        "format": "{icon}",
        "tooltip-format": "Power profile: {profile}\nDriver: {driver}",
        "tooltip": true,
        "format-icons": {
            "default": "",
            "performance": "",
            "balanced": "",
            "power-saver": ""
        }
    },
    "keyboard-state": {
        "numlock": true,
        "capslock": true,
        "format": {
            "numlock": "󰎠 {icon}",
            "capslock": "󰬴 {icon} "
        },
        "format-icons": {
            "locked": "󰗠",
            "unlocked": "󰅙"
        }
    },
    "pulseaudio": {
        // "scroll-step": 1, // %, can be a float
        "format": "{volume}% {icon} {format_source}",
        "format-bluetooth": "{volume}% {icon} ",
        "format-bluetooth-muted": "󰖁 {icon} ",
        "format-muted": "󰖁",
        "format-source": "{volume}% 󰍬",
        "format-source-muted": "󰍭",
        "format-icons": {
            "headphone": "󰋋",
            "hands-free": "󰋏",
            "headset": "󰋎",
            "phone": "󰏲",
            "portable": "󰏲",
            "car": "󰄋",
            "default": [
                "󰕿",
                "󰖀",
                "󰕾"
            ]
        },
        "on-click": "pavucontrol"
    },
    "cpu": {
        "format": "󰍛"
    },
    "memory": {
        "format": "󰑭",
        "tooltip-format": "{used:0.1f}G/{total:0.1f}G"
    },
    "temperature": {
        "critical-threshold": 80,
        "format": "{icon}",
        "format-icons": [
            "󱃃",
            "󱃂",
            "󰸁"
        ]
    },
    "backlight": {
        // "device": "acpi_video1",
        "format": "{icon}",
        "format-icons": [
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
        ]
    },
    "battery": {
        "states": {
            // "good": 95,
            "warning": 30,
            "critical": 15
        },
        "format": "{icon}",
        "format-charging": "󰃨",
        "format-plugged": "",
        "format-alt": "{icon}",
        "format-icons": [
            "󰂃",
            "󰁻",
            "󰁻",
            "󰁼",
            "󰁽",
            "󰁾",
            "󰁿",
            "󰂀",
            "󰂁",
            "󰂂",
            "󰁹"
        ],
        "tooltip-format": "{capacity} {timeTo}"
    },
    "tray": {
        "icon-size": 21,
        "spacing": 10
    }
}