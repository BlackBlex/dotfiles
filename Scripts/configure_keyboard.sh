#!/bin/sh
#Keyboard switch latam
result=$(xinput --list | grep -i "Chicony HP Elite USB Keyboard" | grep -v "pointer" | awk -F'=' '{print $2}' | awk -F'\t' '{print $1}')

for id in $result; do
    setxkbmap -device $id -layout latam
done

result=$(xinput --list | grep -i "HP HP 330 Wireless Mouse and Keyboard Combo" | grep -v "pointer" | awk -F'=' '{print $2}' | awk -F'\t' '{print $1}')

for id in $result; do
    setxkbmap -device $id -layout latam
done

#setxkbmap -device 11 -layout latam

#xinput set-button-map "Dell Premium USB Optical Mouse" 3 2 1 &
