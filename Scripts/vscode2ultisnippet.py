#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse, os, sys, json, locale, time, signal
from pwn import log

ENC = locale.getpreferredencoding()

signal.signal(signal.SIGINT, signal.SIG_DFL)

input_file = ""
output_file = ""

def main():
    global input_file
    global output_file
    parser = argparse.ArgumentParser(description = "Usage for vscode2ultisnippet")
    parser.add_argument("-i", "--inputfile", help = "Specify input file path (vscode snippet)", required = True)
    parser.add_argument("-o", "--outputfile", help = "Specify when to save result file (ultisnippet)", required = True)

    argument = parser.parse_args()

    status = False

    if argument.inputfile:
        input_file = argument.inputfile

        if argument.outputfile:
            output_file = argument.outputfile
            status = True

    if not status:
        parser.print_help()
    else:
        p = log.progress("Parse")

        p.status("Verificando que el archivo exista")
        time.sleep(1)
        if os.path.isfile(input_file):
            p.status("Abriendo archivo")
            open_file = open(input_file, "r")
            time.sleep(1)

            if not open_file.closed:
                p.status("Leyendo archivo")
                json_file = json.loads(open_file.read())
                open_file.close()
                time.sleep(1)

                if json_file:
                    p.status("Convirtiendo a snippet")

                    snippet_final = ""

                    for key in json_file:
                        snippet_obj = json_file[key]

                        snippet_final += "snippet {0} \"{1}\"\n".format(snippet_obj["prefix"], snippet_obj["description"])
                        snippet_final += "\n".join(snippet_obj["body"]) + "\n"
                        snippet_final += "endsnippet\n\n"

                    #print(snippet_final)
                    time.sleep(1)

                    if len(snippet_final) > 0:
                        p.status("Guardando archivo")
                        json_final = open(output_file, "w")
                        json_final.write(snippet_final)
                        json_final.close()

                        time.sleep(1)
                        p.success("Finalizado")
                    else:
                        p.failure("Ocurrió un error")

                # print(json_file)


if __name__ == "__main__":
   main()
